package net.thumbtack.school.concert.model;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Comment {
    @NonNull
    private AbstractUser user;
    @NonNull
    private Song song;
    @NonNull
    private String comment;
    @NonNull
    private Set<User> joined = new HashSet<>();

    public void addJoinedUser(User user){
        this.joined.add(user);
    }

    public void removeJoinedUser(User user){
        this.joined.remove(user);
    }

}
