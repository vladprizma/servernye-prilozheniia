package net.thumbtack.school.concert.daoimpl;

import net.thumbtack.school.concert.dao.UserDao;
import net.thumbtack.school.concert.database.Database;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.model.User;

public class UserDaoImpl implements UserDao {

    @Override
    public void addActiveUser(String token, User user) {
        Database.getDatabase().addActiveUser(token, user);
    }

    @Override
    public void deleteActiveUser(String token) {
        Database.getDatabase().deleteActiveUser(token);
    }

    @Override
    public void deleteUser(String token) {
        Database.getDatabase().deleteUser(token);
    }

    @Override
    public String containsActiveUser(User user) {
        return Database.getDatabase().containsActiveUser(user);
    }

    @Override
    public User getUserByLogin(String login) {
        return Database.getDatabase().getUserByLogin(login);
    }

    @Override
    public User getUserByToken(String token){
        return Database.getDatabase().getUserByToken(token);
    }

    @Override
    public void registerUser(String token, User user) throws ServerException {
        Database.getDatabase().registerUser(token, user);
    }

}
