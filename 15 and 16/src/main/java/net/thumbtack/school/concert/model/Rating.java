package net.thumbtack.school.concert.model;


import lombok.*;

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Rating {
    @NonNull
    private User user;
    @NonNull
    private Song song;
    @NonNull
    private int rating;
}
