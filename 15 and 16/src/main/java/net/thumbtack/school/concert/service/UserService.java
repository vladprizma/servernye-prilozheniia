package net.thumbtack.school.concert.service;

import com.google.gson.Gson;
import net.thumbtack.school.concert.dao.SongDao;
import net.thumbtack.school.concert.dao.UserDao;
import net.thumbtack.school.concert.daoimpl.SongDaoImpl;
import net.thumbtack.school.concert.daoimpl.UserDaoImpl;
import net.thumbtack.school.concert.dto.request.DeleteUserDtoRequest;
import net.thumbtack.school.concert.dto.request.LoginDtoRequest;
import net.thumbtack.school.concert.dto.request.LogoutDtoRequest;
import net.thumbtack.school.concert.dto.request.RegisterDtoRequest;
import net.thumbtack.school.concert.dto.response.EmptyJsonDtoResponse;
import net.thumbtack.school.concert.dto.response.ErrorDtoResponse;
import net.thumbtack.school.concert.dto.response.LoginDtoResponse;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.generic.GenericMethods;
import net.thumbtack.school.concert.mapper.Mapper;
import net.thumbtack.school.concert.model.*;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class UserService {
    private static Gson gson = new Gson();
    private static final int MIN_SONG_RATING_AMOUNT = 1;
    private static final int NO_JOINED_USERS = 0;
    private UserDao userDao = new UserDaoImpl();
    private SongDao songDao = new SongDaoImpl();

    private User getUserByToken(String token) throws ServerException {
        User user = userDao.getUserByToken(token);
        if(user == null){
            throw new ServerException(ServerErrorCode.USER_WRONG_TOKEN);
        }
        return user;
    }

    private User getUserByLogin(String login) throws ServerException {
        User user = userDao.getUserByLogin(login);
        if(user == null){
            throw new ServerException(ServerErrorCode.USER_LOGIN_DOES_NOT_EXIST);
        }
        return user;
    }

    private User checkPassword(String password, User user) throws ServerException {
        if(!password.equals(user.getPassword())){
            throw new ServerException(ServerErrorCode.USER_PASSWORD_INCORRECT);
        }
        return user;
    }


    public String registration(String jsonString) {
        try {
            RegisterDtoRequest dtoRequest = GenericMethods.getClassFromJson(RegisterDtoRequest.class, jsonString);
            RegisterDtoRequest.validate(dtoRequest);

            User user = Mapper.INSTANCE.dtoToUser(dtoRequest);
            String token = UUID.randomUUID().toString();
            userDao.registerUser(token, user);

            LoginDtoResponse dtoResponse = new LoginDtoResponse(token);

            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String login(String jsonString) {
        try {
            LoginDtoRequest dtoRequest = GenericMethods.getClassFromJson(LoginDtoRequest.class, jsonString);
            LoginDtoRequest.validate(dtoRequest);
            User user = checkPassword(dtoRequest.getPassword(), getUserByLogin(dtoRequest.getLogin()));

            String token = userDao.containsActiveUser(user);
            if(token == null){
                token = UUID.randomUUID().toString();
                userDao.addActiveUser(token, user);
            }

            LoginDtoResponse dtoResponse = new LoginDtoResponse(token);

            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String logout(String jsonString) {
        try {
            LogoutDtoRequest dtoRequest = GenericMethods.getClassFromJson(LogoutDtoRequest.class, jsonString);
            LogoutDtoRequest.validate(dtoRequest);

            userDao.deleteActiveUser(dtoRequest.getToken());

            EmptyJsonDtoResponse dtoResponse = new EmptyJsonDtoResponse();
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String deleteUser(String jsonString) {
        try {
            DeleteUserDtoRequest dtoRequest = GenericMethods.getClassFromJson(DeleteUserDtoRequest.class, jsonString);
            DeleteUserDtoRequest.validate(dtoRequest);
            String token = dtoRequest.getToken();

            User user = getUserByToken(token);
            Set<Rating> ratingToDelete = new HashSet<>();

            for(Song song : user.getSongs()){
                if(song.getRatings().size() == MIN_SONG_RATING_AMOUNT){
                    user.removeSong(song);
                    for(Comment comment : song.getComments().values()){
                        comment.getUser().removeComment(comment);
                    }
                    for(Rating rating : song.getRatings().values()){
                        rating.getUser().removeRating(rating);
                    }
                    songDao.removeSong(song.getId());
                }
                else{
                    song.setSongAdder(Community.getCommunity());
                    song.removeRatingByUser(user);
                    ratingToDelete.add(song.getRatingByUser(user));
                }
            }

            user.getRatedSongs().clear();
            for(Rating rating : ratingToDelete) {
                user.removeRating(rating);
            }

            for (Comment comment : user.getComments()){
                if(comment.getJoined().size() == NO_JOINED_USERS) {
                    user.removeComment(comment);
                    comment.getSong().removeComment(comment);
                }
                else{
                    user.removeComment(comment);
                    comment.setUser(Community.getCommunity());
                }
            }

            for (Rating rating : user.getRatedSongs()){
                rating.getSong().removeRatingByUser(user);
            }

            userDao.deleteUser(token);

            EmptyJsonDtoResponse dtoResponse = new EmptyJsonDtoResponse();
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

}
