package net.thumbtack.school.concert.database;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.model.Song;
import net.thumbtack.school.concert.model.User;

import java.util.*;

public final class Database {
    private static Database database;
    private static int idSongCounter;
    private static final int FIRST_ELEMENT = 0;
    private Map<String, User> users;
    private BiMap<String, User> onlineUsers;
    private BiMap<Integer, Song> idSong;
    private Multimap<String, Song> composerSongs;
    private Multimap<String, Song> wordsAuthorsSongs;
    private Multimap<String, Song> performerSongs;


    private Database() {
        idSongCounter = 0;
        this.users = new HashMap<>();
        this.onlineUsers = HashBiMap.create();
        this.idSong = HashBiMap.create();
        this.composerSongs = HashMultimap.create();
        this.wordsAuthorsSongs = HashMultimap.create();
        this.performerSongs = HashMultimap.create();
    }

    public static Database getDatabase(){
        if(Database.database == null){
            database = new Database();
        }
        return database;
    }

    //Возвращает токен по активному пользователю, если он уже в сети
    public String containsActiveUser(User user){
        return onlineUsers.inverse().get(user);
    }

    public void addActiveUser(String token, User user){
        this.onlineUsers.put(token, user);
    }

    public void deleteActiveUser(String token) {
        onlineUsers.remove(token);
    }

    public User getUserByLogin(String login) {
        return users.get(login);
    }

    public User getUserByToken(String token){
        return onlineUsers.get(token);
    }

    public void registerUser(String token, User user) throws ServerException {
        String userLogin = user.getLogin();
        if(users.putIfAbsent(userLogin, user) != null){
            throw new ServerException(ServerErrorCode.USER_LOGIN_OCCUPIED);
        }
        addActiveUser(token, user);
    }

    public void deleteUser(String token){
        this.users.remove(onlineUsers.remove(token).getLogin());
    }

    public int addSong(Song song) throws ServerException {
        idSongCounter++;
        this.idSong.put(idSongCounter, song);
        for(String composer : song.getComposers()){
            this.composerSongs.put(composer, song);
        }
        for(String wordsAuthor : song.getWordsAuthors()){
            this.wordsAuthorsSongs.put(wordsAuthor, song);
        }
        this.performerSongs.put(song.getPerformer(), song);

        return idSongCounter;
    }

    public void removeSong(int id){
        this.idSong.remove(id);
    }

    public Song getSong(int id) {
        return idSong.get(id);
    }

    public Set<Song> getSongs() {
        return idSong.values();
    }

    public Multimap<String, Song> getSongsFromComposers(List<String> composers) {
        Multimap<String, Song> songs = HashMultimap.create();
        for(String composer : composers){
            if(composerSongs.containsKey(composer)){
                songs.putAll(composer, composerSongs.get(composer));
            }
        }
        return songs;
    }

    public Multimap<String, Song> getSongsFromWordsAuthors(List<String> wordsAuthors) {
        Multimap<String, Song> songs = HashMultimap.create();
        for(String wordsAuthor : wordsAuthors){
            if(wordsAuthorsSongs.containsKey(wordsAuthor)){
                songs.putAll(wordsAuthor, wordsAuthorsSongs.get(wordsAuthor));
            }
        }
        return songs;
    }

    public List<Song> getSongsFromPerformer(String performer) {
        return new ArrayList<>(performerSongs.get(performer));
    }

}
