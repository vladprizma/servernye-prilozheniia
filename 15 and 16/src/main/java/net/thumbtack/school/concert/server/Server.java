package net.thumbtack.school.concert.server;

import net.thumbtack.school.concert.service.DatabaseService;
import net.thumbtack.school.concert.service.SongService;
import net.thumbtack.school.concert.service.UserService;


public class Server {
    private boolean serverStart = false;
    private boolean serverStop = false;
    private UserService userService = new UserService();
    private SongService songService = new SongService();
    private DatabaseService databaseService = new DatabaseService();

    //Исключение в UserService
    public String startServer(String savedDataFileName) {
        serverStart = true;
        serverStop = false;
        if (savedDataFileName != null) {
            return databaseService.databaseStart(savedDataFileName);
        }
        return null;
    }

    public String saveServer(String savedDataFileName){
        serverStart = false;
        serverStop = true;
        if (savedDataFileName != null) {
            return databaseService.databaseSave(savedDataFileName);
        }
        return null;
    }

    public String registerUser(String requestJsonString) {
        if(!serverStart || serverStop){
            return null;
        }
        return userService.registration(requestJsonString);
    }

    public String loginUser(String requestJsonString) {
        if(!serverStart || serverStop){
            return null;
        }
        return userService.login(requestJsonString);
    }

    public String logoutUser(String requestJsonString) {
        if(!serverStart || serverStop){
            return null;
        }
        return userService.logout(requestJsonString);
    }

    public String deleteUser(String requestJsonString) {
        if(!serverStart || serverStop){
            return null;
        }
        return userService.deleteUser(requestJsonString);
    }

    public String addSong(String requestJsonString){
        if(!serverStart || serverStop){
            return null;
        }
        return songService.addSong(requestJsonString);
    }

    public String deleteSong(String requestJsonString){
        if(!serverStart || serverStop){
            return null;
        }
        return songService.deleteSong(requestJsonString);
    }

    public String addComment(String requestJsonString){
        if(!serverStart || serverStop){
            return null;
        }
        return songService.addComment(requestJsonString);
    }

    public String changeComment(String requestJsonString){
        if(!serverStart || serverStop){
            return null;
        }
        return songService.changeComment(requestJsonString);
    }

    public String parsingJoinToComment(String requestJsonString){
        if(!serverStart || serverStop){
            return null;
        }
        return  songService.joinToComment(requestJsonString);
    }

    public String parsingCancelJoinToComment(String requestJsonString) {
        if (!serverStart || serverStop) {
            return null;
        }
        return songService.cancelJoinToComment(requestJsonString);
    }

    public String addRating(String requestJsonString){
        if(!serverStart || serverStop){
            return null;
        }
        return songService.addRating(requestJsonString);
    }

    public String changeRating(String requestJsonString){
        if(!serverStart || serverStop){
            return null;
        }
        return songService.changeRating(requestJsonString);
    }

    public String deleteRating(String requestJsonString){
        if(!serverStart || serverStop){
            return null;
        }
        return songService.deleteRating(requestJsonString);
    }

}
