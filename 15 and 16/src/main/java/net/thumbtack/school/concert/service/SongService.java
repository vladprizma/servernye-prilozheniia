package net.thumbtack.school.concert.service;

import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import net.thumbtack.school.concert.dao.SongDao;
import net.thumbtack.school.concert.dao.UserDao;
import net.thumbtack.school.concert.daoimpl.SongDaoImpl;
import net.thumbtack.school.concert.daoimpl.UserDaoImpl;
import net.thumbtack.school.concert.dto.request.*;
import net.thumbtack.school.concert.dto.response.AddSongDtoResponse;
import net.thumbtack.school.concert.dto.response.AppropriateSongsDtoResponse;
import net.thumbtack.school.concert.dto.response.EmptyJsonDtoResponse;
import net.thumbtack.school.concert.dto.response.ErrorDtoResponse;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.generic.GenericMethods;
import net.thumbtack.school.concert.mapper.Mapper;
import net.thumbtack.school.concert.model.*;

import java.util.ArrayList;
import java.util.List;

public class SongService {
    private static Gson gson = new Gson();
    private static final int FIRST_ELEMENT = 0;
    private static final int MAX_RATING = 5;
    private static final int MIN_SONG_RATING_AMOUNT = 1;
    private static final int NO_JOINED_USERS = 0;
    private SongDao songDao = new SongDaoImpl();
    private UserDao userDao = new UserDaoImpl();


    private User getUserByLogin(String login) throws ServerException {
        User user = userDao.getUserByLogin(login);
        if(user == null){
            throw new ServerException(ServerErrorCode.USER_LOGIN_DOES_NOT_EXIST);
        }
        return user;
    }

    private User getUserByToken(String token) throws ServerException {
        User user = userDao.getUserByToken(token);
        if(user == null){
            throw new ServerException(ServerErrorCode.USER_WRONG_TOKEN);
        }
        return user;
    }

    private Song getSong(int id) throws ServerException {
        Song song = songDao.getSong(id);
        if(song == null){
            throw new ServerException(ServerErrorCode.SONG_DOES_NOT_EXIST);
        }
        return song;
    }

    private void songRated(User user, Song song, boolean isRated) throws ServerException {
        Rating rating = song.getRatingByUser(user);
        if(rating != null && !isRated){
            throw new ServerException(ServerErrorCode.SONG_RATING_ALREADY_ADDED);
        }
        if(rating == null && isRated){
            throw new ServerException(ServerErrorCode.SONG_IS_NOT_RATED_BY_USER);
        }
    }

    private void userIsSongAdder(User user, Song song, boolean equalsUser) throws ServerException {
        if(!song.getSongAdder().equals(user) && equalsUser){
            throw new ServerException(ServerErrorCode.SONG_WRONG_USER_DELETE_ATTEMPT);
        }
        if(song.getSongAdder().equals(user) && !equalsUser){
            throw new ServerException(ServerErrorCode.SONG_ADDER_CHANGE_RATING_ATTEMPT);
        }
    }

    private void containsCommentByUser(User user, Song song, boolean isCommented) throws ServerException {
        Comment comment = song.getCommentByUser(user);
        if(comment == null && isCommented){
            throw new ServerException(ServerErrorCode.SONG_IS_NOT_COMMENTED_BY_USER);
        }
        if (comment != null && !isCommented) {
            throw new ServerException(ServerErrorCode.SONG_USER_ALREADY_IN_CURRENT_COMMENT_SECTION);
        }
    }

    private void containsJoinedUser(User commentAuthor, User user, Song song, boolean containsUser) throws ServerException {
        Comment comment = song.getCommentByUser(commentAuthor);
        if(!comment.getJoined().contains(user) && containsUser){
            throw new ServerException(ServerErrorCode.SONG_USER_NOT_JOINED_TO_COMMENT);
        }
        if(comment.getJoined().contains(user) && !containsUser){
            throw new ServerException(ServerErrorCode.SONG_USER_ALREADY_JOINED_TO_COMMENT);
        }
    }


    public String addSong(String jsonString) {
        try{
            AddSongDtoRequest dtoRequest = GenericMethods.getClassFromJson(AddSongDtoRequest.class, jsonString);
            AddSongDtoRequest.validate(dtoRequest);

            Song song = Mapper.INSTANCE.dtoToSong(dtoRequest);
            User user = getUserByToken(dtoRequest.getToken());

            song.setSongAdder(user);
            Rating rating = new Rating(user, song, MAX_RATING);
            song.addRating(rating);
            user.getRatedSongs().add(rating);
            user.addSong(song);
            int id = songDao.addSong(song);
            song.setId(id);

            AddSongDtoResponse dtoResponse = new AddSongDtoResponse(id);
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String deleteSong(String jsonString) {
        try{
            DeleteSongDtoRequest dtoRequest = GenericMethods.getClassFromJson(DeleteSongDtoRequest.class, jsonString);
            DeleteSongDtoRequest.validate(dtoRequest);


            Song song = getSong(dtoRequest.getId());
            User user = getUserByToken(dtoRequest.getToken());
            userIsSongAdder(user, song, true);

            if(song.getRatings().size() == MIN_SONG_RATING_AMOUNT) {
                user.removeSong(song);
                for(Comment comment : song.getComments().values()){
                    comment.getUser().removeComment(comment);
                }
                for(Rating rating : song.getRatings().values()){
                    rating.getUser().removeRating(rating);
                }
                songDao.removeSong(song.getId());
            }
            else{
                song.setSongAdder(Community.getCommunity());
                song.removeRatingByUser(user);
                user.removeRating(song.getRatingByUser(user));
                user.removeSong(song);
            }

            EmptyJsonDtoResponse dtoResponse = new EmptyJsonDtoResponse();
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String getAllSongs(String jsonString){
        try{
            GetSongsDtoRequest dtoRequest = GenericMethods.getClassFromJson(GetSongsDtoRequest.class, jsonString);
            GetSongsDtoRequest.validate(dtoRequest);

            List<Song> songs = new ArrayList<>(songDao.getSongs());
            List<String> titles = new ArrayList<>();
            for(Song song : songs){
                titles.add(song.getTitle());
            }

            AppropriateSongsDtoResponse dtoResponse = new AppropriateSongsDtoResponse(titles);
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String getSongsFromComposers(String jsonString){
        try{
            ComposersSongsDtoRequest dtoRequest = GenericMethods.getClassFromJson(ComposersSongsDtoRequest.class, jsonString);
            ComposersSongsDtoRequest.validate(dtoRequest);

            List<String> titles = new ArrayList<>();
            Multimap<String, Song> songs = songDao.getSongsFromComposers(dtoRequest.getComposers());
            int firstElem = 0;
            for(String composer : dtoRequest.getComposers()){
                if(songs.keySet().contains(composer)){
                    break;
                }
                firstElem++;
            }
            if(songs.size() > 0) {
                List<Song> intersection = new ArrayList<>(songs.get(dtoRequest.getComposers().get(firstElem)));
                for (String composer : songs.keySet()) {
                    intersection.retainAll(songs.get(composer));
                }
                for(Song song : intersection){
                    titles.add(song.getTitle());
                }
            }

            AppropriateSongsDtoResponse dtoResponse = new AppropriateSongsDtoResponse(titles);
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String getSongsFromWordsAuthors(String jsonString){
        try{
            WordsAuthorsSongsDtoRequest dtoRequest = GenericMethods.getClassFromJson(WordsAuthorsSongsDtoRequest.class, jsonString);
            WordsAuthorsSongsDtoRequest.validate(dtoRequest);

            List<String> titles = new ArrayList<>();
            Multimap<String, Song> songs = songDao.getSongsFromWordsAuthors(dtoRequest.getWordsAuthors());
            int firstElem = 0;
            for(String wordsAuthor : dtoRequest.getWordsAuthors()){
                if(songs.keySet().contains(wordsAuthor)){
                    break;
                }
                firstElem++;
            }
            if(songs.size() > 0) {
                List<Song> intersection = new ArrayList<>(songs.get(dtoRequest.getWordsAuthors().get(firstElem)));
                for (String wordsAuthor : songs.keySet()) {
                    intersection.retainAll(songs.get(wordsAuthor));
                }
                for(Song song : intersection){
                    titles.add(song.getTitle());
                }
            }

            AppropriateSongsDtoResponse dtoResponse = new AppropriateSongsDtoResponse(titles);
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String getSongsFromPerformer(String jsonString){
        try{
            PerformerSongsDtoRequest dtoRequest = GenericMethods.getClassFromJson(PerformerSongsDtoRequest.class, jsonString);
            PerformerSongsDtoRequest.validate(dtoRequest);

            List<Song> performersSongs = new ArrayList<Song>(songDao.getSongsFromPerformer(dtoRequest.getPerformer()));
            List<String> titles = new ArrayList<>();
            for(Song song : performersSongs){
                titles.add(song.getTitle());
            }

            AppropriateSongsDtoResponse dtoResponse = new AppropriateSongsDtoResponse(titles);
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String joinToComment(String jsonString){
        try{
            JoinCommentDtoRequest dtoRequest = GenericMethods.getClassFromJson(JoinCommentDtoRequest.class, jsonString);
            JoinCommentDtoRequest.validate(dtoRequest);

            Song song = getSong(dtoRequest.getId());
            User user = getUserByToken(dtoRequest.getToken());
            User commentAuthor = getUserByLogin(dtoRequest.getCommentAuthorLogin());
            containsCommentByUser(commentAuthor, song, true);
            containsJoinedUser(commentAuthor, user, song, false);
            Comment comment = song.getCommentByUser(userDao.getUserByLogin(dtoRequest.getCommentAuthorLogin()));

            comment.addJoinedUser(user);

            EmptyJsonDtoResponse dtoResponse = new EmptyJsonDtoResponse();
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String cancelJoinToComment(String jsonString){
        try{
            CancelJoinToCommentDtoRequest dtoRequest = GenericMethods.getClassFromJson(CancelJoinToCommentDtoRequest.class, jsonString);
            CancelJoinToCommentDtoRequest.validate(dtoRequest);

            Song song = getSong(dtoRequest.getId());
            User user = getUserByToken(dtoRequest.getToken());
            User commentAuthor = getUserByLogin(dtoRequest.getCommentAuthorLogin());
            containsCommentByUser(commentAuthor, song, true);
            containsJoinedUser(commentAuthor, user, song, true);
            Comment comment = song.getCommentByUser(userDao.getUserByLogin(dtoRequest.getCommentAuthorLogin()));

            comment.removeJoinedUser(user);

            EmptyJsonDtoResponse dtoResponse = new EmptyJsonDtoResponse();
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String addComment(String jsonString){
        try{
            AddCommentDtoRequest dtoRequest = GenericMethods.getClassFromJson(AddCommentDtoRequest.class, jsonString);
            AddCommentDtoRequest.validate(dtoRequest);

            User user = getUserByToken(dtoRequest.getToken());
            Song song = getSong(dtoRequest.getId());

            containsCommentByUser(user, song, false);
            Comment comment = new Comment(user, song, dtoRequest.getComment());

            song.addComment(comment);
            user.addComment(comment);

            EmptyJsonDtoResponse dtoResponse = new EmptyJsonDtoResponse();
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String changeComment(String jsonString){
        try{
            ChangeCommentDtoRequest dtoRequest = GenericMethods.getClassFromJson(ChangeCommentDtoRequest.class, jsonString);
            ChangeCommentDtoRequest.validate(dtoRequest);

            User user = getUserByToken(dtoRequest.getToken());
            Song song = getSong(dtoRequest.getId());

            containsCommentByUser(user, song, true);
            Comment newComment = new Comment(user, song, dtoRequest.getComment());

            Comment comment = new Comment();
            for(Comment commentElem : user.getComments()){
                if(commentElem.getSong().getTitle().equals(song.getTitle()) &&
                        commentElem.getSong().getPerformer().equals(song.getPerformer())){

                    comment = commentElem;
                    break;
                }
            }

            if(comment.getJoined().size() == NO_JOINED_USERS) {
                user.removeComment(comment);
                song.removeComment(comment);
                user.addComment(newComment);
                song.addComment(newComment);
            }
            else{
                song.addComment(newComment);
                user.removeComment(comment);
                user.addComment(newComment);
                comment.setUser(Community.getCommunity());
            }

            EmptyJsonDtoResponse dtoResponse = new EmptyJsonDtoResponse();
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String addRating(String jsonString){
        try{
            RatingAddDtoRequest dtoRequest = GenericMethods.getClassFromJson(RatingAddDtoRequest.class, jsonString);
            RatingAddDtoRequest.validate(dtoRequest);

            User user = getUserByToken(dtoRequest.getToken());
            Song song = getSong(dtoRequest.getId());
            userIsSongAdder(user, song, false);
            songRated(user, song, false);
            Rating rating = new Rating(user, song, dtoRequest.getRating());

            user.addRating(rating);
            song.addRating(rating);

            EmptyJsonDtoResponse dtoResponse = new EmptyJsonDtoResponse();
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String changeRating(String jsonString){
        try{
            RatingChangeDtoRequest dtoRequest = GenericMethods.getClassFromJson(RatingChangeDtoRequest.class, jsonString);
            RatingChangeDtoRequest.validate(dtoRequest);

            User user = getUserByToken(dtoRequest.getToken());
            Song song = getSong(dtoRequest.getId());
            userIsSongAdder(user, song, false);
            songRated(user, song, true);
            Rating newRating = new Rating(user, song, dtoRequest.getRating());

            user.removeRating(song.getRatingByUser(user));
            song.removeRatingByUser(user);
            user.addRating(newRating);
            song.addRating(newRating);

            EmptyJsonDtoResponse dtoResponse = new EmptyJsonDtoResponse();
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

    public String deleteRating(String jsonString){
        try{
            RatingDeleteDtoRequest dtoRequest = GenericMethods.getClassFromJson(RatingDeleteDtoRequest.class, jsonString);
            RatingDeleteDtoRequest.validate(dtoRequest);

            User user = getUserByToken(dtoRequest.getToken());
            Song song = getSong(dtoRequest.getId());
            userIsSongAdder(user, song, false);
            songRated(user, song, true);
            user.removeRating(song.getRatingByUser(user));
            song.removeRatingByUser(user);

            EmptyJsonDtoResponse dtoResponse = new EmptyJsonDtoResponse();
            return gson.toJson(dtoResponse);
        }
        catch(ServerException e){
            return gson.toJson(new ErrorDtoResponse(e.getMessage()));
        }
    }

}
