package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.thumbtack.school.concert.dao.SongDao;
import net.thumbtack.school.concert.dao.UserDao;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

@Getter
@AllArgsConstructor
public class AddCommentDtoRequest {
    private static final int MIN_ID = 1;
    private String token;
    private int id;
    private String comment;

    public static void validate(AddCommentDtoRequest dtoRequest) throws ServerException {
        if(dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getId() < MIN_ID ||
                dtoRequest.getComment() == null ||
                dtoRequest.getComment().equals("")){

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }

}
