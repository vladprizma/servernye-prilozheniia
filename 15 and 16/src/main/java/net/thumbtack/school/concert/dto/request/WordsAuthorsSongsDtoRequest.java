package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

import java.util.List;

@AllArgsConstructor
@Getter
public class WordsAuthorsSongsDtoRequest {
    private String token;
    private List<String> wordsAuthors;

    public static void validate(WordsAuthorsSongsDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getWordsAuthors() == null ||
                dtoRequest.getWordsAuthors().size() == 0 ||
                dtoRequest.getWordsAuthors().contains(null) ||
                dtoRequest.getWordsAuthors().contains("") ) {

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }
}
