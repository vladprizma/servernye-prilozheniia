package net.thumbtack.school.concert.model;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

@Getter
public abstract class AbstractUser {
    private Set<Song> songs = new HashSet<>();
    private Set<Comment> comments = new HashSet<>();

    public void addSong(Song song){
        this.songs.add(song);
    }

    public void addComment(Comment comment){
        this.comments.add(comment);
    }

    public void removeComment(Comment comment){
        this.comments.remove(comment);
    }

}
