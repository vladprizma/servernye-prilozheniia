package net.thumbtack.school.concert.dao;

import com.google.common.collect.Multimap;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.model.Song;

import java.util.List;
import java.util.Set;


public interface SongDao {

    public int addSong(Song song) throws ServerException;

    public void removeSong(int id);

    public Song getSong(int id) throws ServerException;

    public Set<Song> getSongs();

    public Multimap<String, Song> getSongsFromComposers(List<String> composers);

    public Multimap<String, Song> getSongsFromWordsAuthors(List<String> wordsAuthors);

    public List<Song> getSongsFromPerformer(String performer);

}
