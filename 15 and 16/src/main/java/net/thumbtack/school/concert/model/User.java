package net.thumbtack.school.concert.model;

import lombok.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@EqualsAndHashCode(callSuper = false)
public class User extends AbstractUser {
    @NonNull
    private String lastName;
    @NonNull
    private String firstName;
    @NonNull
    private String login;
    @NonNull
    private String password;
    private Set<Rating> ratedSongs = new HashSet<>();

    public void removeSong(Song song){
        getSongs().remove(song);
    }

    public void addRating(Rating rating){
        this.ratedSongs.add(rating);
    }

    public void removeRating(Rating rating){
        this.ratedSongs.remove(rating);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(lastName, user.lastName) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(ratedSongs, user.ratedSongs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName, login, password);
    }
}
