package net.thumbtack.school.concert.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class AddSongDtoResponse {
    private int id;
}
