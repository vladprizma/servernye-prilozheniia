package net.thumbtack.school.concert.model;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
public class Community extends AbstractUser {
    private static Community community;

    private Community(){};

    public static Community getCommunity(){
        if(community == null){
            community = new Community();
        }
        return community;
    }

}
