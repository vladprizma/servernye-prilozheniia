package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

@Getter
@AllArgsConstructor
public class RegisterDtoRequest {
    private String lastName;
    private String firstName;
    private String login;
    private String password;

    public static void validate(RegisterDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest.getFirstName() == null ||
                dtoRequest.getFirstName().equals("") ||
                dtoRequest.getLastName() == null ||
                dtoRequest.getLastName().equals("") ||
                dtoRequest.getLogin() == null ||
                dtoRequest.getLogin().equals("") ||
                dtoRequest.getPassword() == null ||
                dtoRequest.getPassword().equals("")) {

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }

    }

}
