package net.thumbtack.school.threads.task;

public class TaskReader implements Runnable{
    private TaskQueue taskQueue;

    public TaskReader(TaskQueue taskQueue){
        this.taskQueue = taskQueue;
    }

    @Override
    public void run() {
        System.out.println("Reader Started");
        Executable task;
        try {
            while (true) {
                task = taskQueue.take();
                task.execute();
                if (task.equals(Executable.poison)) {
                    System.out.println("Reader finished");
                    break;
                }
                Task taskCover = (Task) task;
                System.out.println("Reader has done the task: " + taskCover.getFirstTerm() + " + " + taskCover.getSecondTerm() + " = " + taskCover.getSum());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
