package net.thumbtack.school.threads;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyConcurrentHashMap<K,V> extends HashMap<K,V> {
    private static final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Map<K,V> map;

    public MyConcurrentHashMap(){
        map = new HashMap<>();
    }

    public V get(Object key){
        synchronized (map) {
            return map.get(key);
        }
    }

    public V put(K key, V value){
        synchronized (map){
            return map.put(key, value);
        }
    }

    public V remove(Object key){
        synchronized (map){
            return map.remove(key);
        }
    }

}
