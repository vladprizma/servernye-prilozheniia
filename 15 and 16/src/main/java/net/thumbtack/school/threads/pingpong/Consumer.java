package net.thumbtack.school.threads.pingpong;

public class Consumer extends Thread{
    private PingPong pingPong;

    public Consumer(PingPong pingPong) {
        this.pingPong = pingPong;
    }

    public void run() {
        for (int i = 0; i < 100; i++){
            pingPong.pong();
        }
    }
}
