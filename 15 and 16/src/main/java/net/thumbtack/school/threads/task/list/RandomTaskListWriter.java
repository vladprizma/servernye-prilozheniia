package net.thumbtack.school.threads.task.list;

import net.thumbtack.school.threads.task.Task;
import net.thumbtack.school.threads.task.TaskQueue;

import java.util.Arrays;
import java.util.Random;

public class RandomTaskListWriter implements Runnable{
    private TaskQueue taskQueue;
    private int taskAmount;
    private DoneTaskAndWritersQueue doneTaskAndWritersQueue;
    private Random rand;

    public RandomTaskListWriter(TaskQueue taskQueue, int taskAmount, DoneTaskAndWritersQueue doneTaskAndWritersQueue, Random rand){
        this.taskQueue = taskQueue;
        this.taskAmount = taskAmount;
        this.doneTaskAndWritersQueue = doneTaskAndWritersQueue;
        this.rand = rand;
    }

    @Override
    public void run() {
        try{
            int chance = rand.nextInt(100);
            if(chance < 30){
                System.out.println("RandomWriter is creating RandomWriter");
                Thread newThread = new Thread(new RandomTaskListWriter(taskQueue, taskAmount, doneTaskAndWritersQueue, rand));
                doneTaskAndWritersQueue.put(new TasksAndWriters(newThread));
                doneTaskAndWritersQueue.incAmountOfWriters();
                newThread.start();
            }
            else {
                System.out.println("RandomWriter is creating tasks");
                for (int i = 0; i < taskAmount; i++) {
                    taskQueue.put(new MultiTask(Arrays.asList(
                            new Task(1, 2),
                            new Task(2, 2),
                            new Task(2, 3))));
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("RandomWriter finished");
    }

}
