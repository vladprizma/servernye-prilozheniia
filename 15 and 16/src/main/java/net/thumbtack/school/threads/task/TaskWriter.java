package net.thumbtack.school.threads.task;

public class TaskWriter implements Runnable{
    private TaskQueue taskQueue;
    private int taskAmount;

    public TaskWriter(TaskQueue taskQueue, int taskAmount){
        this.taskQueue = taskQueue;
        this.taskAmount = taskAmount;
    }

    @Override
    public void run() {
        System.out.println("Writer Started");
        try{
            for(int i = 0; i < taskAmount; i++){
                taskQueue.put(new Task(1,2));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Writer finished");
    }

}
