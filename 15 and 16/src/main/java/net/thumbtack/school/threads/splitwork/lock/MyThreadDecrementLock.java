package net.thumbtack.school.threads.splitwork.lock;

import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Lock;

public class MyThreadDecrementLock extends Thread{
    private Random rnd = new Random(System.currentTimeMillis());
    private final List<Integer> count;
    private final Lock lock;

    public MyThreadDecrementLock(List<Integer> count, Lock lock) {
        this.count = count;
        this.lock = lock;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            lock.lock();
            try {
                if (!count.isEmpty()) {
                    count.remove(rnd.nextInt(count.size()));
                }
            } finally {
                lock.unlock();
            }
        }
    }

}
