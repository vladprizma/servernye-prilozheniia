package net.thumbtack.school.threads.data;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class DataQueue {
    private static final int QUEUE_LENGTH = 5;
    public static final Data POISON = new Data(new int[]{});

    private BlockingQueue<Data> queue = new ArrayBlockingQueue<>(QUEUE_LENGTH);

    public void put(Data data) throws InterruptedException {
        this.queue.put(data);
    }

    public Data take() throws InterruptedException {
        return this.queue.take();
    }

    public void poison() throws InterruptedException {
        this.queue.put(POISON);
    }

}
