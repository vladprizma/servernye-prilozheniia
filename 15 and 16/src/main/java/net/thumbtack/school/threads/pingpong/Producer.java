package net.thumbtack.school.threads.pingpong;

public class Producer extends Thread{

    private PingPong pingPong;

    public Producer(PingPong pingPong) {
        this.pingPong = pingPong;
    }

    public void run() {
        for (int i = 0; i < 100; i++){
            pingPong.ping();
        }
    }

}
