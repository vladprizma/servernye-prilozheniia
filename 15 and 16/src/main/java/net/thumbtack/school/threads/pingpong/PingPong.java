package net.thumbtack.school.threads.pingpong;

import java.util.concurrent.Semaphore;

public class PingPong {
    private static Semaphore semCon = new Semaphore(0);
    private static Semaphore semProd = new Semaphore(1);

    public void ping() {
        try {
            semCon.acquire();
            System.out.println("Ping");
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        } finally {
            semProd.release();
        }
    }

    public void pong() {
        try {
            semProd.acquire();
            System.out.println("Pong");
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        } finally {
            semCon.release();
        }
    }

}
