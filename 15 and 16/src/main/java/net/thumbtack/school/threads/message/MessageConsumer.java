package net.thumbtack.school.threads.message;

public class MessageConsumer implements Runnable{

    private Transport transport;

    public MessageConsumer(Transport transport) {
        this.transport = transport;
    }

    public void run() {
        System.out.println("Consumer Started");
        Message message = null;
        while (true) {
            try {
                message = transport.take();
                if (message.getEmailAddress() == null) {
                    System.out.println("Consumer finished");
                    break;
                }
                System.out.println("Consumer send email to : " + message.getEmailAddress());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
