package net.thumbtack.school.threads.splitwork;

import java.util.List;
import java.util.Random;

public class MyThreadDecrement extends Thread{

    private Random rnd = new Random(System.currentTimeMillis());
    private final List<Integer> count;

    public MyThreadDecrement(List<Integer> count) {
        this.count = count;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            synchronized(count) {
                if(!count.isEmpty()) {
                    count.remove(rnd.nextInt(count.size()));
                }
            }
        }
    }


}
