package net.thumbtack.school.threads.data;

import java.util.Arrays;

public class DataReader implements Runnable{
    private DataQueue dataQueue;

    public DataReader(DataQueue dataQueue){
        this.dataQueue = dataQueue;
    }

    @Override
    public void run() {
        System.out.println("Reader Started");
        Data data;
        try {
            while (true) {
                data = dataQueue.take();
                if (data.equals(DataQueue.POISON)) {
                    System.out.println("Reader finished");
                    break;
                }
                System.out.println("Reader read : " + Arrays.toString(data.get()));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
