package net.thumbtack.school.threads.data;

public class Data {
    private int[] arr;

    public Data(int[] arr){
        this.arr = arr;
    }

    public int[] get(){
        return arr;
    }

}
