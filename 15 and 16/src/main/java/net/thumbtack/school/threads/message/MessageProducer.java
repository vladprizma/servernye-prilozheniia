package net.thumbtack.school.threads.message;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class MessageProducer implements Runnable{

    private Transport transport;
    private File file;
    private int poison;

    public MessageProducer(File file, Transport transport, int poison) {
        this.transport = transport;
        this.file = file;
        this.poison = poison;
    }

    public void addPoison(){
        try {
            transport.send(new Message(null, null, null, null));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        System.out.println("Producer Started");
        String address = "";
        try (BufferedReader inputStream = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            while((address = inputStream.readLine()) != null) {
                transport.send(new Message(address, "Me", "Cinema", "Hi!"));
            }
            for(int i = 0; i < poison; i++){
                transport.send(new Message(null, null, null, null));
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Producer finished");
    }

}
