package net.thumbtack.school.threads.message;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Transport {
    private static final int QUEUE_LENGTH = 5;
    private BlockingQueue<Message> queue = new ArrayBlockingQueue<>(QUEUE_LENGTH);

    public void send(Message message) throws InterruptedException {
        this.queue.put(message);
    }

    public Message take() throws InterruptedException {
        return this.queue.take();
    }

    public BlockingQueue getQueue(){
        return this.queue;
    }
}
