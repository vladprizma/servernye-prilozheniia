package net.thumbtack.school.threads.task.list;

import net.thumbtack.school.threads.task.Executable;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class DoneTaskAndWritersQueue {
    private static final int QUEUE_LENGTH = 5;

    private BlockingQueue<TasksAndWriters> queue = new ArrayBlockingQueue<>(QUEUE_LENGTH);
    private int allTasksAmount;
    private AtomicInteger allWriterAmount;

    public DoneTaskAndWritersQueue(int allTasks){
        this.allTasksAmount = allTasks;
    }

    public DoneTaskAndWritersQueue(int allTasksAmount, AtomicInteger allWriterAmount){
        this.allTasksAmount = allTasksAmount;
        this.allWriterAmount = allWriterAmount;
    }

    public void incAmountOfWriters(){
        this.allWriterAmount.incrementAndGet();
    }

    public void put(TasksAndWriters task) throws InterruptedException {
        this.queue.put(task);
    }

    public TasksAndWriters take() throws InterruptedException {
        return this.queue.take();
    }

    public int getSumOfTasksAndWritersAmount(){ return allTasksAmount + allWriterAmount.get();}

    public int getAllTasksAmount(){
        return allTasksAmount;
    }

}
