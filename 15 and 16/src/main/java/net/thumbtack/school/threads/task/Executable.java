package net.thumbtack.school.threads.task;

public interface Executable {
    Executable poison = () -> {};

    void execute();

}
