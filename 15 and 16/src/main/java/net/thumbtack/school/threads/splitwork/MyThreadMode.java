package net.thumbtack.school.threads.splitwork;

public class MyThreadMode extends Thread{
    public enum Mode{
        INCREMENT,
        DECREMENT,
        NOT_SYNC_INCREMENT,
        NOT_SYNC_DECREMENT,
    }

    private final ListIncDec count;
    private Mode mode;

    public MyThreadMode(ListIncDec count, Mode mode) {
        this.count = count;
        this.mode = mode;
    }

    public void run(){
        synchronized (count) {
            switch (mode) {
                case INCREMENT:
                    for (int i = 0; i < 10000; i++) {
                        count.increment();
                    }
                    break;
                case DECREMENT:
                    for (int i = 0; i < 10000; i++) {
                        count.decrement();
                    }
                    break;
                case NOT_SYNC_INCREMENT:
                    for (int i = 0; i < 10000; i++) {
                        count.incrementNotSync();
                    }
                    break;
                case NOT_SYNC_DECREMENT:
                    for (int i = 0; i < 10000; i++) {
                        count.decrementNotSync();
                    }
                    break;
            }
        }
    }


}
