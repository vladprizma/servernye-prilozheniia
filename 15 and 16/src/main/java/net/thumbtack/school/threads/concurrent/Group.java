package net.thumbtack.school.threads.concurrent;

import java.util.*;

public class Group {
    private String name;
    private String room;
    private List<Trainee> list;

    public Group(String name, String room) throws TrainingException {
        setName(name);
        setRoom(room);
        list = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws TrainingException {
        if(name == null || name.equals("")){
            throw new TrainingException(TrainingErrorCode.GROUP_WRONG_NAME);
        }
        this.name = name;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) throws TrainingException {
        if(room == null || room.equals("")){
            throw new TrainingException(TrainingErrorCode.GROUP_WRONG_ROOM);
        }
        this.room = room;
    }

    public List<Trainee> getTrainees(){
        return list;
    }

    public void addTrainee(Trainee trainee){
        this.list.add(trainee);
    }

    public void removeTrainee(Trainee trainee) throws TrainingException {
        if(!list.remove(trainee)){
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
    }

    public void removeTrainee(int index) throws TrainingException {
        if(index < 0 || index >= list.size()){
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
        list.remove(index);
    }

    public Trainee getTraineeByFirstName(String firstName) throws TrainingException {
        for(Trainee elem : list){
            if(elem.getFirstName().equals(firstName)){
                return elem;
            }
        }
        throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
    }

    public Trainee getTraineeByFullName(String fullName) throws TrainingException {
        for(Trainee elem : list){
            if(elem.getFullName().equals(fullName)){
                return elem;
            }
        }
        throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
    }

    public void sortTraineeListByFirstNameAscendant(){
        list.sort((elem1, elem2) -> elem1.getFirstName().compareTo(elem2.getFirstName()));
    }

    public void sortTraineeListByRatingDescendant(){
        list.sort((elem1, elem2) -> -Integer.compare(elem1.getRating(),elem2.getRating()));
    }

    public void reverseTraineeList(){
        Collections.reverse(list);
    }

    public void rotateTraineeList(int positions){
        Collections.rotate(list,positions);
    }

    public List<Trainee> getTraineesWithMaxRating() throws TrainingException {
        if(list.size() == 0){
            throw new TrainingException(TrainingErrorCode.TRAINEE_NOT_FOUND);
        }
        List<Trainee> buf = new ArrayList<>();
        for(Trainee elem : list){
            if(buf.size() != 0 && (elem.getRating() > buf.get(0).getRating())) {
                buf.clear();
            }
            if(buf.size() == 0 || elem.getRating() >= buf.get(0).getRating()) {
                buf.add(elem);
            }
        }
        return buf;
    }

    public boolean hasDuplicates(){
        Set<Trainee> set = new HashSet<>();
        for (Trainee elem: list){
            if (!set.add(elem)){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(name, group.name) &&
                Objects.equals(room, group.room) &&
                Objects.equals(list, group.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, room, list);
    }
}
