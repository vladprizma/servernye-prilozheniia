package net.thumbtack.school.threads;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {
    private static ThreadLocal<SimpleDateFormat> dateThreadLocal = new ThreadLocal<>();
    private SimpleDateFormat pattern;

    public Formatter(String pattern){
        this.pattern = new SimpleDateFormat(pattern);
    }

    public String format(Date date){
        dateThreadLocal.set(pattern);
        return dateThreadLocal.get().format(date);
    }

}