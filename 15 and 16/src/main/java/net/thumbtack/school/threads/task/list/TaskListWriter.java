package net.thumbtack.school.threads.task.list;

import net.thumbtack.school.threads.task.Task;
import net.thumbtack.school.threads.task.TaskQueue;

import java.util.Arrays;


public class TaskListWriter implements Runnable{
    private TaskQueue taskQueue;
    private int taskAmount;

    public TaskListWriter(TaskQueue taskQueue, int taskAmount){
        this.taskQueue = taskQueue;
        this.taskAmount = taskAmount;
    }

    @Override
    public void run() {
        System.out.println("Writer is creating tasks");
        try{
            for(int i = 0; i < taskAmount; i++){
                taskQueue.put(new MultiTask(Arrays.asList(
                        new Task(1,2),
                        new Task(2,2),
                        new Task(2,3))));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Writer finished");
    }

}
