package net.thumbtack.school.threads.splitwork;

import java.util.List;
import java.util.Random;

public class ListIncDec {
    private Random rnd = new Random(System.currentTimeMillis());
    private final List<Integer> count;

    public ListIncDec(List<Integer> count) {
        this.count = count;
    }

    public synchronized void increment(){
        count.add(rnd.nextInt());
    }

    public synchronized void decrement(){
        if(!count.isEmpty()) {
            count.remove(rnd.nextInt(count.size()));
        }
    }

    public void incrementNotSync(){
        count.add(rnd.nextInt());
    }

    public void decrementNotSync(){
        if(!count.isEmpty()) {
            int max = count.size();
            count.remove(rnd.nextInt(max));
        }
    }



}
