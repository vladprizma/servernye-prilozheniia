package net.thumbtack.school.threads.task.list;

import net.thumbtack.school.threads.task.Executable;
import net.thumbtack.school.threads.task.TaskQueue;

public class TaskListReader implements Runnable{
    private TaskQueue taskQueue;
    private DoneTaskAndWritersQueue doneTaskAndWritersQueue;

    public TaskListReader(TaskQueue taskQueue, DoneTaskAndWritersQueue doneTaskAndWritersQueue){
        this.taskQueue = taskQueue;
        this.doneTaskAndWritersQueue = doneTaskAndWritersQueue;
    }

    @Override
    public void run() {
        System.out.println("Reader is solving tasks");
        try {
            Executable task = taskQueue.take();
            while (task != Executable.poison) {
               task.execute();
               if(task instanceof MultiTask && !((MultiTask) task).isTasksDone()){
                   taskQueue.put(task);
               }
               else{
                   doneTaskAndWritersQueue.put(new TasksAndWriters(task));
               }
               task = taskQueue.take();
            }
            System.out.println("Reader finished");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
