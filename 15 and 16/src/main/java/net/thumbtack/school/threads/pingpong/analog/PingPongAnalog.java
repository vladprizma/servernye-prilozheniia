package net.thumbtack.school.threads.pingpong.analog;

import net.thumbtack.school.threads.pingpong.PingPong;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PingPongAnalog extends PingPong {
    private static final Lock lock = new ReentrantLock();
    private static final Condition ping = lock.newCondition();
    private static final Condition pong = lock.newCondition();
    private boolean isPong = true;
    private boolean isPing = false;

    public void ping() {
        lock.lock();
        try {
            if(!isPong)
                pong.await();
            System.out.println("PingAnalog");
            isPing = true;
            isPong = false;
            ping.signal();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        } finally {
            lock.unlock();
        }
    }

    public void pong() {
        lock.lock();
        try {
            if(!isPing)
                ping.await();
            System.out.println("PongAnalog");
            isPong = true;
            isPing = false;
            pong.signal();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        } finally {
            lock.unlock();
        }
    }


}
