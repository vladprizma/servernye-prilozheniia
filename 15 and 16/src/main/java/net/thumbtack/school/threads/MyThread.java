package net.thumbtack.school.threads;

import net.thumbtack.school.threads.data.DataQueue;
import net.thumbtack.school.threads.data.DataReader;
import net.thumbtack.school.threads.data.DataWriter;
import net.thumbtack.school.threads.message.MessageConsumer;
import net.thumbtack.school.threads.message.MessageProducer;
import net.thumbtack.school.threads.message.Transport;
import net.thumbtack.school.threads.pingpong.Consumer;
import net.thumbtack.school.threads.pingpong.PingPong;
import net.thumbtack.school.threads.pingpong.Producer;
import net.thumbtack.school.threads.pingpong.analog.PingPongAnalog;
import net.thumbtack.school.threads.splitwork.ListIncDec;
import net.thumbtack.school.threads.splitwork.MyThreadDecrement;
import net.thumbtack.school.threads.splitwork.MyThreadIncrement;
import net.thumbtack.school.threads.splitwork.MyThreadMode;
import net.thumbtack.school.threads.splitwork.lock.MyThreadDecrementLock;
import net.thumbtack.school.threads.splitwork.lock.MyThreadIncrementLock;
import net.thumbtack.school.threads.task.*;
import net.thumbtack.school.threads.task.list.DoneTaskAndWritersQueue;
import net.thumbtack.school.threads.task.list.RandomTaskListWriter;
import net.thumbtack.school.threads.task.list.TaskListReader;
import net.thumbtack.school.threads.task.list.TaskListWriter;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MyThread extends Thread{

    public MyThread() {

    }

    public void run(){
        System.out.println("Thread started");
        try{
            for(int i = 0; i < 3; i++){
                Thread.sleep(10);
            }
        } catch(InterruptedException ex){
            System.out.println("Thread was interrupted");
        }
        System.out.println("Thread finished");
    }

    // 1
    public static void currentThreadInfo(){
        Thread thread = new Thread();
        System.out.println(thread.getId());
        System.out.println(thread.getName());
        System.out.println(thread.getPriority());
        System.out.println(thread.getState());
        System.out.println(thread.isAlive());
        System.out.println(thread.isInterrupted());
    }

    // 2
    public static void createThreadAndWaitUntilFinished(){
        Runnable runnable = () -> {
            try {
                System.out.println("Second task started");
            } finally{
                System.out.println("Second task finished");
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        try{
            thread.join();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 3
    public static void createThreeThreadsAndWaitUntilFinished(){
        Thread myThread3 = new MyThread();
        Thread myThread4 = new MyThread();
        Thread myThread5 = new MyThread();
        myThread3.start();
        myThread4.start();
        myThread5.start();

        try{
            myThread3.join();
            myThread4.join();
            myThread5.join();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 4
    public static void threadsWorkTogether(){
        List<Integer> list = new ArrayList<>();
        MyThreadIncrement increment = new MyThreadIncrement(list);
        MyThreadDecrement decrement = new MyThreadDecrement(list);
        increment.start();
        decrement.start();

        try{
            increment.join();
            decrement.join();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 5
    public static void threadsWorkTogetherReduction(){
        List<Integer> list = new ArrayList<>();
        ListIncDec listIncDec = new ListIncDec(list);

        MyThreadMode increment = new MyThreadMode(listIncDec, MyThreadMode.Mode.INCREMENT);
        MyThreadMode decrement = new MyThreadMode(listIncDec, MyThreadMode.Mode.DECREMENT);
        increment.start();
        decrement.start();

        try{
            increment.join();
            decrement.join();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 6
    public static void threadsWorkTogetherReductionSyncList(){
        List<Integer> list = Collections.synchronizedList(new ArrayList<>());
        ListIncDec listIncDec = new ListIncDec(list);
        MyThreadMode increment = new MyThreadMode(listIncDec, MyThreadMode.Mode.NOT_SYNC_INCREMENT);
        MyThreadMode decrement = new MyThreadMode(listIncDec, MyThreadMode.Mode.NOT_SYNC_DECREMENT);
        increment.start();
        decrement.start();

        try{
            increment.join();
            decrement.join();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 7
    public static void pingPong(){
        PingPong pingPong = new PingPong();
        Consumer consumer = new Consumer(pingPong);
        Producer producer = new Producer(pingPong);

        consumer.start();
        producer.start();

        try{
            consumer.join();
            producer.join();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 8
    public static void readerWriter(){
        ReadWriteLock lock = new ReentrantReadWriteLock();
        Runnable writeTask = () -> {
            lock.writeLock().lock();
            try {
                System.out.println("  Writer thread id: " + Thread.currentThread().getId());
            } finally {
                System.out.println("  Write lock unlocked by thread with id: " + Thread.currentThread().getId());
                lock.writeLock().unlock();
            }
        };

        Runnable readTask = () -> {
            lock.readLock().lock();
            try {
                System.out.println("Reader thread id: " + Thread.currentThread().getId());
            } finally {
                System.out.println("Read lock unlocked by thread with id: " + Thread.currentThread().getId());
                lock.readLock().unlock();
            }
        };

        Thread thread1 = new Thread(writeTask);
        Thread thread2 = new Thread(writeTask);
        Thread thread3 = new Thread(readTask);
        Thread thread4 = new Thread(readTask);
        Thread thread5 = new Thread(readTask);

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();


        try{
            thread1.join();
            thread2.join();
            thread3.join();
            thread4.join();
            thread5.join();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 10
    public static void threadsWorkTogetherLock(){
        Lock lock = new ReentrantLock();
        List<Integer> list = new ArrayList<>();
        MyThreadIncrementLock increment = new MyThreadIncrementLock(list, lock);
        MyThreadDecrementLock decrement = new MyThreadDecrementLock(list, lock);
        increment.start();
        decrement.start();

        try{
            increment.join();
            decrement.join();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 11
    public static void pingPongAnalog(){
        PingPongAnalog pingPong = new PingPongAnalog();
        Consumer consumer = new Consumer(pingPong);
        Producer producer = new Producer(pingPong);

        consumer.start();
        producer.start();

        try{
            consumer.join();
            producer.join();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 13
    public static void formatDate(){
        Date date = new Date();
        Formatter formatter = new Formatter("dd/MM/yyyy");

        Runnable runnable = () -> System.out.println(formatter.format(date));

        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        Thread thread3 = new Thread(runnable);
        Thread thread4 = new Thread(runnable);
        Thread thread5 = new Thread(runnable);

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();

        try{
            thread1.join();
            thread2.join();
            thread3.join();
            thread4.join();
            thread5.join();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 14
    public static File fileCreate(){
        String email1 = "email@mail.ru";
        String email2 = "email@yandex.ru";
        String email3 = "itsyandexe@yandex.ru";
        String email4 = "email@gmail.com";
        String email5 = "itsgmail@gmail.com";
        String email6 = "email@yahoo.com";
        String email7 = "itsmail@mail.ru";
        String email8 = "notgmail@mail.ru";

        File file = new File("emails.txt");
        try {
            if(file.createNewFile()) {
                try (BufferedWriter outputStream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8))) {
                    outputStream.write(email1);
                    outputStream.newLine();
                    outputStream.write(email2);
                    outputStream.newLine();
                    outputStream.write(email3);
                    outputStream.newLine();
                    outputStream.write(email4);
                    outputStream.newLine();
                    outputStream.write(email5);
                    outputStream.newLine();
                    outputStream.write(email6);
                    outputStream.newLine();
                    outputStream.write(email7);
                    outputStream.newLine();
                    outputStream.write(email8);
                    outputStream.newLine();
                }

            }
        }
        catch(IOException e) {
            System.out.println("Exception Occurred:");
            e.printStackTrace();
        }
        return file;
    }

    // 14
    public static void fileDelete(File file){
        file.deleteOnExit();
    }

    // 14
    public static void sendMessage(File file){
        Transport transport = new Transport();

        int threadAmount = 5;
        ExecutorService es = Executors.newFixedThreadPool(threadAmount);

        Thread threadProducer1 = new Thread(new MessageProducer(file, transport, threadAmount));

        threadProducer1.start();

        for(int i = 0; i < threadAmount; i++){
            es.execute(new MessageConsumer(transport));
        }

        try{
            threadProducer1.join();
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }

        es.shutdown();
    }

    // 15
    public static void dataQueue(){
        DataQueue dataQueue = new DataQueue();

        int dataAmount = 5;
        int threadAmount = 5;
        List<Thread> writerThreads = new ArrayList<>();
        List<Thread> readerThreads = new ArrayList<>();

        for(int i = 0; i < threadAmount; i++){
            writerThreads.add(new Thread(new DataWriter(dataQueue, dataAmount)));
            readerThreads.add(new Thread(new DataReader(dataQueue)));
            writerThreads.get(i).start();
            readerThreads.get(i).start();
        }

        try{
            for(Thread thread : writerThreads){
                thread.join();
            }

            for(int i = 0; i < threadAmount; i++) {
                dataQueue.poison();
            }

            for(Thread thread : readerThreads){
                thread.join();
            }
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 16
    public static void taskExecute(){
        TaskQueue taskQueue = new TaskQueue();

        int taskAmount = 5;
        int threadAmount = 5;
        List<Thread> writerThreads = new ArrayList<>();
        List<Thread> readerThreads = new ArrayList<>();

        for(int i = 0; i < threadAmount; i++){
            writerThreads.add(new Thread(new TaskWriter(taskQueue, taskAmount)));
            readerThreads.add(new Thread(new TaskReader(taskQueue)));
            writerThreads.get(i).start();
            readerThreads.get(i).start();
        }

        try{
            for(Thread thread : writerThreads){
                thread.join();
            }

            for(int i = 0; i < threadAmount; i++) {
                taskQueue.poison();
            }

            for(Thread thread : readerThreads){
                thread.join();
            }
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    // 17
    public static void taskListExecute() {
        TaskQueue taskQueue = new TaskQueue();

        int taskAmount = 5;
        int threadAmount = 5;
        List<Thread> writerThreads = new ArrayList<>();
        List<Thread> readerThreads = new ArrayList<>();

        DoneTaskAndWritersQueue doneTaskAndWritersQueue = new DoneTaskAndWritersQueue(taskAmount*threadAmount);
        int countDoneTasks = 0;

        for(int i = 0; i < threadAmount; i++){
            writerThreads.add(new Thread(new TaskListWriter(taskQueue, taskAmount)));
            readerThreads.add(new Thread(new TaskListReader(taskQueue, doneTaskAndWritersQueue)));
            writerThreads.get(i).start();
            readerThreads.get(i).start();
        }

        try{
            while(doneTaskAndWritersQueue.getAllTasksAmount() != countDoneTasks) {
                doneTaskAndWritersQueue.take();
                System.out.println(countDoneTasks + " : " + doneTaskAndWritersQueue.getAllTasksAmount());
                countDoneTasks++;
            }
            System.out.println(countDoneTasks + " : " + doneTaskAndWritersQueue.getAllTasksAmount());
            for (int i = 0; i < threadAmount; i++) {
                taskQueue.poison();
            }

            for(Thread thread : readerThreads){
                thread.join();
            }
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    //18
    public static void randomTaskListExecute(){
        TaskQueue taskQueue = new TaskQueue();

        int taskAmount = 5;
        int threadAmount = 5;
        List<Thread> writerThreads = new ArrayList<>();
        List<Thread> readerThreads = new ArrayList<>();
        AtomicInteger counterOfWriters = new AtomicInteger(0); // 0 т.к. изначальные потоки (threadAmount) не добавляются в doneTaskAndWritersQueue
        Random rand = new Random();

        DoneTaskAndWritersQueue doneTaskAndWritersQueue = new DoneTaskAndWritersQueue(taskAmount*threadAmount, counterOfWriters);
        int countDoneTasksAndWriters = 0;

        for(int i = 0; i < threadAmount; i++){
            writerThreads.add(new Thread(new RandomTaskListWriter(taskQueue, taskAmount, doneTaskAndWritersQueue, rand)));
            readerThreads.add(new Thread(new TaskListReader(taskQueue, doneTaskAndWritersQueue)));
            writerThreads.get(i).start();
            readerThreads.get(i).start();
        }

        try{
            while(doneTaskAndWritersQueue.getSumOfTasksAndWritersAmount() != countDoneTasksAndWriters) {
                doneTaskAndWritersQueue.take();
                System.out.println(countDoneTasksAndWriters + " : " + doneTaskAndWritersQueue.getSumOfTasksAndWritersAmount());
                countDoneTasksAndWriters++;
            }
            System.out.println(countDoneTasksAndWriters + " : " + doneTaskAndWritersQueue.getSumOfTasksAndWritersAmount());
            for (int i = 0; i < threadAmount; i++) {
                taskQueue.poison();
            }

            for(Thread thread : readerThreads){
                thread.join();
            }
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // 1
        currentThreadInfo();
        // 2
        createThreadAndWaitUntilFinished();
        // 3
        createThreeThreadsAndWaitUntilFinished();
        // 4
        threadsWorkTogether();
        // 5
        threadsWorkTogetherReduction();
        // 6
        threadsWorkTogetherReductionSyncList();
        // 7
        pingPong();
        // 8
        readerWriter();
        // 10
        threadsWorkTogetherLock();
        // 11
        pingPongAnalog();
        // 12
        // Будет хуже, т.к. реализация ConcurredHashMap блокирует не весь массив, а только букет.
        // А так же один поток, при работе с ConcurredHashMap модифицирует элементы в одном букете, а другой поток - в другом
        // 13
        formatDate();
        // 14
        File file = fileCreate();
        sendMessage(file);
        fileDelete(file);
        // 15
        dataQueue();
        // 16
        taskExecute();
        // 17
        taskListExecute();
        // 18
        randomTaskListExecute();
    }
}