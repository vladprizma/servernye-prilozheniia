package net.thumbtack.school.threads.task;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class TaskQueue {
    private static final int QUEUE_LENGTH = 5;

    private BlockingQueue<Executable> queue = new ArrayBlockingQueue<>(QUEUE_LENGTH);

    public void put(Executable task) throws InterruptedException {
        this.queue.put(task);
    }

    public Executable take() throws InterruptedException {
        return this.queue.take();
    }

    public void poison() throws InterruptedException {
        this.queue.put(Executable.poison);
    }

    public boolean isEmpty(){
        return this.queue.isEmpty();
    }

}
