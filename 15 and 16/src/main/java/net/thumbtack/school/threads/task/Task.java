package net.thumbtack.school.threads.task;

import java.util.ArrayList;
import java.util.List;

public class Task implements Executable {
    private int sum;
    private int firstTerm;
    private int secondTerm;

    public Task(int firstTerm, int secondTerm){
        this.firstTerm = firstTerm;
        this.secondTerm = secondTerm;
    }

    @Override
    public void execute() {
        sum = firstTerm + secondTerm;
    }

    public int getSum() {
        return sum;
    }

    public int getFirstTerm() {
        return firstTerm;
    }

    public int getSecondTerm() {
        return secondTerm;
    }


}
