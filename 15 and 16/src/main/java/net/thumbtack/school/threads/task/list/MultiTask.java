package net.thumbtack.school.threads.task.list;

import net.thumbtack.school.threads.task.Executable;
import net.thumbtack.school.threads.task.Task;

import java.util.List;

public class MultiTask implements Executable {
    private List<Executable> tasks;
    private int currentTask;

    public MultiTask(List<Executable> tasks) {
        this.tasks = tasks;
        this.currentTask = 0;
    }

    @Override
    public void execute() {
        tasks.get(currentTask).execute();
        currentTask++;
    }

    public boolean isTasksDone(){
        return currentTask == tasks.size();
    }

    public boolean isEmpty() {
        return tasks.isEmpty();
    }

}