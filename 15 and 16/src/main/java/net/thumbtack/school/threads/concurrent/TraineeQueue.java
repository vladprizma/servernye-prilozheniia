package net.thumbtack.school.threads.concurrent;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class TraineeQueue {
    private Queue<Trainee> queue;

    public TraineeQueue(){
        queue = new ConcurrentLinkedQueue<>();
    }

    public void addTrainee(Trainee trainee){
        queue.add(trainee);
    }

    public Trainee removeTrainee() throws TrainingException {
        Trainee buf;
        if((buf = queue.poll())==null){
            throw new TrainingException(TrainingErrorCode.EMPTY_TRAINEE_QUEUE);
        }
        return buf;
    }

    public boolean isEmpty(){
        return queue.peek() == null;
    }

}
