package net.thumbtack.school.threads.data;

public class DataWriter implements Runnable{
    private DataQueue dataQueue;
    private int dataAmount;

    public DataWriter(DataQueue dataQueue, int dataAmount){
        this.dataQueue = dataQueue;
        this.dataAmount = dataAmount;
    }

    @Override
    public void run() {
        System.out.println("Writer Started");
        try{
            for(int i = 0; i < dataAmount; i++){
                dataQueue.put(new Data(new int[]{1,2,3}));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Writer finished");
    }
}
