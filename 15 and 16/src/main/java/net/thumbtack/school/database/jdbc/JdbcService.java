package net.thumbtack.school.database.jdbc;

import com.google.common.collect.ArrayListMultimap;
import com.mysql.jdbc.Statement;
import net.thumbtack.school.database.model.Group;
import net.thumbtack.school.database.model.School;
import net.thumbtack.school.database.model.Subject;
import net.thumbtack.school.database.model.Trainee;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


public class JdbcService {


    public static void insertTrainee(Trainee trainee) throws SQLException {
        String insertQuery = "insert into trainee values(?,?,?,?,?)";
        int result;
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setNull(1, java.sql.Types.INTEGER);
            stmt.setString(2, trainee.getFirstName());
            stmt.setString(3, trainee.getLastName());
            stmt.setInt(4, trainee.getRating());
            stmt.setNull(5,java.sql.Types.INTEGER);
            result = stmt.executeUpdate();
            if (result > 0) {
                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    trainee.setId(rs.getInt(1));
                    rs.close();
                }
            }
        }
    }


    public static void updateTrainee(Trainee trainee) throws SQLException {
        String updateQuery = "update trainee set firstname = ?, lastname = ?, rating = ? where id = ?";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(updateQuery)) {
            stmt.setString(1, trainee.getFirstName());
            stmt.setString(2, trainee.getLastName());
            stmt.setInt(3, trainee.getRating());
            stmt.setInt(4, trainee.getId());
            stmt.executeUpdate();
        }
    }


    public static Trainee getTraineeByIdUsingColNames(int traineeId) throws SQLException {
        String selectQuery = "select * from trainee where id = ?";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(selectQuery)) {
            stmt.setInt(1, traineeId);
            try (ResultSet rs = stmt.executeQuery()) {

                if (rs.next()) {
                    return new Trainee(
                            rs.getInt("id"),
                            rs.getString("firstName"),
                            rs.getString("lastName"),
                            rs.getInt("rating"));
                }
                return null;
            }
        }
    }


    public static Trainee getTraineeByIdUsingColNumbers(int traineeId) throws SQLException {
        String selectQuery = "select * from trainee where id = ?";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(selectQuery)) {
            stmt.setInt(1, traineeId);
            try (ResultSet rs = stmt.executeQuery()) {

                if (rs.next()) {
                    return new Trainee(
                            rs.getInt(1),
                            rs.getString(2),
                            rs.getString(3),
                            rs.getInt(4));
                }
                return null;
            }
        }
    }


    public static List<Trainee> getTraineesUsingColNames() throws SQLException {
        String selectQuery = "select * from trainee";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(selectQuery);
             ResultSet rs = stmt.executeQuery()) {

            List<Trainee> trainees = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                int rating = rs.getInt("rating");
                trainees.add(new Trainee(id, firstName, lastName, rating));
            }
            return trainees;
        }
    }


    public static List<Trainee> getTraineesUsingColNumbers() throws SQLException {
        String selectQuery = "select * from trainee";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(selectQuery);
             ResultSet rs = stmt.executeQuery()) {

            List<Trainee> trainees = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt(1);
                String firstName = rs.getString(2);
                String lastName = rs.getString(3);
                int rating = rs.getInt(4);
                trainees.add(new Trainee(id, firstName, lastName, rating));
            }
            return trainees;
        }
    }


    public static void deleteTrainee(Trainee trainee) throws SQLException {
        String deleteQuery = "delete from trainee where id = ?";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(deleteQuery)) {
            stmt.setInt(1, trainee.getId());
            stmt.executeUpdate();
        }
    }


    public static void deleteTrainees() throws SQLException {
        String deleteQuery = "delete from trainee";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(deleteQuery)) {
            stmt.executeUpdate();
        }
    }


    public static void insertSubject(Subject subject) throws SQLException {
        String insertQuery = "insert into subject values(?,?)";
        int result;
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setNull(1, java.sql.Types.INTEGER);
            stmt.setString(2, subject.getName());
            result = stmt.executeUpdate();
            if (result > 0) {
                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    subject.setId(rs.getInt(1));
                    rs.close();
                }
            }
        }
    }


    public static Subject getSubjectByIdUsingColNames(int subjectId) throws SQLException {
        String selectQuery = "select * from subject where id = ?";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(selectQuery)) {
            stmt.setInt(1, subjectId);
            try (ResultSet rs = stmt.executeQuery()) {

                if (rs.next()) {
                    return new Subject(rs.getInt("id"),
                            rs.getString("name"));
                }
                return null;
            }
        }
    }


    public static Subject getSubjectByIdUsingColNumbers(int subjectId) throws SQLException {
        String selectQuery = "select * from subject where id = ?";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(selectQuery)) {
            stmt.setInt(1, subjectId);
            try (ResultSet rs = stmt.executeQuery()) {

                if (rs.next()) {
                    return new Subject(rs.getInt(1),
                            rs.getString(2));
                }
                return null;
            }
        }
    }


    public static void deleteSubjects() throws SQLException {
        String deleteQuery = "delete from subject";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(deleteQuery)) {
            stmt.executeUpdate();
        }
    }


    public static void insertSchool(School school) throws SQLException {
        String insertQuery = "insert into school values(?,?,?)";
        int result;
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setNull(1, java.sql.Types.INTEGER);
            stmt.setString(2, school.getName());
            stmt.setInt(3, school.getYear());
            result = stmt.executeUpdate();
            if (result > 0) {
                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    school.setId(rs.getInt(1));
                    rs.close();
                }
            }
        }
    }


    public static School getSchoolByIdUsingColNames(int schoolId) throws SQLException {
        String selectQuery = "select * from school where id = ?";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(selectQuery)) {
            stmt.setInt(1, schoolId);
            try (ResultSet rs = stmt.executeQuery()) {

                if (rs.next()) {
                    return new School(
                            rs.getInt("id"),
                            rs.getString("name"),
                            rs.getInt("year")
                    );
                }
                return null;
            }
        }
    }


    public static School getSchoolByIdUsingColNumbers(int schoolId) throws SQLException {
        String selectQuery = "select * from school where id = ?";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(selectQuery)) {
            stmt.setInt(1, schoolId);
            try (ResultSet rs = stmt.executeQuery()) {

                if (rs.next()) {
                    return new School(
                            rs.getInt(1),
                            rs.getString(2),
                            rs.getInt(3)
                    );
                }
                return null;
            }
        }
    }


    public static void deleteSchools() throws SQLException {
        String deleteQuery = "delete from school";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(deleteQuery)) {
            stmt.executeUpdate();
        }
    }

    public static void insertGroup(School school, Group group) throws SQLException {
        String insertQuery = "insert into `group` values(?,?,?,?)";
        int result;
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {
            stmt.setNull(1, java.sql.Types.INTEGER);
            stmt.setString(2, group.getName());
            stmt.setString(3, group.getRoom());
            stmt.setInt(4, school.getId());
            result = stmt.executeUpdate();
            if (result > 0) {
                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    group.setId(rs.getInt(1));
                    rs.close();
                }
            }
        }
    }


    public static School getSchoolByIdWithGroups(int id) throws SQLException {
        String selectQuery = "select * from `group`, school where school.id = ?";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(selectQuery)) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {

                School school = null;
                List<Group> groups = new ArrayList<>();
                boolean schoolDef = false;

                while (rs.next()) {
                    if (!schoolDef) {
                        school = new School(
                                rs.getInt("school.id"),
                                rs.getString("school.name"),
                                rs.getInt("school.year")
                        );
                        schoolDef = true;
                    }
                    groups.add(new Group(
                            rs.getInt("group.id"),
                            rs.getString("group.name"),
                            rs.getString("group.room")));
                }
                if (schoolDef) {
                    school.setGroups(groups);
                }
                return school;
            }
        }
    }

    public static List<School> getSchoolsWithGroups() throws SQLException {
        String selectQuery = "select * from `group`, school";
        try (PreparedStatement stmt = JdbcUtils.getConnection().prepareStatement(selectQuery);
             ResultSet rs = stmt.executeQuery()) {

            // Set неупорядочен (из-за этого возникают проблемы с необходимостью сортировки), поэтому используется ArrayListMultimap
            ArrayListMultimap<School, Group> schoolGroup = ArrayListMultimap.create();
            List<School> schools = new ArrayList<>();
            School school;

            while (rs.next()) {
                school = new School(
                        rs.getInt("school.id"),
                        rs.getString("school.name"),
                        rs.getInt("school.year"));

                if (school.getId() == rs.getInt("group.schoolid")) {
                    schoolGroup.put(school, new Group(
                            rs.getInt("group.id"),
                            rs.getString("group.name"),
                            rs.getString("group.room")));
                    if(!schools.contains(school)){
                        schools.add(school);
                    }
                }
            }

            for (School value : schools) {
                school = value;
                school.setGroups(schoolGroup.get(school));
            }

            return schools;
        }
    }

}
