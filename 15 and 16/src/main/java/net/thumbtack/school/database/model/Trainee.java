package net.thumbtack.school.database.model;

import lombok.*;

import java.util.Objects;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Trainee {
    private int id;
    private String firstName;
    private String lastName;
    private int rating;

    public Trainee(String firstName, String lastName, int rating){
        this(0, firstName, lastName, rating);
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getRating() {
        return rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trainee)) return false;
        Trainee trainee = (Trainee) o;
        return getId() == trainee.getId() &&
                getRating() == trainee.getRating() &&
                Objects.equals(getFirstName(), trainee.getFirstName()) &&
                Objects.equals(getLastName(), trainee.getLastName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName(), getRating());
    }
}
