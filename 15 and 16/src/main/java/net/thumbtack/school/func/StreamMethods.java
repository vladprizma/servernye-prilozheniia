package net.thumbtack.school.func;

import net.thumbtack.school.func.PersonWithAge.Person;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamMethods {

    //13
    public static IntStream transform(IntStream stream, IntUnaryOperator op){
        IntStream iS = stream.map(op);
        iS.forEach(System.out::println);
        return iS;
    }

    //14
    public static IntStream parallelTransform(IntStream stream, IntUnaryOperator op){
        IntStream iS = stream.parallel().map(op);
        iS.parallel().forEach(System.out::println);
        return iS;
    }

    // 15
    public static List<String> uniqueNamesOlderThirtySortLength(List<Person> list){
        return list.stream().filter(p -> p.getAge() > 30).
                map(Person::getName).distinct().
                sorted(Comparator.comparingInt(String::length)).
                collect(Collectors.toList());
    }

    // 16
    public static List<String> uniqueNamesOlderThirtySortByFrequency(List<Person> list){
        return list.stream().filter(p -> p.getAge() > 30).
                collect(Collectors.groupingBy(Person::getName, Collectors.counting())).
                entrySet().stream().
                sorted(Map.Entry.<String, Long>comparingByValue().reversed()). // Надо уточнять параметры, тогда работает
                map(Map.Entry::getKey).
                collect(Collectors.toList());
    }

    //17
    public static Integer sum(List<Integer> list){
        return list.stream().reduce(Integer::sum).orElse(0);
    }

    public static Integer product(List<Integer> list){
        return list.stream().reduce((a,b) -> a*b).orElse(0);
    }

}
