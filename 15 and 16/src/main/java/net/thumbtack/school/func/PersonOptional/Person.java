package net.thumbtack.school.func.PersonOptional;

import java.util.Optional;

// 12 b
public class Person {
    private String firstName;
    private String lastName;
    private Optional<Person> father;
    private Optional<Person> mother;

    public Person(String firstName) {
        this.firstName = firstName;
    }

    public Person(String firstName, String lastName) {
        this(firstName);
        this.lastName = lastName;
    }

    public Person(String firstName, String lastName, Person father, Person mother) {
        this(firstName, lastName);
        this.father = Optional.ofNullable(father);
        this.mother = Optional.ofNullable(mother);
    }

    @Override
    public String toString() {
        return "Person [firstName=" + firstName + ", lastName=" + lastName + "]";
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Optional<Person> getMothersMotherFather() {
        return this.mother.flatMap(oP -> oP.mother).flatMap(oP -> oP.father);
    }

    public Optional<Person> getFather() {
        return father;
    }

    public Optional<Person> getMother() {
        return mother;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Person other = (Person) obj;
        if (firstName == null) {
            if (other.firstName != null)
                return false;
        } else if (!firstName.equals(other.firstName))
            return false;
        if (lastName == null) {
            if (other.lastName != null)
                return false;
        } else if (!lastName.equals(other.lastName))
            return false;
        return true;
    }
}
