package net.thumbtack.school.func;

import net.thumbtack.school.func.Person.Person;

import java.util.Arrays;
import java.util.List;

// 10
public class MyFunctionMethods {

    public static MyFunction<String, List<String>> split =
            str -> Arrays.asList(str.split(" "));

    public static MyFunction<List<?>, Integer> count = List::size;

    public static MyFunction<String, Person> create = Person::new;

}
