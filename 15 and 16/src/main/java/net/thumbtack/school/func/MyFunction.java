package net.thumbtack.school.func;

// 10
@FunctionalInterface
public interface MyFunction<T,K> {
    K apply(T arg);

    // 11
    //K apply(T arg1, T arg2);
    // @FunctionalInterface гарантирует что будет один функциональный метод

    //Error:(4, 1) java: Unexpected @FunctionalInterface annotation
    //  net.thumbtack.school.func.MyFunction is not a functional interface
    //    multiple non-overriding abstract methods found in interface net.thumbtack.school.func.MyFunction
}
