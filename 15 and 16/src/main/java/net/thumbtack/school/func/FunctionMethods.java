package net.thumbtack.school.func;

import net.thumbtack.school.func.Person.Person;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.*;

public class FunctionMethods {

    // 1
    public static Function<String, List<String>> split =
            str -> Arrays.asList(str.split(" "));

    public static Function<List<?>, Integer> count = List::size;

    // 2
    // Такое возможно потому что входные параметры указываются в Function<T, R>
    // так что независимо от указания типа в функции (T t) -> ...
    // у t тип T указан входным параметром в Function, значит можно писать (t) -> ...

    // 3
    // Это возможно в нескольких случаях:
    // ContainingClass::staticMethodName - все параметры соответствуют сигнатуре функции
    // containingObject::instanceMethodName - все параметры соответствуют сигнатуре функции
    // ContainingClass::instanceMethodName - это возможно из-за скрытого параметра this, который может удовлетворять сигнатуре функции

    // 4
    // a
    public static Function<String, Integer> splitAndCountA = split.andThen(count);
    // b
    public static Function<String, Integer> splitAndCountB = count.compose(split);
    // В плане работы данный метод не отличается ни чем
    // Однако, данный метод сокращает количество кода необходимое для вызова последовательности функций
    // А следовательно повышает читабельность

    // 5
    // public static Function<String, Person> create = str -> new Person(str);
    public static Function<String, Person> create = Person::new;
    // ClassName::new - reference на конструктор

    // 6
    public static ToIntBiFunction<Integer, Integer> max = Math::max;
    //ToDoubleBiFunction, ToIntBiFunction, ToLongBiFunction - метод возвращает свой тип

    // 7
    public static Supplier<Date> getCurrentDate = Date::new;

    // 8
    public static Predicate<Integer> isEven  = a -> a % 2 == 0;

    // 9
    public static BiPredicate<Integer, Integer> areEqual  = Integer::equals;


}
