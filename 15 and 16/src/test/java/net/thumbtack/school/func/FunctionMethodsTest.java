package net.thumbtack.school.func;

import net.thumbtack.school.func.PersonWithAge.Person;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class FunctionMethodsTest {

    @Test
    public void testSplitAndCount(){
        String spaces = "Space check Space check Space check Space check Space check";
        List<String> output = FunctionMethods.split.apply(spaces);

        assertEquals(10, output.size());
        assertEquals(output.size(), FunctionMethods.count.apply(output));

        assertEquals(output.size(), FunctionMethods.splitAndCountA.apply(spaces));
        assertEquals(output.size(), FunctionMethods.splitAndCountB.apply(spaces));

        spaces = "Space     check";
        output = FunctionMethods.split.apply(spaces);
        assertEquals(6, output.size());
        assertEquals(output.size(), FunctionMethods.count.apply(output));
    }

    @Test
    public void testMySplitAndCount(){
        String spaces = "Space check Space check Space check Space check Space check";
        List<String> output = MyFunctionMethods.split.apply(spaces);

        assertEquals(10, output.size());
        assertEquals(output.size(), MyFunctionMethods.count.apply(output));

        spaces = "Space     check";
        output = MyFunctionMethods.split.apply(spaces);
        assertEquals(6, output.size());
        assertEquals(output.size(), MyFunctionMethods.count.apply(output));
    }

    @Test
    public void testPerson(){
        String name = "Name";
        net.thumbtack.school.func.Person.Person person = FunctionMethods.create.apply(name);

        assertEquals(name, person.getFirstName());
        person = MyFunctionMethods.create.apply(name);
        assertEquals(name, person.getFirstName());
    }

    @Test
    public void testMax(){
        assertEquals(5, FunctionMethods.max.applyAsInt(-1, 5));
        assertEquals(0, FunctionMethods.max.applyAsInt(0, 0));
        assertEquals(3, FunctionMethods.max.applyAsInt(3, -1000));
        assertEquals(1000, FunctionMethods.max.applyAsInt(999, 1000));
    }

    /*
    @Test
    public void testCurrentDate(){ // Сильно зависит от скорости выполнения
        assertEquals(new Date().toString(), FunctionMethods.getCurrentDate.get().toString());
    }
     */

    @Test
    public void testEven(){
        assertTrue(FunctionMethods.isEven.test(6));
        assertTrue(FunctionMethods.isEven.test(-2));
        assertFalse(FunctionMethods.isEven.test(3));
        assertFalse(FunctionMethods.isEven.test(-3));
        assertTrue(FunctionMethods.isEven.test(0));
    }

    @Test
    public void testEqual(){
        assertTrue(FunctionMethods.areEqual.test(0, 0));
        assertFalse(FunctionMethods.areEqual.test(-1, 0));
        assertFalse(FunctionMethods.areEqual.test(0, 1));
        assertTrue(FunctionMethods.areEqual.test(-3, -3));
        assertTrue(FunctionMethods.areEqual.test(3, 3));

    }

    @Test
    public void testMothersMotherFather(){
        net.thumbtack.school.func.Person.Person mothersMotherFather = new
                net.thumbtack.school.func.Person.Person("Father", "Family");
        net.thumbtack.school.func.Person.Person mothersMother = new
                net.thumbtack.school.func.Person.Person("Mother", "Family", mothersMotherFather, null);
        net.thumbtack.school.func.Person.Person mother =
                new net.thumbtack.school.func.Person.Person("Mother", "Family", null, mothersMother);
        net.thumbtack.school.func.Person.Person person =
                new net.thumbtack.school.func.Person.Person("Person", "Family", null, mother);
        assertEquals(mothersMotherFather, person.getMothersMotherFather());

        mothersMother =
                new net.thumbtack.school.func.Person.Person("Mother", "Family", null, null);
        mother =
                new net.thumbtack.school.func.Person.Person("Mother", "Family", null, mothersMother);
        person =
                new net.thumbtack.school.func.Person.Person("Person", "Family", null, mother);
        assertNull(person.getMothersMotherFather());

        mother =
                new net.thumbtack.school.func.Person.Person("Mother", "Family", null, null);
        person =
                new net.thumbtack.school.func.Person.Person("Person", "Family", null, mother);
        assertNull(person.getMothersMotherFather());

        person =
                new net.thumbtack.school.func.Person.Person("Person", "Family", null, null);
        assertNull(person.getMothersMotherFather());
    }

    @Test
    public void testMothersMotherFatherOptional(){
        net.thumbtack.school.func.PersonOptional.Person mothersMotherFather =
                new net.thumbtack.school.func.PersonOptional.Person("Father", "Family");
        net.thumbtack.school.func.PersonOptional.Person mothersMother =
                new net.thumbtack.school.func.PersonOptional.Person("Mother", "Family", mothersMotherFather, null);
        net.thumbtack.school.func.PersonOptional.Person mother =
                new net.thumbtack.school.func.PersonOptional.Person("Mother", "Family", null, mothersMother);
        net.thumbtack.school.func.PersonOptional.Person person =
                new net.thumbtack.school.func.PersonOptional.Person("Person", "Family", null, mother);
        assertEquals(Optional.of(mothersMotherFather), person.getMothersMotherFather());

        mothersMother =
                new net.thumbtack.school.func.PersonOptional.Person("Mother", "Family", null, null);
        mother =
                new net.thumbtack.school.func.PersonOptional.Person("Mother", "Family", null, mothersMother);
        person =
                new net.thumbtack.school.func.PersonOptional.Person("Person", "Family", null, mother);
        assertEquals(Optional.empty(), person.getMothersMotherFather());

        mother =
                new net.thumbtack.school.func.PersonOptional.Person("Mother", "Family", null, null);
        person =
                new net.thumbtack.school.func.PersonOptional.Person("Person", "Family", null, mother);
        assertEquals(Optional.empty(), person.getMothersMotherFather());

        person =
                new net.thumbtack.school.func.PersonOptional.Person("Person", "Family", null, null);
        assertEquals(Optional.empty(), person.getMothersMotherFather());
    }

    @Test
    public void testIntStream() {
        // Версия с одним потоком
        IntStream iS = IntStream.of(1, 2, 3, 4, 5, 6);
        IntUnaryOperator iUP = a -> a + a;
        IntStream output = StreamMethods.transform(iS, iUP);
        // int[] outputArr = output.toArray();
        // int[] answer1 = new int[]{2,4,6,8,10,12};
        //assertArrayEquals(answer1, outputArr); // - с выводом не будет работать

        // Параллельная версия
        iS = IntStream.of(1, 2, 3, 4, 5, 6);
        IntStream parallelMethodOutput = StreamMethods.parallelTransform(iS, iUP);
        // В этой версии вывод произошел дробью, что-то прилетело раньше, что-то позже
    }
    @Test
    public void testNamesOlderThirty() {
        Person name31 = new Person("Name", 31);
        Person name32 = new Person("Name", 32);
        Person name33 = new Person("Name", 33);
        Person naname31 = new Person("Naname", 31);
        Person naname32 = new Person("Naname", 32);
        Person nameme31 = new Person("Nameme", 31);
        Person name29 = new Person("Name", 31);
        Person naname30 = new Person("Name", 30);

        List<Person> input = new ArrayList<>();
        Collections.addAll(input, name31, name32, name33, naname31, naname32, nameme31, name29, naname30);

        List<String> answer = new ArrayList<>();
        Collections.addAll(answer,
                name31.getName(),
                naname31.getName(),
                nameme31.getName()
        );

        List<String> output = StreamMethods.uniqueNamesOlderThirtySortLength(input);
        assertEquals(answer, output);

        output = StreamMethods.uniqueNamesOlderThirtySortByFrequency(input);
        assertEquals(answer, output);
    }
    @Test
    public void testSumAndProduct(){
        List<Integer> input = new ArrayList<>();
        Collections.addAll(input, 1,2,3,4,5);
        int output = StreamMethods.sum(input);
        assertEquals(15, output);
        output = StreamMethods.product(input);
        assertEquals(120, output);
    }
}