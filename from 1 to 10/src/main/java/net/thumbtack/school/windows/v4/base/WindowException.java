package net.thumbtack.school.windows.v4.base;

import net.thumbtack.school.windows.v3.base.Window;

public class WindowException extends Exception {
    WindowErrorCode code;

    public WindowException(WindowErrorCode code) throws WindowException {
        if(code == null){
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }
        this.code = code;
    }

    public WindowErrorCode getWindowErrorCode(){
        return code;
    }

}
