package net.thumbtack.school.windows.v4.base;

public enum WindowErrorCode {
    WRONG_STATE("Window already destroyed or parameter equals null"),
    WRONG_INDEX("Incorrect index"),
    EMPTY_ARRAY("String array equals null"),
    NULL_WINDOW("Window equals null");

    private String errorString;

    WindowErrorCode(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
