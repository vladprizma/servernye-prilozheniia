package net.thumbtack.school.windows.v4.base;

public enum WindowState {
    ACTIVE,
    INACTIVE,
    DESTROYED;

    public static WindowState fromString(String stateString) throws WindowException {
        WindowState state = WindowState.ACTIVE;
        try{
            state = WindowState.valueOf(stateString);
        }
        catch (IllegalArgumentException | NullPointerException ex){
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }

        return state;
    }

}
