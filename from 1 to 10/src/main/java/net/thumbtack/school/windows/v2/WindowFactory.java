package net.thumbtack.school.windows.v2;

public class WindowFactory {
    private static int counterRect = 0,counterRound = 0;

    public static RectButton createRectButton(Point leftTop, Point rightBottom, boolean active, String text){
        counterRect++;
        return new RectButton(leftTop,rightBottom,active, text);
    }

    public static RoundButton createRoundButton(Point center, int radius, boolean active, String text){
        counterRound++;
        return new RoundButton(center,radius,active, text);
    }

    public static int getRectButtonCount(){
        return counterRect;
    }

    public static int getRoundButtonCount(){
        return counterRound;
    }

    public static int getWindowCount(){
        return counterRect + counterRound;
    }

    public static void reset(){
        counterRect = 0;
        counterRound = 0;
    }

}
