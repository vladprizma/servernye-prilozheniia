package net.thumbtack.school.windows.v3.base;

import net.thumbtack.school.windows.v3.Desktop;
import net.thumbtack.school.windows.v3.Point;
import net.thumbtack.school.windows.v3.RectButton;
import net.thumbtack.school.windows.v3.iface.Movable;
import net.thumbtack.school.windows.v3.iface.Resizable;

import java.util.Objects;

public abstract class RectWindow extends Window{
    private Point topLeft, bottomRight;

    public RectWindow(Point topLeft, Point bottomRight, boolean active){
        super(active);
        if(topLeft.equals(bottomRight)){
            this.topLeft = new Point(topLeft.getX(),topLeft.getY());
            this.bottomRight = new Point(topLeft.getX() + 1,topLeft.getY() + 1);
        } else {
            this.topLeft = new Point(topLeft);
            this.bottomRight = new Point(bottomRight);
        }
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public void setTopLeft(Point topLeft) {
        this.topLeft = topLeft;
    }

    public void setBottomRight(Point bottomRight) {
        this.bottomRight = bottomRight;
    }

    public int getWidth(){
        return bottomRight.getX() - topLeft.getX() + 1;
    }

    public int getHeight() {
        return bottomRight.getY() - topLeft.getY() + 1;
    }

    public void moveRel(int dx, int dy){
        topLeft.moveRel(dx ,dy);
        bottomRight.moveRel(dx ,dy);
    }

    public void moveTo(int x, int y){
        bottomRight.moveTo(bottomRight.getX() - topLeft.getX() + x, bottomRight.getY() - topLeft.getY() + y);
        topLeft.moveTo(x ,y);
    }

    public void resize(double ratio){
        if(getWidth() * ratio < 1)
            bottomRight.setX(topLeft.getX());
        else
            bottomRight.setX((int) (topLeft.getX() + getWidth() * ratio) - 1);

        if(getHeight() * ratio < 1)
            bottomRight.setY(topLeft.getY());
        else
            bottomRight.setY( (int) (topLeft.getY() + getHeight() * ratio)-1);
    }

    public boolean isInside(int x, int y){
        return x >= topLeft.getX() && x <= bottomRight.getX() && y >= topLeft.getY() && y <= bottomRight.getY();
    }

    public boolean isInside(Point point){
        return isInside(point.getX(),point.getY());
    }

    public boolean isIntersects(RectWindow rectButton){
        return topLeft.getX() <= rectButton.bottomRight.getX() &&
                topLeft.getY() <=  rectButton.bottomRight.getY() &&
                bottomRight.getX() >= rectButton.topLeft.getX() &&
                bottomRight.getY() >= rectButton.topLeft.getY();
    }

    public boolean isInside(RectWindow rectButton){
        return topLeft.getX() <= rectButton.topLeft.getX() &&
                topLeft.getY() <= rectButton.topLeft.getY() &&
                bottomRight.getX() >= rectButton.bottomRight.getX() &&
                bottomRight.getY() >= rectButton.bottomRight.getY();
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop){
        return topLeft.getX() >= 0 &&
                topLeft.getY() >= 0 &&
                bottomRight.getX() < desktop.getWidth() &&
                bottomRight.getY() < desktop.getHeight();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RectWindow that = (RectWindow) o;
        return Objects.equals(topLeft, that.topLeft) &&
                Objects.equals(bottomRight, that.bottomRight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), topLeft, bottomRight);
    }
}
