package net.thumbtack.school.windows.v4.base;

import net.thumbtack.school.windows.v4.Desktop;
import net.thumbtack.school.windows.v4.Point;

import java.util.Objects;

public abstract class RoundWindow extends Window {
    private Point center;
    private int radius;

    public RoundWindow(Point center, int radius, WindowState state) throws WindowException {
        super(state);
        this.center = new Point(center);
        this.radius = radius;
    }

    public Point getCenter(){
        return center;
    }

    public int getRadius(){
        return radius;
    }

    public void setRadius(int radius){
        this.radius = radius;
    }

    public void setCenter(int x, int y) {
        this.center.moveTo(x, y);
    }

    public void moveTo(int x, int y) {
        setCenter(x, y);
    }

    public void setCenter(Point center) {
        this.setCenter(center.getX(),center.getY());
    }

    public void moveRel(int dx, int dy){
        center.moveRel(dx ,dy);
    }

    public void resize(double ratio){
        radius = (int) (radius * ratio);
        if(radius < 1){
            radius = 1;
        }
    }

    public boolean isInside(int x, int y){
        return (center.getX() - x)*(center.getX() - x) +
                (center.getY() - y)*(center.getY() - y) <= radius*radius;
    }

    public boolean isInside(Point point){
        return isInside(point.getX(),point.getY());
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop){
        return 0 <= center.getX() - radius && 0 <= center.getY() - radius &&
                desktop.getWidth() > (center.getX() + radius) && desktop.getHeight() > (center.getY() + radius);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RoundWindow that = (RoundWindow) o;
        return radius == that.radius &&
                Objects.equals(center, that.center);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), center, radius);
    }
}
