package net.thumbtack.school.windows.managers;

import net.thumbtack.school.windows.v4.Desktop;
import net.thumbtack.school.windows.v4.base.Window;
import net.thumbtack.school.windows.v4.base.WindowErrorCode;
import net.thumbtack.school.windows.v4.base.WindowException;
import net.thumbtack.school.windows.v4.cursors.Cursor;

public class ArrayManager<T extends Window> {
    private T[] arr;

    public ArrayManager(T[] arr) throws WindowException {
        if(arr == null){
            throw new WindowException(WindowErrorCode.NULL_WINDOW);
        }
        for(T elem : arr){
            if(elem == null){
                throw new WindowException(WindowErrorCode.NULL_WINDOW);
            }
        }
        this.arr = arr;
    }

    public T[] getWindows() {
        return arr;
    }

    public void setWindows(T[] arr) throws WindowException {
        if(arr == null){
            throw new WindowException(WindowErrorCode.NULL_WINDOW);
        }
        for(T elem : arr){
            if(elem == null){
                throw new WindowException(WindowErrorCode.NULL_WINDOW);
            }
        }
        this.arr = arr;
    }

    public T getWindow(int index) {
        return arr[index];
    }

    public void setWindow(T elem, int index) throws WindowException {
        if(elem == null){
            throw new WindowException(WindowErrorCode.NULL_WINDOW);
        }
        this.arr[index] = elem;
    }

    public boolean isSameSize(ArrayManager obj){
        return this.arr.length == obj.arr.length;
    }

    public boolean allWindowsFullyVisibleOnDesktop(Desktop desktop){
        for(T elem : arr){
            if(!elem.isFullyVisibleOnDesktop(desktop)){
                return false;
            }
        }
        return true;
    }

    public boolean anyWindowFullyVisibleOnDesktop(Desktop desktop){
        for(T elem : arr){
            if(elem.isFullyVisibleOnDesktop(desktop)){
                return true;
            }
        }
        return false;
    }

    public Window getFirstWindowFromCursor(Cursor cursor){
        for(T elem : arr){
            if(elem.isInside(cursor.getX(),cursor.getY())){
                return elem;
            }
        }
        return null;
    }

}
