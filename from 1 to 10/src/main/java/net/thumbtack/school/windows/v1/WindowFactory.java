package net.thumbtack.school.windows.v1;

public class WindowFactory {
    private static int counterRect = 0,counterRound = 0;

    public static RectButton createRectButton(Point leftTop, Point rightBottom, boolean active){
        counterRect++;
        return new RectButton(leftTop,rightBottom,active);
    }

    public static RoundButton createRoundButton(Point center, int radius, boolean active){
        counterRound++;
        return new RoundButton(center,radius,active);
    }

    public static int getRectButtonCount(){
        return counterRect;
    }

    public static int getRoundButtonCount(){
        return counterRound;
    }

    public static int getWindowCount(){
        return counterRect + counterRound;
    }

    public static void reset(){
        counterRect = 0;
        counterRound = 0;
    }

}
