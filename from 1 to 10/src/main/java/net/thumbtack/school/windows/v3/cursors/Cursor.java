package net.thumbtack.school.windows.v3.cursors;

import net.thumbtack.school.windows.v3.Point;
import net.thumbtack.school.windows.v3.iface.Movable;

public class Cursor implements Movable {
    private Point point;
    private int cursorForm;

    public Cursor(int x, int y, int cursorForm) {
        point = new Point(x, y);
        this.cursorForm = cursorForm;
    }

    public Cursor(Point point, int cursorForm) {
        this(point.getX(), point.getY(), cursorForm);
    }

    public int getCursorForm(){
        return cursorForm;
    }

    public void setCursorForm(int cursorForm){
        this.cursorForm = cursorForm;
    }

    public int getX(){
        return point.getX();
    }

    public int getY(){
        return point.getY();
    }

    public void setX(int x){
        point.setX(x);
    }

    public void setY(int y){
        point.setY(y);
    }

    public void moveTo(int x, int y){
        point.moveTo(x, y);
    }

    public void moveTo(Point point){
        this.point.moveTo(point.getX(),point.getY());
    }

    public void moveRel(int dx, int dy){
        point.moveRel(dx, dy);
    }

}
