package net.thumbtack.school.windows.v4;

import net.thumbtack.school.base.StringOperations;
import net.thumbtack.school.windows.v4.base.RectWindow;
import net.thumbtack.school.windows.v4.base.Window;
import net.thumbtack.school.windows.v4.base.WindowErrorCode;
import net.thumbtack.school.windows.v4.base.WindowException;
import net.thumbtack.school.windows.v4.base.WindowState;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

public class ListBox extends RectWindow {
    private String[] lines;

    public ListBox(Point topLeft, Point bottomRight, WindowState state, String[] lines) throws WindowException {
        super(topLeft,bottomRight,state);
        if (lines != null) {
            this.lines = new String[lines.length];
            System.arraycopy(lines, 0, this.lines, 0, lines.length);
        } else {
            this.lines = null;
        }
    }

    public ListBox(Point topLeft, Point bottomRight, String stateString, String[] lines) throws WindowException {
        this(topLeft, bottomRight, WindowState.fromString(stateString), lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, WindowState state, String[] lines) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), state, lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, String stateString, String[] lines) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), WindowState.fromString(stateString), lines);
    }

    public ListBox(Point topLeft, Point bottomRight, String[] lines) throws WindowException {
        this(topLeft, bottomRight, WindowState.ACTIVE, lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, String[] lines) throws WindowException {
        this(new Point(xLeft, yTop), new Point(xLeft + width - 1, yTop + height - 1), WindowState.ACTIVE, lines);
    }

    public String[] getLines() {
        if (lines != null) {
            return lines.clone();
        }
        return null;
    }

    public void setLines(String[] lines) {
        if (lines != null) {
            this.lines = new String[lines.length];
            System.arraycopy(lines, 0, this.lines, 0, lines.length);
        } else {
            this.lines = null;
        }
    }

    public String[] getLinesSlice(int from, int to) throws WindowException {
        if (lines == null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        }
        if (from < 0 || to > lines.length || from > (to - 1)) {
            throw new WindowException(WindowErrorCode.WRONG_INDEX);
        }
        int length = to - from;
        if (length > lines.length) {
            length = lines.length;
        }
        String[] buf = new String[length];
        System.arraycopy(lines, from, buf, 0, length);
        return buf;
    }

    public String getLine(int index) throws WindowException {
        if (lines == null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        }
        if (index < 0 || lines.length <= index) {
            throw new WindowException(WindowErrorCode.WRONG_INDEX);
        }
        return lines[index];
    }

    public void setLine(int index, String line) throws WindowException {
        if (lines == null) {
            throw new WindowException(WindowErrorCode.EMPTY_ARRAY);
        }
        if (index < 0 || lines.length <= index) {
            throw new WindowException(WindowErrorCode.WRONG_INDEX);
        }
        lines[index] = line;
    }

    public Integer findLine(String line) {
        if (lines != null) {
            int buf = Arrays.asList(lines).indexOf(line);
            if (buf >= 0) {
                return buf;
            }
        }
        return null;
    }

    public void reverseLineOrder() {
        if (lines != null) {
            Collections.reverse(Arrays.asList(lines));
        }
    }

    public void reverseLines() {
        if (lines != null) {
            for (int i = 0; i < lines.length; i++) {
                lines[i] = StringOperations.reverse(lines[i]);
            }
        }
    }

    public void duplicateLines() {
        if (lines != null) {
            String[] buf = new String[lines.length * 2];
            int count = 0;
            for (int i = 0; i < buf.length; i = i + 2) {
                buf[i] = lines[count];
                buf[i + 1] = lines[count];
                count++;
            }
            lines = buf;
        }
    }

    public void removeOddLines() {
        if (lines != null) {
            int length = lines.length / 2;
            if (lines.length % 2 != 0) {
                length++;
            }
            String[] buf = new String[length];
            int count = 0;
            for (int i = 0; i < lines.length; i++) {
                if (i % 2 == 0) {
                    buf[count] = lines[i];
                    count++;
                }
            }
            lines = buf;
        }
    }

    public boolean isSortedDescendant() {
        if (lines != null) {
            for (int i = 1; i < lines.length; i++) {
                if (lines[i - 1].compareTo(lines[i]) <= 0) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ListBox listBox = (ListBox) o;
        return Arrays.equals(lines, listBox.lines);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + Arrays.hashCode(lines);
        return result;
    }
}
