package net.thumbtack.school.windows.managers;

import net.thumbtack.school.windows.v4.Desktop;
import net.thumbtack.school.windows.v4.base.Window;
import net.thumbtack.school.windows.v4.base.WindowErrorCode;
import net.thumbtack.school.windows.v4.base.WindowException;

public class PairManager<T extends Window, V extends Window> {
    private T objT;
    private V objV;

    public PairManager(T objT, V objV) throws WindowException {
        if(objT == null || objV == null){
            throw new WindowException(WindowErrorCode.NULL_WINDOW);
        }
        this.objT = objT;
        this.objV = objV;
    }

    public T getFirstWindow() {
        return objT;
    }

    public V getSecondWindow() {
        return objV;
    }

    public void setFirstWindow(T objT) throws WindowException {
        if(objT == null){
            throw new WindowException(WindowErrorCode.NULL_WINDOW);
        }
        this.objT = objT;
    }

    public void setSecondWindow(V objV) throws WindowException {
        if(objV == null){
            throw new WindowException(WindowErrorCode.NULL_WINDOW);
        }
        this.objV = objV;
    }

    public  boolean allWindowsFullyVisibleOnDesktop(PairManager<? extends Window,? extends Window> pair, Desktop desktop){
        return objT.isFullyVisibleOnDesktop(desktop) &&
                objV.isFullyVisibleOnDesktop(desktop) &&
                pair.getFirstWindow().isFullyVisibleOnDesktop(desktop) &&
                pair.getSecondWindow().isFullyVisibleOnDesktop(desktop);
    }

    public static boolean allWindowsFullyVisibleOnDesktop(
            PairManager<? extends Window,? extends Window> firstPair, PairManager<? extends Window,? extends Window> secondPair, Desktop desktop){
        return firstPair.getFirstWindow().isFullyVisibleOnDesktop(desktop) &&
                firstPair.getSecondWindow().isFullyVisibleOnDesktop(desktop) &&
                secondPair.getFirstWindow().isFullyVisibleOnDesktop(desktop) &&
                secondPair.getSecondWindow().isFullyVisibleOnDesktop(desktop);
    }
}
