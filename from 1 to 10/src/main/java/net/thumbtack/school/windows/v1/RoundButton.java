package net.thumbtack.school.windows.v1;

import java.util.Objects;

public class RoundButton {
    private Point center;
    private int radius;
    private boolean active = true;

    public RoundButton(Point center, int radius, boolean active){
        this.center = new Point(center);
        this.radius = radius;
        this.active = active;
    }

    public RoundButton(int xCenter, int yCenter, int radius, boolean active){
        this(new Point(xCenter,yCenter), radius, active);
    }

    public RoundButton(Point center, int radius){
        this(new Point(center),radius,true);
    }

    public RoundButton(int xCenter, int yCenter, int radius){
        this(new Point(xCenter,yCenter),radius,true);
    }

    public Point getCenter(){
        return center;
    }

    public int getRadius(){
        return radius;
    }

    public boolean isActive(){
        return active;
    }

    public void moveTo(int x, int y){
        center.moveTo(x ,y);
    }

    public void moveTo(Point point){
        center.moveTo(point.getX(),point.getY());
    }

    public void setCenter(int x, int y){
        center.moveTo(x,y);
    }

    public void setRadius(int radius){
        this.radius = radius;
    }

    public void setActive(boolean active){
        this.active = active;
    }

    public void moveRel(int dx, int dy){
        center.moveRel(dx ,dy);
    }

    public void resize(double ratio){
        radius = (int) (radius * ratio);
        if(radius < 1){
            radius = 1;
        }
    }

    public boolean isInside(int x, int y){
        return (center.getX() - x)*(center.getX() - x) +
                (center.getY() - y)*(center.getY() - y) <= radius*radius;
    }

    public boolean isInside(Point point){
        return isInside(point.getX(),point.getY());
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop){
        return 0 <= center.getX() - radius && 0 <= center.getY() - radius &&
                desktop.getWidth() > (center.getX() + radius) && desktop.getHeight() > (center.getY() + radius);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoundButton that = (RoundButton) o;
        return radius == that.radius &&
                active == that.active &&
                Objects.equals(center, that.center);
    }

    @Override
    public int hashCode() {
        return Objects.hash(center, radius, active);
    }
}
