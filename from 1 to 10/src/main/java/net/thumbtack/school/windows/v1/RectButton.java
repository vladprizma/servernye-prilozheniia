package net.thumbtack.school.windows.v1;

import java.io.PipedOutputStream;
import java.util.Objects;

public class RectButton {
    private Point topLeft, bottomRight;
    private boolean active = true;

    public RectButton(Point topLeft, Point bottomRight, boolean active){
        if(topLeft.equals(bottomRight)){
            this.topLeft = new Point(topLeft.getX(),topLeft.getY());
            this.bottomRight = new Point(topLeft.getX() + 1,topLeft.getY() + 1);
        } else {
            this.topLeft = new Point(topLeft);
            this.bottomRight = new Point(bottomRight);
        }
        this.active = active;
    }

    public RectButton(int xLeft, int yTop, int width, int height, boolean active){
        this(new Point(xLeft,yTop),new Point(xLeft + width - 1,yTop + height - 1),active);
    }

    public RectButton(Point topLeft, Point bottomRight){
        this(new Point(topLeft), new Point(bottomRight), true);
    }

    public RectButton(int xLeft, int yTop, int width, int height){
        this(new Point(xLeft,yTop),new Point(xLeft + width - 1,yTop + height - 1),true);
    }

    public Point getTopLeft(){
        return topLeft;
    }

    public Point getBottomRight(){
        return bottomRight;
    }

    public boolean isActive(){
        return active;
    }

    public void setTopLeft(Point topLeft){
        this.topLeft = new Point(topLeft);
    }

    public void setBottomRight(Point bottomRight){
        this.bottomRight = new Point(bottomRight);
    }

    public void setActive(boolean active){
        this.active = active;
    }

    public int getWidth(){
        return bottomRight.getX() - topLeft.getX() + 1;
    }

    public int getHeight() {
        return bottomRight.getY() - topLeft.getY() + 1;
    }

    public void moveTo(int x, int y){
        bottomRight.moveTo(getWidth() + x - 1, getHeight() + y - 1);
        topLeft.moveTo(x ,y);
    }

    public void moveTo(Point point){
        this.moveTo(point.getX(),point.getY());
    }

    public void moveRel(int dx, int dy){
        topLeft.moveRel(dx ,dy);
        bottomRight.moveRel(dx ,dy);
    }

    public void resize(double ratio){
        if(getWidth() * ratio < 1)
            bottomRight.setX(topLeft.getX());
        else
            bottomRight.setX((int) (topLeft.getX() + getWidth() * ratio) - 1);

        if(getHeight() * ratio < 1)
            bottomRight.setY(topLeft.getY());
        else
            bottomRight.setY( (int) (topLeft.getY() + getHeight() * ratio)-1);
    }

    public boolean isInside(int x, int y){
        return x >= topLeft.getX() && x <= bottomRight.getX() && y >= topLeft.getY() && y <= bottomRight.getY();
    }

    public boolean isInside(Point point){
        return point.getX() >= topLeft.getX() &&
                point.getX() <= bottomRight.getX() &&
                point.getY() >= topLeft.getY() &&
                point.getY() <= bottomRight.getY();
    }

    public boolean isIntersects(RectButton rectButton){
        return topLeft.getX() <= rectButton.bottomRight.getX() &&
                topLeft.getY() <=  rectButton.bottomRight.getY() &&
                bottomRight.getX() >= rectButton.topLeft.getX() &&
                bottomRight.getY() >= rectButton.topLeft.getY();
    }

    public boolean isInside(RectButton rectButton){
        return topLeft.getX() <= rectButton.topLeft.getX() &&
                topLeft.getY() <= rectButton.topLeft.getY() &&
                bottomRight.getX() >= rectButton.bottomRight.getX() &&
                bottomRight.getY() >= rectButton.bottomRight.getY();
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop){
        return topLeft.getX() >= 0 &&
                topLeft.getY() >= 0 &&
                bottomRight.getX() <= desktop.getWidth() &&
                bottomRight.getY() <= desktop.getHeight();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RectButton that = (RectButton) o;
        return active == that.active &&
                Objects.equals(topLeft, that.topLeft) &&
                Objects.equals(bottomRight, that.bottomRight);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topLeft, bottomRight, active);
    }
}
