package net.thumbtack.school.windows.v2;

import java.util.Objects;

public class RoundButton {
    private Point center;
    private int radius;
    private boolean active = true;
    private String text;

    public RoundButton(Point center, int radius, boolean active, String text){
        this.center = new Point(center);
        this.radius = radius;
        this.active = active;
        this.text = text;
    }

    public RoundButton(int xCenter, int yCenter, int radius, boolean active, String text){
        this(new Point(xCenter,yCenter), radius, active, text);
    }

    public RoundButton(Point center, int radius, String text){
        this(new Point(center),radius,true, text);
    }

    public RoundButton(int xCenter, int yCenter, int radius, String text){
        this(new Point(xCenter,yCenter),radius,true, text);
    }

    public Point getCenter(){
        return center;
    }

    public int getRadius(){
        return radius;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isActive(){
        return active;
    }

    public void moveTo(int x, int y){
        center.moveTo(x ,y);
    }

    public void moveTo(Point point){
        center.moveTo(point.getX(),point.getY());
    }

    public void setCenter(int x, int y){
        center.moveTo(x,y);
    }

    public void setRadius(int radius){
        this.radius = radius;
    }

    public void setActive(boolean active){
        this.active = active;
    }

    public void moveRel(int dx, int dy){
        center.moveRel(dx ,dy);
    }

    public void resize(double ratio){
        radius = (int) (radius * ratio);
        if(radius < 1){
            radius = 1;
        }
    }

    public boolean isInside(int x, int y){
        return (center.getX() - x)*(center.getX() - x) +
                (center.getY() - y)*(center.getY() - y) <= radius*radius;
    }

    public boolean isInside(Point point){
        return isInside(point.getX(),point.getY());
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop){
        return 0 <= center.getX() - radius && 0 <= center.getY() - radius &&
                desktop.getWidth() > (center.getX() + radius) && desktop.getHeight() > (center.getY() + radius);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoundButton that = (RoundButton) o;
        return radius == that.radius &&
                active == that.active &&
                Objects.equals(center, that.center) &&
                Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(center, radius, active, text);
    }
}
