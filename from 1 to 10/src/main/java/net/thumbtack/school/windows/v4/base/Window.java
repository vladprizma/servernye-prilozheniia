package net.thumbtack.school.windows.v4.base;

import net.thumbtack.school.windows.v4.Desktop;
import net.thumbtack.school.windows.v4.Point;
import net.thumbtack.school.windows.v4.iface.Movable;
import net.thumbtack.school.windows.v4.iface.Resizable;

import java.util.Objects;

public abstract class Window implements Movable, Resizable {
    private WindowState state;

    public Window(WindowState state) throws WindowException {
        if(state == null || state == WindowState.DESTROYED){
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }
        this.state = state;
    }

    public WindowState getState(){
        return state;
    }

    public void setState(WindowState state) throws WindowException {
        if(state == null || (this.state == WindowState.DESTROYED && (state == WindowState.ACTIVE || state == WindowState.INACTIVE))){
            throw new WindowException(WindowErrorCode.WRONG_STATE);
        }
        this.state = state;
    }

    abstract public boolean isInside(int x, int y);

    public boolean isInside(Point point){
        return isInside(point.getX(),point.getY());
    }

    abstract public void resize(double ratio);

    abstract public boolean isFullyVisibleOnDesktop(Desktop desktop);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Window window = (Window) o;
        return state == window.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(state);
    }
}
