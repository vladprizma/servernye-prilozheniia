package net.thumbtack.school.windows.managers;

import net.thumbtack.school.windows.v4.Point;
import net.thumbtack.school.windows.v4.base.Window;
import net.thumbtack.school.windows.v4.base.WindowErrorCode;
import net.thumbtack.school.windows.v4.base.WindowException;

public class Manager<T extends Window> {
    private T obj;

    public Manager(T obj) throws WindowException {
        if(obj == null){
            throw new WindowException(WindowErrorCode.NULL_WINDOW);
        }
        this.obj = obj;
    }

    public T getWindow() {
        return obj;
    }

    public void setWindow(T obj) throws WindowException {
        if(obj == null){
            throw new WindowException(WindowErrorCode.NULL_WINDOW);
        }
        this.obj = obj;
    }

    public void moveTo(int x, int y){
        obj.moveTo(x,y);
    }

    public void moveTo(Point point){
        obj.moveTo(point);
    }


}
