package net.thumbtack.school.windows.v3;

import net.thumbtack.school.base.StringOperations;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

public class ListBox {
    private Point topLeft;
    private Point bottomRight;
    private boolean active = true;
    private String[] lines;

    public ListBox(Point topLeft, Point bottomRight, boolean active, String[] lines){
        this.topLeft = new Point(topLeft);
        this.bottomRight = new Point(bottomRight);
        this.active = active;
        if(lines != null) {
            this.lines = new String[lines.length];
            System.arraycopy(lines, 0, this.lines, 0, lines.length);
        }
        else{
            this.lines = null;
        }
    }

    public ListBox(int xLeft, int yTop, int width, int height, boolean active, String[] lines){
        this(new Point(xLeft,yTop), new Point(xLeft+width-1, yTop+height-1), active, lines);
    }

    public ListBox(Point topLeft, Point bottomRight, String[] lines){
        this(topLeft, bottomRight, true, lines);
    }

    public ListBox(int xLeft, int yTop, int width, int height, String[] lines){
        this(new Point(xLeft,yTop), new Point(xLeft+width-1, yTop+height-1), true, lines);
    }

    public Point getTopLeft(){
        return topLeft;
    }

    public Point getBottomRight(){
        return bottomRight;
    }

    public boolean isActive(){
        return active;
    }

    public void setTopLeft(Point topLeft){
        this.topLeft = new Point(topLeft);
    }

    public void setBottomRight(Point bottomRight){
        this.bottomRight = new Point(bottomRight);
    }

    public void setActive(boolean active){
        this.active = active;
    }

    public int getWidth(){
        return bottomRight.getX() - topLeft.getX() + 1;
    }

    public int getHeight(){
        return bottomRight.getY() - topLeft.getY() + 1;
    }

    public String[] getLines(){
        if(lines != null) {
            return lines.clone();
        }
        return null;
    }

    public void setLines(String[] lines){
        if(lines != null) {
            this.lines = new String[lines.length];
            System.arraycopy(lines, 0, this.lines, 0, lines.length);
        }
        else{
            this.lines = null;
        }
    }

    public String[] getLinesSlice(int from, int to){
        if(lines != null) {
            int length = to - from;
            if(length > lines.length){
                length = lines.length;
            }
            String [] buf = new String[length];
            System.arraycopy(lines, from, buf, 0, length);
            return buf;
        }
        return null;
    }

    public String getLine(int index){
        if(index >= lines.length || index < 0){
            return null;
        }
        return lines[index];
    }

    public void setLine(int index, String line){
        if((index < lines.length && index >= 0 ) && lines != null){
            lines[index] = line;
        }
    }

    public Integer findLine(String line){
        if(lines != null) {
            int buf = Arrays.asList(lines).indexOf(line);
            if (buf >= 0) {
                return buf;
            }
        }
        return null;
    }

    public void reverseLineOrder(){
        if(lines != null){
            Collections.reverse(Arrays.asList(lines));
        }
    }

    public void reverseLines(){
        if(lines != null){
            for(int i = 0; i < lines.length; i++){
               lines[i] = StringOperations.reverse(lines[i]);
            }
        }
    }

    public void duplicateLines(){
        if(lines != null){
            String [] buf = new String[lines.length*2];
            int count = 0;
            for(int i = 0; i < buf.length; i = i + 2) {
                buf[i] = lines[count];
                buf[i + 1] = lines[count];
                count++;
            }
            lines = buf;
        }
    }

    public void removeOddLines(){
        if(lines != null){
            int length = lines.length/2;
            if(lines.length % 2 != 0){
                length++;
            }
            String [] buf = new String[length];
            int count = 0;
            for(int i = 0; i < lines.length; i++){
                if(i % 2 == 0){
                    buf[count] = lines[i];
                    count++;
                }
            }
            lines = buf;
        }
    }

    public boolean isSortedDescendant(){
        if(lines != null) {
            for (int i = 1; i < lines.length; i++) {
                if (lines[i - 1].compareTo(lines[i]) <= 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public void moveTo(int x, int y){
        bottomRight.moveTo(getWidth() + x - 1, getHeight() + y - 1);
        topLeft.moveTo(x ,y);
    }

    public void moveTo(Point point){
        this.moveTo(point.getX(),point.getY());
    }

    public void moveRel(int dx, int dy){
        topLeft.moveRel(dx ,dy);
        bottomRight.moveRel(dx ,dy);
    }

    public void resize(double ratio){
        if(getWidth() * ratio < 1)
            bottomRight.setX(topLeft.getX());
        else
            bottomRight.setX((int) (topLeft.getX() + getWidth() * ratio) - 1);

        if(getHeight() * ratio < 1)
            bottomRight.setY(topLeft.getY());
        else
            bottomRight.setY( (int) (topLeft.getY() + getHeight() * ratio)-1);
    }

    public boolean isInside(int x, int y){
        return x >= topLeft.getX() && x <= bottomRight.getX() && y >= topLeft.getY() && y <= bottomRight.getY();
    }

    public boolean isInside(Point point){
        return point.getX() >= topLeft.getX() &&
                point.getX() <= bottomRight.getX() &&
                point.getY() >= topLeft.getY() &&
                point.getY() <= bottomRight.getY();
    }

    public boolean isIntersects(ListBox listBox){
        return topLeft.getX() <= listBox.bottomRight.getX() &&
                topLeft.getY() <=  listBox.bottomRight.getY() &&
                bottomRight.getX() >= listBox.topLeft.getX() &&
                bottomRight.getY() >= listBox.topLeft.getY();
    }

    public boolean isInside(ListBox listBox){
        return topLeft.getX() <= listBox.topLeft.getX() &&
                topLeft.getY() <= listBox.topLeft.getY() &&
                bottomRight.getX() >= listBox.bottomRight.getX() &&
                bottomRight.getY() >= listBox.bottomRight.getY();
    }

    public boolean isFullyVisibleOnDesktop(Desktop desktop){
        return topLeft.getX() >= 0 &&
                topLeft.getY() >= 0 &&
                bottomRight.getX() <= desktop.getWidth() &&
                bottomRight.getY() <= desktop.getHeight();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListBox listBox = (ListBox) o;
        return active == listBox.active &&
                Objects.equals(topLeft, listBox.topLeft) &&
                Objects.equals(bottomRight, listBox.bottomRight) &&
                Arrays.equals(lines, listBox.lines);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(topLeft, bottomRight, active);
        result = 31 * result + Arrays.hashCode(lines);
        return result;
    }
}
