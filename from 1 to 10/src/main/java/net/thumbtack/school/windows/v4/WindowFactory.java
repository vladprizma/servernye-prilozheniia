package net.thumbtack.school.windows.v4;

import net.thumbtack.school.windows.v4.base.WindowException;
import net.thumbtack.school.windows.v4.base.WindowState;

public class WindowFactory {
    private static int counterRect = 0,counterRound = 0;

    public static RectButton createRectButton(Point leftTop, Point rightBottom, WindowState state, String text) throws WindowException {
        counterRect++;
        return new RectButton(leftTop, rightBottom, state, text);
    }

    public static RoundButton createRoundButton(Point center, int radius, WindowState state, String text) throws WindowException {
        counterRound++;
        return new RoundButton(center, radius, state, text);
    }

    public static int getRectButtonCount(){
        return counterRect;
    }

    public static int getRoundButtonCount(){
        return counterRound;
    }

    public static int getWindowCount(){
        return counterRect + counterRound;
    }

    public static void reset(){
        counterRect = 0;
        counterRound = 0;
    }

}
