package net.thumbtack.school.base;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;

public class NumberOperations {

    public static Integer find(int[] array, int value){
        Integer [] arr = new Integer[array.length];
        int counter = 0;
        for (int elem : array) {
            arr[counter] = elem;
            counter++;
        }
        int buf = Arrays.asList(arr).indexOf(value);

        if(buf < 0){
            return null;
        }
        return buf;
    }

    public static Integer find(double[] array, double value, double eps){
        int counter = 0;
        for(double elem : array){
            if(Math.abs(elem - value) < eps){
                return counter;
            }
            counter++;
        }
        return null;
    }

    public static Double calculateDensity(double weight, double volume, double min, double max){
        double buf = weight/volume;
        int test = Double.compare(buf,min);
        int test1 = Double.compare(buf,max);
        if(Double.compare(buf,min) < 0 || Double.compare(buf,max) > 0){
            return null;
        }
        return buf;
    }

    public static Integer find(BigInteger[] array, BigInteger value){
        int buf = Arrays.asList(array).indexOf(value);
        if(buf < 0){
            return null;
        }
        return buf;
    }

    public static BigDecimal calculateDensity(BigDecimal weight, BigDecimal volume, BigDecimal min, BigDecimal max){
        BigDecimal buf = weight.divide(volume);
        if(buf.compareTo(min) < 0 || buf.compareTo(max) > 0){
            return null;
        }
        return buf;
    }

}
