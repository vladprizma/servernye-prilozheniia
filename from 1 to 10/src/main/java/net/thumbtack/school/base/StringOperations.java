package net.thumbtack.school.base;

import java.util.Arrays;

public class StringOperations {

    public static int getSummaryLength(String[] strings) {
        int totalLength = 0;
        for (String elem : strings) {
            totalLength += elem.length();
        }
        return totalLength;
    }

    public static String getFirstAndLastLetterString(String string) {
        return String.valueOf(string.charAt(0)) + string.charAt(string.length() - 1);
    }

    public static boolean isSameCharAtPosition(String string1, String string2, int index) {
        return String.valueOf(string1.charAt(index)).equals(String.valueOf(string2.charAt(index)));
    }

    public static boolean isSameFirstCharPosition(String string1, String string2, char character) {
        return string2.charAt(string1.indexOf(character)) == character;
    }

    public static boolean isSameLastCharPosition(String string1, String string2, char character) {
        return string2.charAt(string1.lastIndexOf(character)) == character;
    }

    public static boolean isSameFirstStringPosition(String string1, String string2, String str) {
        return string1.indexOf(str) == string2.indexOf(str);
    }

    public static boolean isSameLastStringPosition(String string1, String string2, String str) {
        return new StringBuilder(string1).reverse().indexOf(str) ==
                new StringBuilder(string2).reverse().indexOf(str);
    }

    public static boolean isEqual(String string1, String string2) {
        return string1.equals(string2);
    }

    public static boolean isEqualIgnoreCase(String string1, String string2) {
        return string1.toLowerCase().equals(string2.toLowerCase());
    }

    public static boolean isLess(String string1, String string2) {
        return string1.compareTo(string2) < 0;
    }

    public static boolean isLessIgnoreCase(String string1, String string2) {
        return string1.toLowerCase().compareTo(string2.toLowerCase()) < 0;
    }

    public static String concat(String string1, String string2) {
        return string1 + string2;
    }

    public static boolean isSamePrefix(String string1, String string2, String prefix) {
        return string1.startsWith(prefix) && string2.startsWith(prefix);
    }

    public static boolean isSameSuffix(String string1, String string2, String suffix) {
        return string1.endsWith(suffix) && string2.endsWith(suffix);
    }

    public static String getCommonPrefix(String string1, String string2) {
        String buf = "";
        int commonLength = Math.min(string1.length(),string2.length());
        for (int i = 0; i < commonLength; i++) {
            if (string1.charAt(i) != string2.charAt(i)) {
                buf = string1.substring(0,i);
                return buf.toString();
            }
            if (commonLength == i)
                break;
        }
        if(string1.length() <= string2.length()) {
            buf = string1;
        }
        else {
            buf = string2;
        }
        return buf;
    }

    public static String reverse(String string) {
        return new StringBuilder(string).reverse().toString();
    }

    public static boolean isPalindrome(String string) {
       int j = string.length() - 1;
       for(int i = 0; i < j; i++, j--){
           if(string.charAt(i) != string.charAt(j))
               return false;
        }
       return true;
    }

    public static boolean isPalindromeIgnoreCase(String string) {
        return string.toLowerCase().equals(new StringBuilder(string.toLowerCase()).reverse().toString());
    }

    public static String getLongestPalindromeIgnoreCase(String[] strings) {
        StringBuilder buf = new StringBuilder();
        for (String elem : strings) {
            if (elem.length() > buf.length() && StringOperations.isPalindromeIgnoreCase(elem)) {
                buf = new StringBuilder(elem);
            }
        }

        return buf.toString();
    }

    public static boolean hasSameSubstring(String string1, String string2, int index, int length) {
        if(index + length > string1.length() || index + length > string2.length()){
            return false;
        }
        return string1.substring(index, index + length).equals(string2.substring(index, index + length));
    }

    public static boolean isEqualAfterReplaceCharacters
            (String string1, char replaceInStr1, char replaceByInStr1, String string2, char replaceInStr2, char replaceByInStr2) {
        return string1.replaceAll(Character.toString(replaceInStr1), Character.toString(replaceByInStr1)).
                equals(string2.replaceAll(Character.toString(replaceInStr2), Character.toString(replaceByInStr2)));
    }

    public static boolean isEqualAfterReplaceStrings
            (String string1, String replaceInStr1, String replaceByInStr1, String string2, String replaceInStr2, String replaceByInStr2) {
        return string1.replaceAll(replaceInStr1, replaceByInStr1).
                equals(string2.replaceAll(replaceInStr2, replaceByInStr2));
    }

    public static boolean isPalindromeAfterRemovingSpacesIgnoreCase(String string) {
        return StringOperations.isPalindromeIgnoreCase(String.join("", string.split(" ")));
    }

    public static boolean isEqualAfterTrimming(String string1, String string2) {
        return string1.trim().equals(string2.trim());
    }

    public static String makeCsvStringFromInts(int[] array) {
        return StringOperations.makeCsvStringBuilderFromInts(array).toString();
    }

    public static String makeCsvStringFromDoubles(double[] array) {
        return StringOperations.makeCsvStringBuilderFromDoubles(array).toString();
    }

    public static StringBuilder makeCsvStringBuilderFromInts(int[] array) {
        if (array.length == 0) {
            return new StringBuilder("");
        }
        StringBuilder buf = new StringBuilder();
        int counter = 0;
        for (int elem : array) {
            counter++;
            if(counter == array.length) {
                buf.append(elem);
                break;
            }
            buf.append(elem).append(",");
        }
        return buf;
    }

    public static StringBuilder makeCsvStringBuilderFromDoubles(double[] array) {
        if (array.length == 0) {
            return new StringBuilder("");
        }
        StringBuilder buf = new StringBuilder();
        int counter = 0;
        for (double elem : array) {
            counter++;
            if(counter == array.length) {
                buf.append(String.format("%.2f", elem));
                break;
            }
            buf.append(String.format("%.2f", elem)).append(",");
        }
        return buf;
    }

    public static StringBuilder removeCharacters(String string, int[] positions) {
        StringBuilder buf = new StringBuilder(string);
        int difference = 0;
        for (int elem : positions) {
            buf.deleteCharAt(elem - difference);
            difference++;
        }
        return buf;
    }

    public static StringBuilder insertCharacters(String string, int[] positions, char[] characters) {
        StringBuilder buf = new StringBuilder(string);
        int counter = 0;
        for (int elem : positions) {
            buf.insert(elem + counter, characters[counter]);
            counter++;
        }
        return buf;
    }

}
