package net.thumbtack.school.matrix;

import java.util.*;

public class MatrixNonSimilarRows {
    private int [][]matrix;

    public MatrixNonSimilarRows(int[][] matrix){
        this.matrix = matrix;
    }

    public List<int[]> getNonSimilarRows(){
        Map<Set<Integer>,int[]> map = new HashMap<>();
        for(int [] line : matrix){
            Set<Integer>set = new HashSet<>();
            for(int elem : line){
                set.add(elem);
            }
            map.put(set,line);
        }
        return new ArrayList<>(map.values());
    }

}
