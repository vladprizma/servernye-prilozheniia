package net.thumbtack.school.file;

import com.google.gson.Gson;
import net.thumbtack.school.ttschool.Trainee;
import net.thumbtack.school.ttschool.TrainingErrorCode;
import net.thumbtack.school.ttschool.TrainingException;
import net.thumbtack.school.windows.v4.Point;
import net.thumbtack.school.windows.v4.RectButton;
import net.thumbtack.school.windows.v4.base.WindowException;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileService {

    public static void writeByteArrayToBinaryFile(String fileName, byte[] array) throws IOException {
        writeByteArrayToBinaryFile(new File(fileName), array);
    }

    public static void writeByteArrayToBinaryFile(File file, byte[] array) throws IOException {
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(array);
        }
    }

    public static byte[] readByteArrayFromBinaryFile(String fileName) throws IOException {
        return readByteArrayFromBinaryFile(new File(fileName));
    }

    public static byte[] readByteArrayFromBinaryFile(File file) throws IOException {
        try (FileInputStream inputStream = new FileInputStream(file)) {
            return inputStream.readAllBytes();
        }
    }

    public static byte[] writeAndReadByteArrayUsingByteStream(byte[] array) throws IOException {
        byte []res = null;
        byte []buf = null;
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            outputStream.write(array);
            buf = outputStream.toByteArray();
        }
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(buf)) {
            res = new byte[buf.length/2];
            for(int i = 0; i < res.length; i++){
                res[i] = (byte) inputStream.read();
                inputStream.skip(1);
            }
        }
        return res;
    }

    public static void writeByteArrayToBinaryFileBuffered(String fileName, byte[] array) throws IOException {
        writeByteArrayToBinaryFileBuffered(new File(fileName), array);
    }

    public static void writeByteArrayToBinaryFileBuffered(File file, byte[] array) throws IOException {
        try (BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            outputStream.write(array);
        }
    }

    public static byte[] readByteArrayFromBinaryFileBuffered(String fileName)throws IOException{
        return readByteArrayFromBinaryFileBuffered(new File(fileName));
    }

    public static byte[] readByteArrayFromBinaryFileBuffered(File file) throws IOException {
        try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
            return inputStream.readAllBytes();
        }
    }

    public static void writeRectButtonToBinaryFile(File file, RectButton rectButton) throws IOException {
        try (DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(file))) {
            outputStream.writeInt(rectButton.getTopLeft().getX());
            outputStream.writeInt(rectButton.getTopLeft().getY());
            outputStream.writeInt(rectButton.getBottomRight().getX());
            outputStream.writeInt(rectButton.getBottomRight().getY());
            outputStream.writeUTF(rectButton.getState().name());
            outputStream.writeUTF(rectButton.getText());
        }
    }

    public static RectButton readRectButtonFromBinaryFile(File file) throws IOException, WindowException {
        try (DataInputStream inputStream = new DataInputStream(new FileInputStream(file))) {
            return new RectButton(new Point(inputStream.readInt(), inputStream.readInt()),
                    new Point(inputStream.readInt(), inputStream.readInt()),
                    inputStream.readUTF(),inputStream.readUTF());

        }
    }

    public static void writeRectButtonArrayToBinaryFile(File file, RectButton[] rects) throws IOException {
        try (DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(file))) {
            for(RectButton elem : rects) {
                outputStream.writeInt(elem.getTopLeft().getX());
                outputStream.writeInt(elem.getTopLeft().getY());
                outputStream.writeInt(elem.getBottomRight().getX());
                outputStream.writeInt(elem.getBottomRight().getY());
            }
        }
    }

    public static void modifyRectButtonArrayInBinaryFile(File file) throws IOException {
        long pos = 0;
        int buf = 0;
        try (RandomAccessFile stream = new RandomAccessFile(file, "rw")) {
            while(pos < file.length()) {
                buf = stream.readInt();
                stream.seek(pos);
                stream.writeInt(buf+1);
                pos += 8;
                stream.seek(pos);
            }
        }
    }

    public static RectButton[] readRectButtonArrayFromBinaryFile(File file) throws IOException, WindowException {
        int length = (int) (file.length()/16);
        RectButton []rectButton = new RectButton[length];
        try (DataInputStream inputStream = new DataInputStream(new FileInputStream(file))) {
            for(int i = 0; i < length; i++) {
                rectButton[i] = new RectButton(new Point(inputStream.readInt(), inputStream.readInt()),
                        new Point(inputStream.readInt(), inputStream.readInt()),
                        "ACTIVE", "OK");
            }
        }
        return rectButton;
    }

    public static void writeRectButtonToTextFileOneLine(File file, RectButton rectButton) throws IOException {
        String str = null;
        try (OutputStreamWriter outputStream = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
            str = new String(rectButton.getTopLeft().getX() + " " +
                    rectButton.getTopLeft().getY() + " " +
                    rectButton.getBottomRight().getX() + " " +
                    rectButton.getBottomRight().getY() + " " +
                    rectButton.getState().name() + " " +
                    rectButton.getText());
            outputStream.write(str);
        }
    }

    public static RectButton readRectButtonFromTextFileOneLine(File file) throws IOException, WindowException {
        String []strArray = null;
        try (BufferedReader inputStream = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            strArray = inputStream.readLine().split(" ");
            return new RectButton(new Point(Integer.parseInt(strArray[0]), Integer.parseInt(strArray[1])),
                    new Point(Integer.parseInt(strArray[2]), Integer.parseInt(strArray[3])),
                    strArray[4], strArray[5]);
        }
    }

    public static void writeRectButtonToTextFileSixLines(File file, RectButton rectButton) throws IOException {
        try (BufferedWriter outputStream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8))) {
            outputStream.write(Integer.toString(rectButton.getTopLeft().getX()));
            outputStream.newLine();
            outputStream.write(Integer.toString(rectButton.getTopLeft().getY()));
            outputStream.newLine();
            outputStream.write(Integer.toString(rectButton.getBottomRight().getX()));
            outputStream.newLine();
            outputStream.write(Integer.toString(rectButton.getBottomRight().getY()));
            outputStream.newLine();
            outputStream.write(rectButton.getState().name());
            outputStream.newLine();
            outputStream.write(rectButton.getText());
        }
    }

    public static RectButton readRectButtonFromTextFileSixLines(File file) throws IOException, WindowException {
        try (BufferedReader inputStream = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            return new RectButton(
                    new Point(Integer.parseInt(inputStream.readLine()), Integer.parseInt(inputStream.readLine())),
                    new Point(Integer.parseInt(inputStream.readLine()), Integer.parseInt(inputStream.readLine())),
                    inputStream.readLine(), inputStream.readLine());
        }
    }

    public static void  writeTraineeToTextFileOneLine(File file, Trainee trainee) throws IOException {
        String str = null;
        try (OutputStreamWriter outputStream = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
            str = new String(trainee.getFullName() + " " + trainee.getRating());
            outputStream.write(str);
        }
    }

    public static Trainee  readTraineeFromTextFileOneLine(File file) throws IOException, TrainingException {
        String []strArray = null;
        try (BufferedReader inputStream = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            strArray = inputStream.readLine().split(" ");
            return new Trainee(strArray[0],strArray[1], Integer.parseInt(strArray[2]));
        }
    }

    public static void  writeTraineeToTextFileThreeLines(File file, Trainee trainee) throws IOException {
        try (BufferedWriter outputStream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8))) {
            outputStream.write(trainee.getFirstName());
            outputStream.newLine();
            outputStream.write(trainee.getLastName());
            outputStream.newLine();
            outputStream.write(Integer.toString(trainee.getRating()));
        }
    }

    public static Trainee  readTraineeFromTextFileThreeLines(File file) throws IOException, TrainingException {
        try (BufferedReader inputStream = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            return new Trainee(inputStream.readLine(), inputStream.readLine(), Integer.parseInt(inputStream.readLine()));
        }
    }

    public static void  serializeTraineeToBinaryFile(File file, Trainee trainee) throws IOException {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file))){
            outputStream.writeObject(trainee);
        }
    }

    public static Trainee  deserializeTraineeFromBinaryFile(File file) throws IOException, ClassNotFoundException, TrainingException {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file))) {
            return (Trainee) inputStream.readObject();
        }
    }

    public static String  serializeTraineeToJsonString(Trainee trainee){
        Gson gson = new Gson();
        return gson.toJson(trainee);
    }

    public static Trainee  deserializeTraineeFromJsonString(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, Trainee.class);
    }

    public static void  serializeTraineeToJsonFile(File file, Trainee trainee) throws IOException {
        try (BufferedWriter outputStream = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8))) {
            outputStream.write(serializeTraineeToJsonString(trainee));
        }
    }

    public static Trainee  deserializeTraineeFromJsonFile(File file) throws IOException {
        try (BufferedReader inputStream = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            return deserializeTraineeFromJsonString(inputStream.readLine());
        }
    }

}
