package net.thumbtack.school.introduction;


public class FirstSteps {

    public int sum (int x, int y){
        return x+y;
    }

    public int mul (int x, int y){
        return x*y;
    }

    public int div (int x, int y){
        return x/y;
    }

    public int mod (int x, int y){
        return x % y;
    }

    public boolean isEqual (int x, int y){
        return x == y;
    }

    public boolean isGreater (int x, int y){
        return x > y;
    }

    public boolean isInsideRect(int xLeft, int yTop, int xRight, int yBottom, int x, int y){
        return x >= xLeft && x <= xRight && y >= yTop && y <= yBottom;
    }

    public int sum(int[] array){
        if(array.length == 0){
            return 0;
        }
        int totalSum = 0;
        for (int elem : array) {
            totalSum += elem;
        }
        return totalSum;
    }

    public int mul(int[] array){
        if(array.length == 0){
            return 0;
        }
        int totalMul = 1;
        for (int elem : array) {
            totalMul *= elem;
        }
        return totalMul;
    }

    public int min(int[] array){
        if(array.length == 0){
            return Integer.MAX_VALUE;
        }

        int min = array[0];
        for (int elem : array) {
            if (elem < min){
                min = elem;
            }
        }
        return min;
    }

    public int max(int[] array){
        if(array.length == 0){
            return Integer.MIN_VALUE;
        }
        int max = array[0];
        for (int elem : array) {
            if (elem > max){
                max = elem;
            }
        }
        return max;
    }

    public double average(int[] array){
        if(array.length == 0){
            return 0;
        }
        return (double) sum(array)/array.length;
    }

    public boolean isSortedDescendant(int[] array){
        for (int i = 0; i < array.length - 1; i++) {
            if(array[i] <= array[i+1]){
                return false;
            }
        }
        return true;
    }

    public void cube(int[]array){
        for(int i = 0; i < array.length; i++){
            array[i] *= array[i]*array[i];
        }
    }

    public boolean find(int[]array, int value){
        for (int elem : array) {
            if(elem == value){
                return true;
            }
        }
        return false;
    }

    public void reverse(int[]array){
        int buf;
        int halfLength = array.length/2;
        for(int i = 0; i < halfLength; i++) {
            buf = array[array.length - 1 - i];
            array[array.length - 1 - i] = array[i];
            array[i] = buf;
        }
}

    public boolean isPalindrome(int[]array){
        int halfLength = array.length/2;
        for(int i = 0; i < halfLength; i++) {
            if(array[i] != array[array.length - 1 - i]){
                return false;
            }
        }
        return true;
    }

    public int sum(int[][] matrix){
        int totalSum = 0;
        for (int[] subset : matrix) {
            totalSum += sum(subset);
        }
        return totalSum;
    }

    public int max(int[][] matrix){
        if(matrix.length == 0 || matrix[0].length == 0){
            return Integer.MIN_VALUE;
        }
        int max = matrix[0][0];
        int maxSubset = 0;
        for (int[] subset : matrix) {
            maxSubset = max(subset);
            if(max < maxSubset) {
                max = maxSubset;
            }
        }
        return max;
    }

    public int diagonalMax(int[][] matrix){
        if(matrix.length == 0 || matrix[0].length == 0){
            return Integer.MIN_VALUE;
        }
        int max = matrix[0][0];
        for(int i = 0; i < matrix.length; i++){
            if(max < matrix[i][i]){
                max = matrix[i][i];
            }
        }
        return max;
    }

    public boolean isSortedDescendant(int[][] matrix){
        for (int[] subset : matrix) {
            if (!isSortedDescendant(subset)) {
                return false;
            }
        }
        return true;
    }

}
