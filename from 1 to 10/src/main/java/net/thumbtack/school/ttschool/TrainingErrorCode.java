package net.thumbtack.school.ttschool;

public enum TrainingErrorCode {
    TRAINEE_WRONG_FIRSTNAME("Incorrect firstName data"),
    TRAINEE_WRONG_LASTNAME("Incorrect lastName data"),
    TRAINEE_WRONG_RATING("Incorrect rating data"),
    TRAINEE_WRONG_NAME("Incorrect name data"),
    TRAINEE_WRONG_ROOM("Incorrect room data"),
    TRAINEE_NOT_FOUND("Trainee was not found in List"),
    GROUP_WRONG_NAME("Incorrect group name"),
    GROUP_WRONG_ROOM("Incorrect group room"),
    SCHOOL_WRONG_NAME("Incorrect school name"),
    DUPLICATE_GROUP_NAME("Duplicate name detected"),
    GROUP_NOT_FOUND("Group was not found in Set"),
    DUPLICATE_TRAINEE("Duplicate found"),
    EMPTY_TRAINEE_QUEUE("Queue should not be empty");

    private String errorString;

    TrainingErrorCode(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
