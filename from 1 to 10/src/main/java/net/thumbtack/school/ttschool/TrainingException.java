package net.thumbtack.school.ttschool;

public class TrainingException extends Exception {
    TrainingErrorCode code;

    public TrainingException(TrainingErrorCode code) throws TrainingException {
        if(code == null){
            throw new NullPointerException("Incorrect TrainingErrorCode state");
        }
        this.code = code;
    }

    public TrainingErrorCode getErrorCode(){
        return code;
    }


}
