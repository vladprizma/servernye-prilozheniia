package net.thumbtack.school.concert.exception;

import net.thumbtack.school.concert.errorcode.ServerErrorCode;

public class ServerException extends Exception{
    ServerErrorCode serverErrorCode;

    public ServerException(ServerErrorCode serverErrorCode) throws ServerException {
        if(serverErrorCode == null){
            throw new ServerException(serverErrorCode.USER_WRONG_STATE);
        }
        this.serverErrorCode = serverErrorCode;
    }

    public String getMessage() {
        return serverErrorCode.getErrorString();
    }
}
