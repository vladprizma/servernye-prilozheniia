package net.thumbtack.school.concert.model;

import lombok.*;

import java.util.*;

@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Song {
    @NonNull
    private String title;
    @NonNull
    private List<String> composers;
    @NonNull
    private List<String> wordsAuthors;
    @NonNull
    private String performer;
    @NonNull
    private int duration;
    private transient AbstractUser songAdder;
    private transient int id;
    private transient Map<User, Rating> ratings = new HashMap<>();
    private transient Map<AbstractUser, Comment> comments = new HashMap<>();

    public void addRating(Rating rating){
        this.ratings.put(rating.getUser(), rating);
    }

    public void removeRatingByUser(User user){
        this.ratings.remove(user, ratings.get(user));
    }

    public Rating getRatingByUser(User user){
        return ratings.get(user);
    }

    public void addComment(Comment comment){
        this.comments.put(comment.getUser(), comment);
    }

    public void removeComment(Comment comment){
        this.comments.remove(comment.getUser());
    }

    public Comment getCommentByUser(AbstractUser user){
        return comments.get(user);
    }

}
