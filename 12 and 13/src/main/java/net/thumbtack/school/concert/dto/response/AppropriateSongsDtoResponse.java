package net.thumbtack.school.concert.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class AppropriateSongsDtoResponse {
    private List<String> titles;
}
