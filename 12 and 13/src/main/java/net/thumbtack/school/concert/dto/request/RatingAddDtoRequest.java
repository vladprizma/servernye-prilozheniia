package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

@Getter
@AllArgsConstructor
public class RatingAddDtoRequest {
    private static final int MIN_ID = 1;
    private static final int MAX_RATING = 5;
    private static final int MIN_RATING = 1;
    private String token;
    private int id;
    private int rating;

    public static void validate(RatingAddDtoRequest dtoRequest) throws ServerException {
        if(dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getId() < MIN_ID ||
                dtoRequest.getRating() < MIN_RATING ||
                dtoRequest.getRating() > MAX_RATING){

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }

}
