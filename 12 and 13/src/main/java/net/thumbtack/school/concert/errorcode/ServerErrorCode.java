package net.thumbtack.school.concert.errorcode;

public enum ServerErrorCode {
    DATABASE_FILE_IS_EMPTY("Specified file does not have any data"),
    DATABASE_WRONG_STATE("Database code equals null"),
    DATABASE_FILE_NOT_EXIST("Specified file does not exist"),
    DATABASE_WRONG_FILENAME("Specified filename is incorrect"),

    USER_LOGIN_OCCUPIED("This login already exist"),
    USER_LOGIN_DOES_NOT_EXIST("This login does not exist"),
    USER_PASSWORD_INCORRECT("Wrong password"),
    USER_WRONG_STATE("User code equals null"),
    USER_WRONG_TOKEN("Invalid token"),
    USER_ALREADY_ONLINE("User is online"),

    SONG_DOES_NOT_EXIST("Specified song does not exist"),
    SONG_WRONG_USER_DELETE_ATTEMPT("This user is not who add specified song"),
    SONG_USER_ALREADY_IN_CURRENT_COMMENT_SECTION("This user is already commented specified song"),
    SONG_ALREADY_ADDED("This song is already added"),
    SONG_RATING_ALREADY_ADDED("This song already rated by user"),
    SONG_ADDER_CHANGE_RATING_ATTEMPT("User that add this song cant interact with its rating"),
    SONG_IS_NOT_RATED_BY_USER("This song is not rated by user yet"),
    SONG_DOES_NOT_HAVE_COMMENTS("Song does not have comments yet"),
    SONG_DOES_NOT_HAVE_RATINGS("Song does not have ratings yet"),
    SONG_IS_NOT_COMMENTED_BY_USER("This song is not commented by user yet"),
    SONG_COMMENT_DOES_NOT_HAVE_JOINED_USERS("No one joined to this comment yet"),
    SONG_USER_NOT_JOINED_TO_COMMENT("Current user not joined to this comment"),
    SONG_USER_ALREADY_JOINED_TO_COMMENT("User already joined to this comment"),
    SONG_TITLE_DOES_NOT_EXIST("Song title does not exist"),
    SONG_PERFORMER_DOES_NOT_EXIST("Song performer does not exist"),

    REQUEST_WRONG_PARAMETERS("Parameter set incorrectly"),

    WRONG_JSON("Json data is incorrect");

    private String errorString;

    ServerErrorCode(String errorString){
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
