package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

@Getter
@AllArgsConstructor
public class LogoutDtoRequest {
    private String token;

    public static void validate(LogoutDtoRequest dtoRequest) throws ServerException {
        if(dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("")){

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }

}
