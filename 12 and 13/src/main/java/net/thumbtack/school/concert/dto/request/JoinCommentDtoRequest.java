package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;


@Getter
@AllArgsConstructor
public class JoinCommentDtoRequest {
    private static final int MIN_ID = 1;
    private String token;
    private String commentAuthorLogin;
    private int id;

    public static void validate(JoinCommentDtoRequest dtoRequest) throws ServerException {
        if(dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getCommentAuthorLogin() == null ||
                dtoRequest.getCommentAuthorLogin().equals("") ||
                dtoRequest.getId() < MIN_ID){

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }

}
