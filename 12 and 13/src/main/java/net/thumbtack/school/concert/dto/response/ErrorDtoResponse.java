package net.thumbtack.school.concert.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ErrorDtoResponse {
    private String error;
}
