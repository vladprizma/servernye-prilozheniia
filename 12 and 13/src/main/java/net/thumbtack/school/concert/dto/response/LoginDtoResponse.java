package net.thumbtack.school.concert.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.thumbtack.school.concert.model.User;

@AllArgsConstructor
@Getter
public class LoginDtoResponse {
    private String token;
}
