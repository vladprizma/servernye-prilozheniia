package net.thumbtack.school.concert.daoimpl;

import com.google.common.collect.Multimap;
import net.thumbtack.school.concert.dao.SongDao;
import net.thumbtack.school.concert.database.Database;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.model.Song;

import java.util.List;
import java.util.Set;

public class SongDaoImpl implements SongDao {

    @Override
    public int addSong(Song song) throws ServerException {
        return Database.getDatabase().addSong(song);
    }

    @Override
    public void removeSong(int id) {
        Database.getDatabase().removeSong(id);
    }

    @Override
    public Song getSong(int id) {
        return Database.getDatabase().getSong(id);
    }

    @Override
    public Set<Song> getSongs() {
        return Database.getDatabase().getSongs();
    }

    @Override
    public Multimap<String, Song> getSongsFromComposers(List<String> composers) {
        return Database.getDatabase().getSongsFromComposers(composers);
    }

    @Override
    public Multimap<String, Song> getSongsFromWordsAuthors(List<String> wordsAuthors) {
        return Database.getDatabase().getSongsFromWordsAuthors(wordsAuthors);
    }

    @Override
    public List<Song> getSongsFromPerformer(String performer) {
        return Database.getDatabase().getSongsFromPerformer(performer);
    }

}
