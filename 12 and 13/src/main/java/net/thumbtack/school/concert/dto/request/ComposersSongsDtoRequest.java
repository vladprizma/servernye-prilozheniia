package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

import java.util.List;

@AllArgsConstructor
@Getter
public class ComposersSongsDtoRequest {
    private String token;
    private List<String> composers;

    public static void validate(ComposersSongsDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getComposers() == null ||
                dtoRequest.getComposers().size() == 0 ||
                dtoRequest.getComposers().contains(null) ||
                dtoRequest.getComposers().contains("") ) {

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }

}
