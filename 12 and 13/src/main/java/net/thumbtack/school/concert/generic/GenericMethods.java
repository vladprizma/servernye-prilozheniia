package net.thumbtack.school.concert.generic;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

public class GenericMethods {

    public static <T> T getClassFromJson(Class<T> tClass, String jsonString) throws ServerException {
        try{
            Gson gson = new Gson();
            T dto = gson.fromJson(jsonString, tClass);
            if(dto == null){
                throw new ServerException(ServerErrorCode.WRONG_JSON);
            }
            return dto;
        }
        catch (JsonSyntaxException | ServerException e){
            throw new ServerException(ServerErrorCode.WRONG_JSON);
        }
    }

}
