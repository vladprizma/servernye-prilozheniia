package net.thumbtack.school.concert.server;

import com.google.gson.Gson;
import net.thumbtack.school.concert.dto.request.*;
import net.thumbtack.school.concert.dto.response.*;
import net.thumbtack.school.concert.service.SongService;
import net.thumbtack.school.concert.service.UserService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class TestService {
    private Gson gson = new Gson();
    private UserService userService = new UserService();
    private SongService songService = new SongService();
    private EmptyJsonDtoResponse emptyJsonDtoResponse = new EmptyJsonDtoResponse();


    @Test
    public void testSongActionExceptions(){

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name", "name", "test1", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest addSongDtoRequest1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title", null, null, "performer", 400);

        ErrorDtoResponse errorDtoResponse1 = new ErrorDtoResponse("Parameter set incorrectly");
        assertEquals(gson.toJson(errorDtoResponse1), songService.addSong(gson.toJson(addSongDtoRequest1)));


        // parsingAddSong
        List<String> composer = new ArrayList<>();
        composer.add("composer");
        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors");
        AddSongDtoRequest addSongDtoRequest2 = new AddSongDtoRequest("not actual token",
                "test1", composer, wordsAuthors, "performer", 400);

        ErrorDtoResponse errorDtoResponse2 = new ErrorDtoResponse("Invalid token");
        assertEquals(gson.toJson(errorDtoResponse2), songService.addSong(gson.toJson(addSongDtoRequest2)));


        // parsingAddSong
        AddSongDtoRequest addSongDtoRequest3 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "test1", composer, wordsAuthors, "performer", 400);

        AddSongDtoResponse addSongDtoResponse =
                gson.fromJson(songService.addSong(gson.toJson(addSongDtoRequest3)), AddSongDtoResponse.class);


        // parsingRegistration
        RegisterDtoRequest registerDtoRequest2 =
                new RegisterDtoRequest("name", "name", "test1.5", "password");

        LoginDtoResponse registerDtoResponse2 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest2)), LoginDtoResponse.class);


        // parsingDeleteSong
        DeleteSongDtoRequest deleteSongDtoRequest =
                new DeleteSongDtoRequest(registerDtoResponse2.getToken(), addSongDtoResponse.getId());

        ErrorDtoResponse errorDtoResponse5 = new ErrorDtoResponse("This user is not who add specified song");
        assertEquals(gson.toJson(errorDtoResponse5), songService.deleteSong(gson.toJson(deleteSongDtoRequest)));

    }

    @Test
    public void testCommentActionExceptions(){

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name", "name", "test2", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        // parsingAddSong
        List<String> composer = new ArrayList<>();
        composer.add("composer");
        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors");
        AddSongDtoRequest addSongDtoRequest1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "test2", composer, wordsAuthors, "performer", 400);

       AddSongDtoResponse addSongDtoResponse =
                 gson.fromJson(songService.addSong(gson.toJson(addSongDtoRequest1)), AddSongDtoResponse.class);


        // parsingAddComment
        AddCommentDtoRequest addCommentDtoRequest2 = new AddCommentDtoRequest(registerDtoResponse1.getToken(),
                addSongDtoResponse.getId(), "test2 more like testAddCommentExceptions");

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.addComment(gson.toJson(addCommentDtoRequest2)));


        // parsingAddComment
        AddCommentDtoRequest addCommentDtoRequest3 = new AddCommentDtoRequest(registerDtoResponse1.getToken(),
                addSongDtoResponse.getId(), "test2 more like testAddCommentExceptions");

        ErrorDtoResponse errorDtoResponse4 = new ErrorDtoResponse("This user is already commented specified song");
        assertEquals(gson.toJson(errorDtoResponse4), songService.addComment(gson.toJson(addCommentDtoRequest3)));


        // parsingRegistration
        RegisterDtoRequest registerDtoRequest2 =
                new RegisterDtoRequest("name", "name", "test2.5", "password");

        LoginDtoResponse registerDtoResponse2 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest2)), LoginDtoResponse.class);


        // parsingChangeComment
        ChangeCommentDtoRequest changeCommentDtoRequest2 = new ChangeCommentDtoRequest(registerDtoResponse2.getToken(),
                addSongDtoResponse.getId(), "Can I change comment?");

        ErrorDtoResponse errorDtoResponse5 = new ErrorDtoResponse("This song is not commented by user yet");
        assertEquals(gson.toJson(errorDtoResponse5), songService.changeComment(gson.toJson(changeCommentDtoRequest2)));


    }

    @Test
    public void testJoinActionExceptions(){


        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name", "name", "test3", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        // parsingAddSong
        List<String> composer = new ArrayList<>();
        composer.add("composer");
        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors");
        AddSongDtoRequest addSongDtoRequest1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "test3", composer, wordsAuthors, "performer", 400);

        AddSongDtoResponse addSongDtoResponse =
                gson.fromJson(songService.addSong(gson.toJson(addSongDtoRequest1)), AddSongDtoResponse.class);


        // parsingAddComment
        AddCommentDtoRequest addCommentDtoRequest1 = new AddCommentDtoRequest(registerDtoResponse1.getToken(),
                addSongDtoResponse.getId(), "test2 more like testAddCommentExceptions");

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.addComment(gson.toJson(addCommentDtoRequest1)));


        // parsingRegistration
        RegisterDtoRequest registerDtoRequest2 =
                new RegisterDtoRequest("name", "name", "test3.5", "password");

        LoginDtoResponse registerDtoResponse2 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest2)), LoginDtoResponse.class);


        // parsingJoinToComment
        JoinCommentDtoRequest joinCommentDtoRequest = new JoinCommentDtoRequest(
                registerDtoResponse2.getToken(), "testt", addSongDtoResponse.getId());

        ErrorDtoResponse errorDtoResponse2 = new ErrorDtoResponse("This login does not exist");
        assertEquals(gson.toJson(errorDtoResponse2), songService.joinToComment(gson.toJson(joinCommentDtoRequest)));


        // parsingJoinToComment
        JoinCommentDtoRequest joinCommentDtoRequest2 = new JoinCommentDtoRequest(
                registerDtoResponse2.getToken(), "test3", addSongDtoResponse.getId());
        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.joinToComment(gson.toJson(joinCommentDtoRequest2)));


        ErrorDtoResponse errorDtoResponse3 = new ErrorDtoResponse("User already joined to this comment");
        assertEquals(gson.toJson(errorDtoResponse3), songService.joinToComment(gson.toJson(joinCommentDtoRequest2)));


        // parsingCancelJoinToComment
        CancelJoinToCommentDtoRequest cancelJoinToCommentDtoRequest2 = new CancelJoinToCommentDtoRequest(
                registerDtoResponse1.getToken(), "test3", addSongDtoResponse.getId());

        ErrorDtoResponse errorDtoResponse4 = new ErrorDtoResponse("Current user not joined to this comment");
        assertEquals(gson.toJson(errorDtoResponse4), songService.cancelJoinToComment(gson.toJson(cancelJoinToCommentDtoRequest2)));

    }

    @Test
    public void testRatingActionExceptions(){


        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name", "name", "test4", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        // parsingAddSong
        List<String> composer = new ArrayList<>();
        composer.add("composer");
        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors");
        AddSongDtoRequest addSongDtoRequest1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "test4", composer, wordsAuthors, "performer", 400);


        AddSongDtoResponse addSongDtoResponse =
                gson.fromJson(songService.addSong(gson.toJson(addSongDtoRequest1)), AddSongDtoResponse.class);


        // parsingAddRating
        RatingAddDtoRequest ratingAddDtoRequest1 = new RatingAddDtoRequest(
                registerDtoResponse1.getToken(), addSongDtoResponse.getId(), 3);

        ErrorDtoResponse errorDtoResponse1 = new ErrorDtoResponse("User that add this song cant interact with its rating");
        assertEquals(gson.toJson(errorDtoResponse1), songService.addRating(gson.toJson(ratingAddDtoRequest1)));


        // parsingRegistration
        RegisterDtoRequest registerDtoRequest2 =
                new RegisterDtoRequest("name", "name", "test4.5", "password");

        LoginDtoResponse registerDtoResponse2 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest2)), LoginDtoResponse.class);


        // parsingAddRating
        RatingAddDtoRequest ratingAddDtoRequest2 = new RatingAddDtoRequest(
                registerDtoResponse2.getToken(), addSongDtoResponse.getId(), 3);

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.addRating(gson.toJson(ratingAddDtoRequest2)));

        ErrorDtoResponse errorDtoResponse4 = new ErrorDtoResponse("This song already rated by user");
        assertEquals(gson.toJson(errorDtoResponse4), songService.addRating(gson.toJson(ratingAddDtoRequest2)));


        // parsingRegistration
        RegisterDtoRequest registerDtoRequest3 =
                new RegisterDtoRequest("name", "name", "test4.75", "password");

        LoginDtoResponse registerDtoResponse3 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest3)), LoginDtoResponse.class);


        // parsingChangeRating
        RatingChangeDtoRequest ratingChangeDtoRequest2 = new RatingChangeDtoRequest(
                registerDtoResponse3.getToken(), addSongDtoResponse.getId(), 3);

        ErrorDtoResponse errorDtoResponse5 = new ErrorDtoResponse("This song is not rated by user yet");
        assertEquals(gson.toJson(errorDtoResponse5), songService.changeRating(gson.toJson(ratingChangeDtoRequest2)));


    }

    @Test
    public void testDeleteSongWithRating(){

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name1", "name1", "login1", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        // parsingAddSong
        List<String> composer = new ArrayList<>();
        composer.add("composer");
        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors");
        AddSongDtoRequest addSongDtoRequest1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title", composer, wordsAuthors, "performer", 400);

        AddSongDtoResponse addSongDtoResponse =
                gson.fromJson(songService.addSong(gson.toJson(addSongDtoRequest1)), AddSongDtoResponse.class);


        // parsingRegistration
        RegisterDtoRequest registerDtoRequest2 =
                new RegisterDtoRequest("name2", "name2", "login2", "password");
        LoginDtoResponse registerDtoResponse2 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest2)), LoginDtoResponse.class);


        // parsingAddRating
        RatingAddDtoRequest ratingAddDtoRequest = new RatingAddDtoRequest(registerDtoResponse2.getToken(),
                addSongDtoResponse.getId(), 5);
        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.addRating(gson.toJson(ratingAddDtoRequest)));


        // parsingChangeRating
        RatingChangeDtoRequest ratingChangeDtoRequest = new RatingChangeDtoRequest(registerDtoResponse2.getToken(),
                addSongDtoResponse.getId(), 3);

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.changeRating(gson.toJson(ratingChangeDtoRequest)));


        // parsingDeleteSong
        DeleteSongDtoRequest deleteSongDtoRequest = new DeleteSongDtoRequest(registerDtoResponse1.getToken(),
                addSongDtoResponse.getId() );
        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.deleteSong(gson.toJson(deleteSongDtoRequest)));

        ErrorDtoResponse errorDtoResponse = new ErrorDtoResponse("This user is not who add specified song");
        assertEquals(gson.toJson(errorDtoResponse), songService.deleteSong(gson.toJson(deleteSongDtoRequest)));
    }

    @Test
    public void testDeleteSongWithoutRating(){

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name3", "name3", "login3", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        // parsingAddSong
        List<String> composer = new ArrayList<>();
        composer.add("composer");
        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors");
        AddSongDtoRequest addSongDtoRequest1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "Darude", composer, wordsAuthors, "Sandstorm", 400);


        AddSongDtoResponse addSongDtoResponse =
                gson.fromJson(songService.addSong(gson.toJson(addSongDtoRequest1)), AddSongDtoResponse.class);


        // parsingRegistration
        RegisterDtoRequest registerDtoRequest2 =
                new RegisterDtoRequest("name4", "name5", "login4", "password");
        LoginDtoResponse registerDtoResponse2 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest2)), LoginDtoResponse.class);


        // parsingAddRating
        RatingAddDtoRequest ratingAddDtoRequest = new RatingAddDtoRequest(registerDtoResponse2.getToken(),
                addSongDtoResponse.getId(), 5);
        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.addRating(gson.toJson(ratingAddDtoRequest)));


        // parsingDeleteRating
        RatingDeleteDtoRequest ratingDeleteDtoRequest = new RatingDeleteDtoRequest(registerDtoResponse2.getToken(),
                addSongDtoResponse.getId());

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.deleteRating(gson.toJson(ratingDeleteDtoRequest)));


        // parsingDeleteSong
        DeleteSongDtoRequest deleteSongDtoRequest = new DeleteSongDtoRequest(registerDtoResponse1.getToken(),
                addSongDtoResponse.getId() );
        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.deleteSong(gson.toJson(deleteSongDtoRequest)));

        ErrorDtoResponse errorDtoResponse = new ErrorDtoResponse("Specified song does not exist");
        assertEquals(gson.toJson(errorDtoResponse), songService.deleteSong(gson.toJson(deleteSongDtoRequest)));

    }

    @Test
    public void testChangeCommentWithJoinedUser(){

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name5", "name5", "login5", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        // parsingAddSong
        List<String> composer = new ArrayList<>();
        composer.add("composer");
        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors");
        AddSongDtoRequest addSongDtoRequest1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title2", composer, wordsAuthors, "performer", 400);


        AddSongDtoResponse addSongDtoResponse =
                gson.fromJson(songService.addSong(gson.toJson(addSongDtoRequest1)), AddSongDtoResponse.class);


        // addCommentDtoRequest
        AddCommentDtoRequest addCommentDtoRequest = new AddCommentDtoRequest(registerDtoResponse1.getToken(),
                addSongDtoResponse.getId(), "Why does this song called like that?");

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.addComment(gson.toJson(addCommentDtoRequest)));

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest2 =
                new RegisterDtoRequest("name6", "name6", "login6", "password");

        LoginDtoResponse registerDtoResponse2 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest2)), LoginDtoResponse.class);


        // joinCommentDtoRequest
        JoinCommentDtoRequest joinCommentDtoRequest = new JoinCommentDtoRequest(registerDtoResponse2.getToken(),
                "login5", addSongDtoResponse.getId());

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.joinToComment(gson.toJson(joinCommentDtoRequest)));

        // changeCommentDtoRequest
        ChangeCommentDtoRequest changeCommentDtoRequest = new ChangeCommentDtoRequest(registerDtoResponse1.getToken(),
                addSongDtoResponse.getId(), "Don't join to my comment!!");

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.changeComment(gson.toJson(changeCommentDtoRequest)));

    }

    @Test
    public void testChangeCommentWithoutJoinedUser(){

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name7", "name7", "login7", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        // parsingAddSong
        List<String> composer = new ArrayList<>();
        composer.add("composer");
        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors");
        AddSongDtoRequest addSongDtoRequest1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title3", composer, wordsAuthors, "performer", 400);


        AddSongDtoResponse addSongDtoResponse =
                gson.fromJson(songService.addSong(gson.toJson(addSongDtoRequest1)), AddSongDtoResponse.class);


        // addCommentDtoRequest
        AddCommentDtoRequest addCommentDtoRequest = new AddCommentDtoRequest(registerDtoResponse1.getToken(),
                addSongDtoResponse.getId(), "Why does this song called like that? Deja vu...");

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.addComment(gson.toJson(addCommentDtoRequest)));

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest2 =
                new RegisterDtoRequest("name8", "name8", "login8", "password");

        LoginDtoResponse registerDtoResponse2 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest2)), LoginDtoResponse.class);


        // joinCommentDtoRequest
        JoinCommentDtoRequest joinCommentDtoRequest = new JoinCommentDtoRequest(registerDtoResponse2.getToken(),
                "login7", addSongDtoResponse.getId());

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.joinToComment(gson.toJson(joinCommentDtoRequest)));

        // cancelJoinToCommentDtoRequest
        CancelJoinToCommentDtoRequest cancelJoinToCommentDtoRequest = new CancelJoinToCommentDtoRequest(registerDtoResponse2.getToken(),
                "login7", addSongDtoResponse.getId());

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.cancelJoinToComment(gson.toJson(cancelJoinToCommentDtoRequest)));

        // changeCommentDtoRequest
        ChangeCommentDtoRequest changeCommentDtoRequest = new ChangeCommentDtoRequest(registerDtoResponse1.getToken(),
                addSongDtoResponse.getId(), "I remember! In previous test, song title is title2, very original");

        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.changeComment(gson.toJson(changeCommentDtoRequest)));


    }

    @Test
    public void testUserDelete(){

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name1", "name1", "login12", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        // parsingAddSong
        List<String> composer = new ArrayList<>();
        composer.add("composer");
        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors");
        AddSongDtoRequest addSongDtoRequest1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "titleSafe", composer, wordsAuthors, "performer", 400);


        AddSongDtoResponse addSongDtoResponse1 =
                gson.fromJson(songService.addSong(gson.toJson(addSongDtoRequest1)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest addSongDtoRequest2 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "titleToDelete", composer, wordsAuthors, "performer", 400);

        AddSongDtoResponse addSongDtoResponse2 =
                gson.fromJson(songService.addSong(gson.toJson(addSongDtoRequest2)), AddSongDtoResponse.class);


        // parsingRegistration
        RegisterDtoRequest registerDtoRequest2 =
                new RegisterDtoRequest("name2", "name2", "login11", "password");
        LoginDtoResponse registerDtoResponse2 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest2)), LoginDtoResponse.class);


        // parsingAddRating
        RatingAddDtoRequest ratingAddDtoRequest = new RatingAddDtoRequest(registerDtoResponse2.getToken(),
                addSongDtoResponse2.getId(), 5);
        assertEquals(gson.toJson(emptyJsonDtoResponse), songService.addRating(gson.toJson(ratingAddDtoRequest)));


        // parsingDeleteSong
        DeleteUserDtoRequest deleteUserDtoRequest = new DeleteUserDtoRequest(registerDtoResponse1.getToken());

        assertEquals(gson.toJson(emptyJsonDtoResponse), userService.deleteUser(gson.toJson(deleteUserDtoRequest)));
    }

    @Test
    public void testGetComposersSongs(){

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name", "name", "login10", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        List<String> composer1 = new ArrayList<>();
        composer1.add("composer1");
        composer1.add("composer2");
        composer1.add("composer3");
        List<String> composer2 = new ArrayList<>();
        composer2.add("composer1");
        composer2.add("composer2");
        List<String> composer3 = new ArrayList<>();
        composer3.add("composer1");
        List<String> composer4 = new ArrayList<>();
        composer4.add("composer2");
        List<String> composer5 = new ArrayList<>();
        composer5.add("composer1");
        composer5.add("composer4");
        composer5.add("composer5");
        List<String> composer6 = new ArrayList<>();
        composer6.add("composer3");
        composer6.add("composer4");
        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors");

        // parsingAddSong
        AddSongDtoRequest song1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title1", composer1, wordsAuthors, "performer1", 400);

        AddSongDtoResponse addSongDtoResponse1 =
                gson.fromJson(songService.addSong(gson.toJson(song1)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest song2 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title2", composer2, wordsAuthors, "performer2", 400);

        AddSongDtoResponse addSongDtoResponse2 =
                gson.fromJson(songService.addSong(gson.toJson(song2)), AddSongDtoResponse.class);


        List<String> wordsAuthors2 = new ArrayList<>();
        wordsAuthors2.add("wordsAuthors");

        // parsingAddSong
        AddSongDtoRequest song3 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title3", composer3, wordsAuthors2, "performer3", 400);

        AddSongDtoResponse addSongDtoResponse3 =
                gson.fromJson(songService.addSong(gson.toJson(song3)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest song4 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title4", composer4, wordsAuthors2, "performer4", 400);

        AddSongDtoResponse addSongDtoResponse4 =
                gson.fromJson(songService.addSong(gson.toJson(song4)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest song5 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title5", composer5, wordsAuthors2, "performer5", 400);

        AddSongDtoResponse addSongDtoResponse5 =
                gson.fromJson(songService.addSong(gson.toJson(song5)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest song6 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title6", composer6, wordsAuthors2, "performer6", 400);

        AddSongDtoResponse addSongDtoResponse6 =
                gson.fromJson(songService.addSong(gson.toJson(song6)), AddSongDtoResponse.class);


        // getSongsFromComposers
        ComposersSongsDtoRequest composersSongsDtoRequest1 =
                new ComposersSongsDtoRequest(registerDtoResponse1.getToken(), composer3);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse1 = gson.fromJson(
                songService.getSongsFromComposers(gson.toJson(composersSongsDtoRequest1)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse1.getTitles().contains("title1"));
        assertTrue(appropriateSongsDtoResponse1.getTitles().contains("title2"));
        assertTrue(appropriateSongsDtoResponse1.getTitles().contains("title3"));
        assertTrue(appropriateSongsDtoResponse1.getTitles().contains("title5"));
        assertFalse(appropriateSongsDtoResponse1.getTitles().contains("title4"));
        assertFalse(appropriateSongsDtoResponse1.getTitles().contains("title6"));

        // getSongsFromComposers
        ComposersSongsDtoRequest composersSongsDtoRequest2 =
                new ComposersSongsDtoRequest(registerDtoResponse1.getToken(), composer2);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse2 = gson.fromJson(
                songService.getSongsFromComposers(gson.toJson(composersSongsDtoRequest2)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse2.getTitles().contains("title1"));
        assertTrue(appropriateSongsDtoResponse2.getTitles().contains("title2"));
        assertFalse(appropriateSongsDtoResponse2.getTitles().contains("title3"));
        assertFalse(appropriateSongsDtoResponse2.getTitles().contains("title4"));
        assertFalse(appropriateSongsDtoResponse2.getTitles().contains("title5"));
        assertFalse(appropriateSongsDtoResponse2.getTitles().contains("title6"));


        // getSongsFromComposers
        ComposersSongsDtoRequest composersSongsDtoRequest3 =
                new ComposersSongsDtoRequest(registerDtoResponse1.getToken(), composer1);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse3 = gson.fromJson(
                songService.getSongsFromComposers(gson.toJson(composersSongsDtoRequest3)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse3.getTitles().contains("title1"));
        assertFalse(appropriateSongsDtoResponse3.getTitles().contains("title2"));
        assertFalse(appropriateSongsDtoResponse3.getTitles().contains("title3"));
        assertFalse(appropriateSongsDtoResponse3.getTitles().contains("title4"));
        assertFalse(appropriateSongsDtoResponse3.getTitles().contains("title5"));
        assertFalse(appropriateSongsDtoResponse3.getTitles().contains("title6"));

        List<String> composer = new ArrayList<>();
        composer.add("composer5");

        // getSongsFromComposers
        ComposersSongsDtoRequest composersSongsDtoRequest4 =
                new ComposersSongsDtoRequest(registerDtoResponse1.getToken(), composer);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse4 = gson.fromJson(
                songService.getSongsFromComposers(gson.toJson(composersSongsDtoRequest4)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse4.getTitles().contains("title5"));
        assertFalse(appropriateSongsDtoResponse4.getTitles().contains("title1"));
        assertFalse(appropriateSongsDtoResponse4.getTitles().contains("title2"));
        assertFalse(appropriateSongsDtoResponse4.getTitles().contains("title3"));
        assertFalse(appropriateSongsDtoResponse4.getTitles().contains("title4"));
        assertFalse(appropriateSongsDtoResponse4.getTitles().contains("title6"));


        composer.remove("composer5");
        composer.add("composer6");

        // getSongsFromComposers
        ComposersSongsDtoRequest composersSongsDtoRequest5 =
                new ComposersSongsDtoRequest(registerDtoResponse1.getToken(), composer);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse5 = gson.fromJson(
                songService.getSongsFromComposers(gson.toJson(composersSongsDtoRequest5)), AppropriateSongsDtoResponse.class);


        assertEquals(0, appropriateSongsDtoResponse5.getTitles().size());


        composer.add("composer1");

        // getSongsFromComposers
        ComposersSongsDtoRequest composersSongsDtoRequest6 =
                new ComposersSongsDtoRequest(registerDtoResponse1.getToken(), composer);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse6 = gson.fromJson(
                songService.getSongsFromComposers(gson.toJson(composersSongsDtoRequest6)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse6.getTitles().contains("title1"));
        assertTrue(appropriateSongsDtoResponse6.getTitles().contains("title2"));
        assertTrue(appropriateSongsDtoResponse6.getTitles().contains("title3"));
        assertTrue(appropriateSongsDtoResponse6.getTitles().contains("title5"));
        assertFalse(appropriateSongsDtoResponse6.getTitles().contains("title4"));
        assertFalse(appropriateSongsDtoResponse6.getTitles().contains("title6"));



    }

    @Test
    public void testGetWordsAuthorsSongs(){

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name", "name", "login15", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        List<String> composer = new ArrayList<>();
        composer.add("composer");
        List<String> wordsAuthors1 = new ArrayList<>();
        wordsAuthors1.add("wordsAuthors1");
        wordsAuthors1.add("wordsAuthors2");
        wordsAuthors1.add("wordsAuthors3");
        List<String> wordsAuthors2 = new ArrayList<>();
        wordsAuthors2.add("wordsAuthors1");
        wordsAuthors2.add("wordsAuthors2");
        List<String> wordsAuthors3 = new ArrayList<>();
        wordsAuthors3.add("wordsAuthors1");
        List<String> wordsAuthors4 = new ArrayList<>();
        wordsAuthors4.add("wordsAuthors2");
        List<String> wordsAuthors5 = new ArrayList<>();
        wordsAuthors5.add("wordsAuthors1");
        wordsAuthors5.add("wordsAuthors4");
        wordsAuthors5.add("wordsAuthors5");
        List<String> wordsAuthors6 = new ArrayList<>();
        wordsAuthors6.add("wordsAuthors3");
        wordsAuthors6.add("wordsAuthors4");

        // parsingAddSong
        AddSongDtoRequest song1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title1", composer, wordsAuthors1, "performer1", 400);

        AddSongDtoResponse addSongDtoResponse1 =
                gson.fromJson(songService.addSong(gson.toJson(song1)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest song2 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title2", composer, wordsAuthors2, "performer2", 400);

        AddSongDtoResponse addSongDtoResponse2 =
                gson.fromJson(songService.addSong(gson.toJson(song2)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest song3 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title3", composer, wordsAuthors3, "performer3", 400);

        AddSongDtoResponse addSongDtoResponse3 =
                gson.fromJson(songService.addSong(gson.toJson(song3)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest song4 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title4", composer, wordsAuthors4, "performer4", 400);

        AddSongDtoResponse addSongDtoResponse4 =
                gson.fromJson(songService.addSong(gson.toJson(song4)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest song5 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title5", composer, wordsAuthors5, "performer5", 400);

        AddSongDtoResponse addSongDtoResponse5 =
                gson.fromJson(songService.addSong(gson.toJson(song5)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest song6 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title6", composer, wordsAuthors6, "performer6", 400);

        AddSongDtoResponse addSongDtoResponse6 =
                gson.fromJson(songService.addSong(gson.toJson(song6)), AddSongDtoResponse.class);


        // getSongsFromWordsAuthors
        WordsAuthorsSongsDtoRequest wordsAuthorsSongsDtoRequest1 =
                new WordsAuthorsSongsDtoRequest(registerDtoResponse1.getToken(), wordsAuthors3);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse1 = gson.fromJson(
                songService.getSongsFromWordsAuthors(gson.toJson(wordsAuthorsSongsDtoRequest1)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse1.getTitles().contains("title1"));
        assertTrue(appropriateSongsDtoResponse1.getTitles().contains("title2"));
        assertTrue(appropriateSongsDtoResponse1.getTitles().contains("title3"));
        assertTrue(appropriateSongsDtoResponse1.getTitles().contains("title5"));
        assertFalse(appropriateSongsDtoResponse1.getTitles().contains("title4"));
        assertFalse(appropriateSongsDtoResponse1.getTitles().contains("title6"));

        // getSongsFromWordsAuthors
        WordsAuthorsSongsDtoRequest wordsAuthorsSongsDtoRequest2 =
                new WordsAuthorsSongsDtoRequest(registerDtoResponse1.getToken(), wordsAuthors2);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse2 = gson.fromJson(
                songService.getSongsFromWordsAuthors(gson.toJson(wordsAuthorsSongsDtoRequest2)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse2.getTitles().contains("title1"));
        assertTrue(appropriateSongsDtoResponse2.getTitles().contains("title2"));
        assertFalse(appropriateSongsDtoResponse2.getTitles().contains("title3"));
        assertFalse(appropriateSongsDtoResponse2.getTitles().contains("title4"));
        assertFalse(appropriateSongsDtoResponse2.getTitles().contains("title5"));
        assertFalse(appropriateSongsDtoResponse2.getTitles().contains("title6"));


        // getSongsFromWordsAuthors
        WordsAuthorsSongsDtoRequest wordsAuthorsSongsDtoRequest3 =
                new WordsAuthorsSongsDtoRequest(registerDtoResponse1.getToken(), wordsAuthors1);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse3 = gson.fromJson(
                songService.getSongsFromWordsAuthors(gson.toJson(wordsAuthorsSongsDtoRequest3)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse3.getTitles().contains("title1"));
        assertFalse(appropriateSongsDtoResponse3.getTitles().contains("title2"));
        assertFalse(appropriateSongsDtoResponse3.getTitles().contains("title3"));
        assertFalse(appropriateSongsDtoResponse3.getTitles().contains("title4"));
        assertFalse(appropriateSongsDtoResponse3.getTitles().contains("title5"));
        assertFalse(appropriateSongsDtoResponse3.getTitles().contains("title6"));

        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors5");

        // getSongsFromWordsAuthors
        WordsAuthorsSongsDtoRequest wordsAuthorsSongsDtoRequest4 =
                new WordsAuthorsSongsDtoRequest(registerDtoResponse1.getToken(), wordsAuthors);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse4 = gson.fromJson(
                songService.getSongsFromWordsAuthors(gson.toJson(wordsAuthorsSongsDtoRequest4)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse4.getTitles().contains("title5"));
        assertFalse(appropriateSongsDtoResponse4.getTitles().contains("title1"));
        assertFalse(appropriateSongsDtoResponse4.getTitles().contains("title2"));
        assertFalse(appropriateSongsDtoResponse4.getTitles().contains("title3"));
        assertFalse(appropriateSongsDtoResponse4.getTitles().contains("title4"));
        assertFalse(appropriateSongsDtoResponse4.getTitles().contains("title6"));


        wordsAuthors.remove("wordsAuthors5");
        wordsAuthors.add("wordsAuthors6");

        // getSongsFromWordsAuthors
        WordsAuthorsSongsDtoRequest wordsAuthorsSongsDtoRequest5 =
                new WordsAuthorsSongsDtoRequest(registerDtoResponse1.getToken(), wordsAuthors);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse5 = gson.fromJson(
                songService.getSongsFromWordsAuthors(gson.toJson(wordsAuthorsSongsDtoRequest5)), AppropriateSongsDtoResponse.class);


        assertEquals(0, appropriateSongsDtoResponse5.getTitles().size());


        wordsAuthors.add("wordsAuthors1");

        // getSongsFromWordsAuthors
        WordsAuthorsSongsDtoRequest wordsAuthorsSongsDtoRequest6 =
                new WordsAuthorsSongsDtoRequest(registerDtoResponse1.getToken(), wordsAuthors);

        AppropriateSongsDtoResponse appropriateSongsDtoResponse6 = gson.fromJson(
                songService.getSongsFromWordsAuthors(gson.toJson(wordsAuthorsSongsDtoRequest6)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse6.getTitles().contains("title1"));
        assertTrue(appropriateSongsDtoResponse6.getTitles().contains("title2"));
        assertTrue(appropriateSongsDtoResponse6.getTitles().contains("title3"));
        assertTrue(appropriateSongsDtoResponse6.getTitles().contains("title5"));
        assertFalse(appropriateSongsDtoResponse6.getTitles().contains("title4"));
        assertFalse(appropriateSongsDtoResponse6.getTitles().contains("title6"));



    }

    @Test
    public void testGetPerformersSong(){

        // parsingRegistration
        RegisterDtoRequest registerDtoRequest1 =
                new RegisterDtoRequest("name", "name", "login16", "password");

        LoginDtoResponse registerDtoResponse1 =
                gson.fromJson(userService.registration(gson.toJson(registerDtoRequest1)), LoginDtoResponse.class);


        List<String> composer = new ArrayList<>();
        composer.add("composer100");
        List<String> wordsAuthors = new ArrayList<>();
        wordsAuthors.add("wordsAuthors1");

        // parsingAddSong
        AddSongDtoRequest song1 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title1", composer, wordsAuthors, "performer1", 400);

        AddSongDtoResponse addSongDtoResponse1 =
                gson.fromJson(songService.addSong(gson.toJson(song1)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest song2 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title2", composer, wordsAuthors, "performer1", 400);

        AddSongDtoResponse addSongDtoResponse2 =
                gson.fromJson(songService.addSong(gson.toJson(song2)), AddSongDtoResponse.class);


        // parsingAddSong
        AddSongDtoRequest song3 = new AddSongDtoRequest(registerDtoResponse1.getToken(),
                "title3", composer, wordsAuthors, "performer2", 400);

        AddSongDtoResponse addSongDtoResponse3 =
                gson.fromJson(songService.addSong(gson.toJson(song3)), AddSongDtoResponse.class);


        // getPerformerSongs
        PerformerSongsDtoRequest performerSongsDtoRequest1 =
                new PerformerSongsDtoRequest(registerDtoResponse1.getToken(), "performer1");

        AppropriateSongsDtoResponse appropriateSongsDtoResponse1 = gson.fromJson(
                songService.getSongsFromPerformer(gson.toJson(performerSongsDtoRequest1)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse1.getTitles().contains("title1"));
        assertTrue(appropriateSongsDtoResponse1.getTitles().contains("title2"));
        assertFalse(appropriateSongsDtoResponse1.getTitles().contains("title3"));


        // getPerformerSongs
        PerformerSongsDtoRequest performerSongsDtoRequest2 =
                new PerformerSongsDtoRequest(registerDtoResponse1.getToken(), "performer2");

        AppropriateSongsDtoResponse appropriateSongsDtoResponse2 = gson.fromJson(
                songService.getSongsFromPerformer(gson.toJson(performerSongsDtoRequest2)), AppropriateSongsDtoResponse.class);


        assertTrue(appropriateSongsDtoResponse2.getTitles().contains("title3"));
        assertFalse(appropriateSongsDtoResponse2.getTitles().contains("title1"));
        assertFalse(appropriateSongsDtoResponse2.getTitles().contains("title2"));



        // getPerformerSongs
        PerformerSongsDtoRequest performerSongsDtoRequest3 =
                new PerformerSongsDtoRequest(registerDtoResponse1.getToken(), "performer3");

        AppropriateSongsDtoResponse appropriateSongsDtoResponse3 = gson.fromJson(
                songService.getSongsFromPerformer(gson.toJson(performerSongsDtoRequest3)), AppropriateSongsDtoResponse.class);


        assertEquals(0, appropriateSongsDtoResponse3.getTitles().size());
    }

}
