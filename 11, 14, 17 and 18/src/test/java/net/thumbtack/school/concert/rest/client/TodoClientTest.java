package net.thumbtack.school.concert.rest.client;

import net.thumbtack.school.concert.dto.request.RegisterDtoRequest;
import net.thumbtack.school.concert.dto.response.AddSongDtoResponse;
import net.thumbtack.school.concert.dto.response.ErrorDtoResponse;
import net.thumbtack.school.concert.dto.response.LoginDtoResponse;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class TodoClientTest extends ClientTestBase {

	@Test
	public void testUserRegistration() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
	}

	@Test
	public void testAddSong() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);
	}

    @Test
    public void testAddSongIncorrectParameters() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		testAddSongIncorrectParameters(loginDtoResponse, "Object is not valid", ServerErrorCode.VALIDATION_ERROR);
    }

	@Test
	public void testDeleteSong() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		testDeleteSong(loginDtoResponse, ServerErrorCode.SUCCESS);
	}

	@Test
	public void testDeleteSongWrongUser() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		testDeleteSongWrongUser(loginDtoResponse, "This user is not who add specified song", ServerErrorCode.SONG_WRONG_USER_DELETE_ATTEMPT);
	}

	@Test
	public void testAddComment() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);
		testAddComment(loginDtoResponse, addSongDtoResponse, ServerErrorCode.SUCCESS);
	}

	@Test
	public void testAddCommentSecondTime() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		testAddCommentSecondTime(loginDtoResponse, "This user is already commented specified song",
				ServerErrorCode.SONG_USER_ALREADY_IN_CURRENT_COMMENT_SECTION);
	}

	@Test
	public void testChangeCommentWithoutAdding() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		testChangeCommentWithoutAdding(loginDtoResponse, "This song is not commented by user yet",
				ServerErrorCode.SONG_IS_NOT_COMMENTED_BY_USER);
	}

	@Test
	public void testJoinToComment() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);
		testAddComment(loginDtoResponse, addSongDtoResponse, ServerErrorCode.SUCCESS);
		testJoinToComment(loginDtoResponse, addSongDtoResponse, ServerErrorCode.SUCCESS);
	}

	@Test
	public void testJoinToCommentWrongLogin() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		testJoinToCommentWrongLogin(loginDtoResponse, "This login does not exist", ServerErrorCode.USER_LOGIN_DOES_NOT_EXIST);
	}

	@Test
	public void testJoinToCommentSecondTime() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		testJoinToCommentSecondTime(loginDtoResponse, "User already joined to this comment", ServerErrorCode.SONG_USER_ALREADY_JOINED_TO_COMMENT);
	}

	@Test
	public void testCancelJoinToComment() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		testCancelJoinToComment(loginDtoResponse, "Current user not joined to this comment", ServerErrorCode.SONG_USER_NOT_JOINED_TO_COMMENT);
	}

	@Test
	public void testAddRating() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);
        RegisterDtoRequest registerDtoRequestSecond =
                new RegisterDtoRequest("name", "name", "test2", "password");
        LoginDtoResponse loginDtoResponseSecond = testUserRegistration(registerDtoRequestSecond, ServerErrorCode.SUCCESS);

		testAddRating(loginDtoResponseSecond, addSongDtoResponse, ServerErrorCode.SUCCESS);
	}

	@Test
	public void testAddRatingSongAdder() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);

		testAddRatingSongAdder(loginDtoResponse, addSongDtoResponse,  "User that add this song cant interact with its rating", ServerErrorCode.SONG_ADDER_CHANGE_RATING_ATTEMPT);
	}

	@Test
	public void testAddRatingSecondTime() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);
		RegisterDtoRequest registerDtoRequestSecond =
				new RegisterDtoRequest("name", "name", "test2", "password");
		LoginDtoResponse loginDtoResponseSecond = testUserRegistration(registerDtoRequestSecond, ServerErrorCode.SUCCESS);

		testAddRatingSecondTime(loginDtoResponseSecond, addSongDtoResponse,  "This song already rated by user", ServerErrorCode.SONG_RATING_ALREADY_ADDED);
	}

	@Test
	public void testChangeRating() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);
		RegisterDtoRequest registerDtoRequestSecond =
				new RegisterDtoRequest("name", "name", "test2", "password");
		LoginDtoResponse loginDtoResponseSecond = testUserRegistration(registerDtoRequestSecond, ServerErrorCode.SUCCESS);

		testChangeRating(loginDtoResponseSecond, addSongDtoResponse, ServerErrorCode.SUCCESS);
	}

	@Test
	public void testChangeRatingWithoutAdding() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);
		RegisterDtoRequest registerDtoRequestSecond =
				new RegisterDtoRequest("name", "name", "test2", "password");
		LoginDtoResponse loginDtoResponseSecond = testUserRegistration(registerDtoRequestSecond, ServerErrorCode.SUCCESS);

		testChangeRatingWithoutAdding(loginDtoResponseSecond, addSongDtoResponse,  "This song is not rated by user yet", ServerErrorCode.SONG_IS_NOT_RATED_BY_USER);
	}

	@Test
	public void testGetComposers() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);

		List<String> composer1 = new ArrayList<>();
		composer1.add("composer1");
		composer1.add("composer2");
		composer1.add("composer3");
		List<String> composer2 = new ArrayList<>();
		composer2.add("composer1");
		composer2.add("composer2");
		List<String> composer3 = new ArrayList<>();
		composer3.add("composer1");
		List<String> composer4 = new ArrayList<>();
		composer4.add("composer2");
		List<String> composer5 = new ArrayList<>();
		composer5.add("composer1");
		composer5.add("composer4");
		composer5.add("composer5");
		List<String> composer6 = new ArrayList<>();
		composer6.add("composer3");
		composer6.add("composer4");
		List<String> wordsAuthors = new ArrayList<>();
		wordsAuthors.add("wordsAuthors");

		testAddSongWithComposers(loginDtoResponse, "title1", composer1, ServerErrorCode.SUCCESS);
		testAddSongWithComposers(loginDtoResponse, "title2", composer2, ServerErrorCode.SUCCESS);
		testAddSongWithComposers(loginDtoResponse, "title3", composer3, ServerErrorCode.SUCCESS);
		testAddSongWithComposers(loginDtoResponse, "title4", composer4, ServerErrorCode.SUCCESS);
		testAddSongWithComposers(loginDtoResponse, "title5", composer5, ServerErrorCode.SUCCESS);
		testAddSongWithComposers(loginDtoResponse, "title6", composer6, ServerErrorCode.SUCCESS);

		List<String> expected = new ArrayList<>();
		expected.add("title1");
		expected.add("title2");
		expected.add("title3");
		expected.add("title5");

		testGetComposers(loginDtoResponse, ServerErrorCode.SUCCESS, composer3, expected);
	}

	@Test
	public void testGetSongsByPerformer() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);

		testAddSongWithPerformer(loginDtoResponse, "title1", "performer", ServerErrorCode.SUCCESS);
		testAddSongWithPerformer(loginDtoResponse, "title2", "performer", ServerErrorCode.SUCCESS);
		testAddSongWithPerformer(loginDtoResponse, "title3", "performer", ServerErrorCode.SUCCESS);
		testAddSongWithPerformer(loginDtoResponse, "title4", "performer1", ServerErrorCode.SUCCESS);
		testAddSongWithPerformer(loginDtoResponse, "title5", "performer1", ServerErrorCode.SUCCESS);
		testAddSongWithPerformer(loginDtoResponse, "title6", "performer1", ServerErrorCode.SUCCESS);

		List<String> expected = new ArrayList<>();
		expected.add("title1");
		expected.add("title2");
		expected.add("title3");

		testGetSongsByPerformer(loginDtoResponse, ServerErrorCode.SUCCESS, "performer", expected);
	}

	@Test
	public void testUserLogin() {
		testUserLogin(ServerErrorCode.SUCCESS);
	}

	@Test
	public void testUserLogout() {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		testUserLogout(loginDtoResponse, ServerErrorCode.SUCCESS);
	}

	@Test
	public void testWrongUrl() {
		Object response = client.post(getBaseURL() + "/wrong", null, ErrorDtoResponse.class);
		checkFailureResponse(response, ServerErrorCode.WRONG_URL);
	}

	
}