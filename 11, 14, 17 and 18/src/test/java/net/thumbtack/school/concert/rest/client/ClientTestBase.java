package net.thumbtack.school.concert.rest.client;

import com.google.common.collect.Ordering;
import net.thumbtack.school.concert.client.Client;
import net.thumbtack.school.concert.database.Database;
import net.thumbtack.school.concert.dto.request.*;
import net.thumbtack.school.concert.dto.response.*;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.service.UserService;
import net.thumbtack.school.concert.utils.MyBatisUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.thumbtack.school.concert.server.Server;
import net.thumbtack.school.concert.settings.Settings;

import javax.validation.ValidationException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ClientTestBase {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClientTestBase.class);

	private UserService userService = new UserService();
	protected static Client client = new Client();
	private static String baseURL;
    private static boolean setUpIsDone = false;

	private static void setBaseUrl() {
		String hostName = null;
		try {
			hostName = InetAddress.getLocalHost().getCanonicalHostName();
		} catch (UnknownHostException e) {
			LOGGER.debug("Can't determine my own host name", e);
		}
		baseURL = "http://" + hostName + ":" + Settings.getRestHTTPPort() + "/api";
	}

	@BeforeAll
	public static void startServer() {
		// Set mode
		Settings.getSettings().setModeToMyBatis();
		if(Settings.getSettings().getMode()){
            if (!setUpIsDone) {
                boolean initSqlSessionFactory = MyBatisUtils.initSqlSessionFactory();
                if (!initSqlSessionFactory) {
                    throw new RuntimeException("Can't create connection, stop");
                }
                setUpIsDone = true;
            }

        }
		setBaseUrl();
		Server.createServer();
	}

	@AfterAll
	public static void stopServer() {
		Server.stopServer();
	}

	@BeforeEach
	public void clearDataBase() {
		userService.truncateAllTables();
	}

	public static String getBaseURL() {
		return baseURL;
	}

	protected void checkFailureResponse(Object response, ServerErrorCode expectedStatus) {
		assertTrue(response instanceof ErrorDtoResponse);
		ErrorDtoResponse errorDtoResponse = (ErrorDtoResponse) response;
		assertEquals(expectedStatus.getErrorString(), errorDtoResponse.getError());
	}

	protected LoginDtoResponse testUserRegistration(RegisterDtoRequest request, ServerErrorCode expectedStatus) {
		Object response = client.post(baseURL + "/concert/users/register", request, LoginDtoResponse.class);
		if (response instanceof LoginDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			return (LoginDtoResponse) response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected LoginDtoResponse testUserLogin(ServerErrorCode expectedStatus) {
		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "test1", "password");
		LoginDtoResponse loginDtoResponse =  testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);
		testUserLogout(loginDtoResponse, ServerErrorCode.SUCCESS);

		LoginDtoRequest request = new LoginDtoRequest(registerDtoRequest.getLogin(), registerDtoRequest.getPassword());

		Object response = client.post(baseURL + "/concert/users/login", request, LoginDtoResponse.class);
		if (response instanceof LoginDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			return (LoginDtoResponse) response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected EmptyJsonDtoResponse testUserLogout(LoginDtoResponse loginDtoResponse, ServerErrorCode expectedStatus) {
		Object response = client.delete(baseURL + "/concert/users/logout", loginDtoResponse.getToken(), EmptyJsonDtoResponse.class);
		if (response instanceof EmptyJsonDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			return (EmptyJsonDtoResponse) response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected AddSongDtoResponse testAddSong(LoginDtoResponse loginDtoResponse, ServerErrorCode expectedStatus){
		List<String> composers = new ArrayList<>();
		composers.add("composer");
		List<String> wordsAuthors = new ArrayList<>();
		wordsAuthors.add("wordsAuthors");
		int idOfFirstSong = 1;

		AddSongDtoRequest request = new AddSongDtoRequest(loginDtoResponse.getToken(),
				"test1", composers, wordsAuthors, "performer", 400);
		Object response = client.post(baseURL + "/concert/songs", request, AddSongDtoResponse.class);
		if (response instanceof AddSongDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			AddSongDtoResponse addSongDtoResponse = (AddSongDtoResponse) response;
			assertEquals(idOfFirstSong, addSongDtoResponse.getId());
			return addSongDtoResponse;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected AddSongDtoResponse testAddSongWithComposers(LoginDtoResponse loginDtoResponse, String title, List<String> composers, ServerErrorCode expectedStatus){
		List<String> wordsAuthors = new ArrayList<>();
		wordsAuthors.add("wordsAuthors");

		AddSongDtoRequest request = new AddSongDtoRequest(loginDtoResponse.getToken(),
				title, composers, wordsAuthors, "performer", 400);
		Object response = client.post(baseURL + "/concert/songs", request, AddSongDtoResponse.class);
		if (response instanceof AddSongDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			return (AddSongDtoResponse) response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected AddSongDtoResponse testAddSongWithWordsAuthors(LoginDtoResponse loginDtoResponse, String title, List<String> wordsAuthors, ServerErrorCode expectedStatus){
		List<String> composers = new ArrayList<>();
		composers.add("composers");

		AddSongDtoRequest request = new AddSongDtoRequest(loginDtoResponse.getToken(),
				title, composers, wordsAuthors, "performer", 400);
		Object response = client.post(baseURL + "/concert/songs", request, AddSongDtoResponse.class);
		if (response instanceof AddSongDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			return (AddSongDtoResponse) response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected AddSongDtoResponse testAddSongWithPerformer(LoginDtoResponse loginDtoResponse, String title, String performer, ServerErrorCode expectedStatus){
		List<String> composers = new ArrayList<>();
		composers.add("composers");
		List<String> wordsAuthors = new ArrayList<>();
		wordsAuthors.add("wordsAuthors");

		AddSongDtoRequest request = new AddSongDtoRequest(loginDtoResponse.getToken(),
				title, composers, wordsAuthors, performer, 400);
		Object response = client.post(baseURL + "/concert/songs", request, AddSongDtoResponse.class);
		if (response instanceof AddSongDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			return (AddSongDtoResponse) response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected ValidationException testAddSongIncorrectParameters(LoginDtoResponse loginDtoResponse, String text, ServerErrorCode expectedStatus){
		AddSongDtoRequest request = new AddSongDtoRequest(loginDtoResponse.getToken(),
				"test1", null, null, "performer", 400);

		Object response = client.post(baseURL + "/concert/songs", request, ValidationException.class);
		if (response instanceof ValidationException) {
			assertEquals(ServerErrorCode.VALIDATION_ERROR, expectedStatus);
			ValidationException ex = (ValidationException) response;
			assertEquals(text, ex.getMessage());
			return ex;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected EmptyJsonDtoResponse testDeleteSong(LoginDtoResponse loginDtoResponse, ServerErrorCode expectedStatus){
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);

		Object response = client.delete(baseURL + "/concert/songs/" + addSongDtoResponse.getId(), loginDtoResponse.getToken(), EmptyJsonDtoResponse.class);
		if (response instanceof EmptyJsonDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			return (EmptyJsonDtoResponse) response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected ErrorDtoResponse testDeleteSongWrongUser(LoginDtoResponse loginDtoResponse, String text, ServerErrorCode expectedStatus){
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);

		RegisterDtoRequest registerDtoRequest =
				new RegisterDtoRequest("name", "name", "wrongSongAdder", "password");
		LoginDtoResponse loginDtoResponseAnother = testUserRegistration(registerDtoRequest, ServerErrorCode.SUCCESS);

		Object response = client.delete(baseURL + "/concert/songs/" + addSongDtoResponse.getId(), loginDtoResponseAnother.getToken(), ErrorDtoResponse.class);
		if (response instanceof ErrorDtoResponse) {
			assertEquals(ServerErrorCode.SONG_WRONG_USER_DELETE_ATTEMPT, expectedStatus);
			ErrorDtoResponse ex = (ErrorDtoResponse) response;
			assertEquals(text, ex.getError());
			return ex;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected EmptyJsonDtoResponse testAddComment(LoginDtoResponse loginDtoResponse, AddSongDtoResponse addSongDtoResponse, ServerErrorCode expectedStatus){
		AddCommentDtoRequest request = new AddCommentDtoRequest(loginDtoResponse.getToken(),
				addSongDtoResponse.getId(), "test2 more like testAddCommentExceptions");

		Object response = client.post(baseURL + "/concert/songs/comments", request, EmptyJsonDtoResponse.class);
		if (response instanceof EmptyJsonDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			return (EmptyJsonDtoResponse) response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected ErrorDtoResponse testAddCommentSecondTime(LoginDtoResponse loginDtoResponse, String text, ServerErrorCode expectedStatus){
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);
		testAddComment(loginDtoResponse, addSongDtoResponse, ServerErrorCode.SUCCESS);

		AddCommentDtoRequest request = new AddCommentDtoRequest(loginDtoResponse.getToken(),
				addSongDtoResponse.getId(), "test2 more like testAddCommentExceptions");

		Object response = client.post(baseURL + "/concert/songs/comments", request, ErrorDtoResponse.class);
		if (response instanceof ErrorDtoResponse) {
			assertEquals(ServerErrorCode.SONG_USER_ALREADY_IN_CURRENT_COMMENT_SECTION, expectedStatus);
			ErrorDtoResponse ex = (ErrorDtoResponse) response;
			assertEquals(text, ex.getError());
			return ex;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

    protected ErrorDtoResponse testChangeCommentWithoutAdding(LoginDtoResponse loginDtoResponse, String text, ServerErrorCode expectedStatus){
        AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);

        AddCommentDtoRequest request = new AddCommentDtoRequest(loginDtoResponse.getToken(),
                addSongDtoResponse.getId(), "test2 more like testAddCommentExceptions");

        Object response = client.put(baseURL + "/concert/songs/comments", request, ErrorDtoResponse.class);
        if (response instanceof ErrorDtoResponse) {
            assertEquals(ServerErrorCode.SONG_IS_NOT_COMMENTED_BY_USER, expectedStatus);
            ErrorDtoResponse ex = (ErrorDtoResponse) response;
            assertEquals(text, ex.getError());
            return ex;
        } else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

	protected EmptyJsonDtoResponse testJoinToComment(LoginDtoResponse loginDtoResponse, AddSongDtoResponse addSongDtoResponse, ServerErrorCode expectedStatus){
		JoinCommentDtoRequest request = new JoinCommentDtoRequest(
				loginDtoResponse.getToken(), "test1", addSongDtoResponse.getId());

		Object response = client.post(baseURL + "/concert/songs/comments/joins", request, EmptyJsonDtoResponse.class);
		if (response instanceof EmptyJsonDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			return (EmptyJsonDtoResponse) response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected ErrorDtoResponse testJoinToCommentWrongLogin(LoginDtoResponse loginDtoResponse, String text, ServerErrorCode expectedStatus){
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);
		testAddComment(loginDtoResponse, addSongDtoResponse, ServerErrorCode.SUCCESS);

		JoinCommentDtoRequest request = new JoinCommentDtoRequest(
				loginDtoResponse.getToken(), "Who is that", addSongDtoResponse.getId());

		Object response = client.post(baseURL + "/concert/songs/comments/joins", request, ErrorDtoResponse.class);
		if (response instanceof ErrorDtoResponse) {
			assertEquals(ServerErrorCode.USER_LOGIN_DOES_NOT_EXIST, expectedStatus);
			ErrorDtoResponse ex = (ErrorDtoResponse) response;
			assertEquals(text, ex.getError());
			return ex;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected ErrorDtoResponse testJoinToCommentSecondTime(LoginDtoResponse loginDtoResponse, String text, ServerErrorCode expectedStatus){
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);
		testAddComment(loginDtoResponse, addSongDtoResponse, ServerErrorCode.SUCCESS);
		testJoinToComment(loginDtoResponse, addSongDtoResponse, ServerErrorCode.SUCCESS);

		JoinCommentDtoRequest request = new JoinCommentDtoRequest(
				loginDtoResponse.getToken(), "test1", addSongDtoResponse.getId());

		Object response = client.post(baseURL + "/concert/songs/comments/joins", request, ErrorDtoResponse.class);
		if (response instanceof ErrorDtoResponse) {
			assertEquals(ServerErrorCode.SONG_USER_ALREADY_JOINED_TO_COMMENT, expectedStatus);
			ErrorDtoResponse ex = (ErrorDtoResponse) response;
			assertEquals(text, ex.getError());
			return ex;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected ErrorDtoResponse testCancelJoinToComment(LoginDtoResponse loginDtoResponse, String text, ServerErrorCode expectedStatus){
		AddSongDtoResponse addSongDtoResponse = testAddSong(loginDtoResponse, ServerErrorCode.SUCCESS);
		testAddComment(loginDtoResponse, addSongDtoResponse, ServerErrorCode.SUCCESS);

		CancelJoinToCommentDtoRequest request = new CancelJoinToCommentDtoRequest(
				loginDtoResponse.getToken(), "test1", addSongDtoResponse.getId());

		Object response = client.delete(baseURL + "/concert/songs/comments/joins/" + request.getCommentAuthorLogin() + "/" + addSongDtoResponse.getId(),
				loginDtoResponse.getToken(), ErrorDtoResponse.class);
		if (response instanceof ErrorDtoResponse) {
			assertEquals(ServerErrorCode.SONG_USER_NOT_JOINED_TO_COMMENT, expectedStatus);
			ErrorDtoResponse ex = (ErrorDtoResponse) response;
			assertEquals(text, ex.getError());
			return ex;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected EmptyJsonDtoResponse testAddRating(LoginDtoResponse loginDtoResponse, AddSongDtoResponse addSongDtoResponse, ServerErrorCode expectedStatus){
		RatingAddDtoRequest request = new RatingAddDtoRequest(
				loginDtoResponse.getToken(), addSongDtoResponse.getId(), 3);

		Object response = client.post(baseURL + "/concert/songs/ratings", request, EmptyJsonDtoResponse.class);
		if (response instanceof EmptyJsonDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			return (EmptyJsonDtoResponse) response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected ErrorDtoResponse testAddRatingSongAdder(LoginDtoResponse loginDtoResponse, AddSongDtoResponse addSongDtoResponse, String text, ServerErrorCode expectedStatus){
		RatingAddDtoRequest request = new RatingAddDtoRequest(
				loginDtoResponse.getToken(), addSongDtoResponse.getId(), 3);

		Object response = client.post(baseURL + "/concert/songs/ratings", request, ErrorDtoResponse.class);
		if (response instanceof ErrorDtoResponse) {
			assertEquals(ServerErrorCode.SONG_ADDER_CHANGE_RATING_ATTEMPT, expectedStatus);
			ErrorDtoResponse ex = (ErrorDtoResponse) response;
			assertEquals(text, ex.getError());
			return ex;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected ErrorDtoResponse testAddRatingSecondTime(LoginDtoResponse loginDtoResponse, AddSongDtoResponse addSongDtoResponse, String text, ServerErrorCode expectedStatus){
		testAddRating(loginDtoResponse, addSongDtoResponse, ServerErrorCode.SUCCESS);
		RatingAddDtoRequest request = new RatingAddDtoRequest(
				loginDtoResponse.getToken(), addSongDtoResponse.getId(), 3);

		Object response = client.post(baseURL + "/concert/songs/ratings", request, ErrorDtoResponse.class);
		if (response instanceof ErrorDtoResponse) {
			assertEquals(ServerErrorCode.SONG_RATING_ALREADY_ADDED, expectedStatus);
			ErrorDtoResponse ex = (ErrorDtoResponse) response;
			assertEquals(text, ex.getError());
			return ex;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected EmptyJsonDtoResponse testChangeRating(LoginDtoResponse loginDtoResponse, AddSongDtoResponse addSongDtoResponse, ServerErrorCode expectedStatus){
		testAddRating(loginDtoResponse, addSongDtoResponse, ServerErrorCode.SUCCESS);
		RatingChangeDtoRequest request = new RatingChangeDtoRequest(
				loginDtoResponse.getToken(), addSongDtoResponse.getId(), 3);

		Object response = client.put(baseURL + "/concert/songs/ratings", request, EmptyJsonDtoResponse.class);
		if (response instanceof EmptyJsonDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			return (EmptyJsonDtoResponse) response;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected ErrorDtoResponse testChangeRatingWithoutAdding(LoginDtoResponse loginDtoResponse, AddSongDtoResponse addSongDtoResponse, String text, ServerErrorCode expectedStatus){
		RatingChangeDtoRequest request = new RatingChangeDtoRequest(
				loginDtoResponse.getToken(), addSongDtoResponse.getId(), 3);

		Object response = client.put(baseURL + "/concert/songs/ratings", request, ErrorDtoResponse.class);
		if (response instanceof ErrorDtoResponse) {
			assertEquals(ServerErrorCode.SONG_IS_NOT_RATED_BY_USER, expectedStatus);
			ErrorDtoResponse ex = (ErrorDtoResponse) response;
			assertEquals(text, ex.getError());
			return ex;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected AppropriateSongsDtoResponse testGetComposers(LoginDtoResponse loginDtoResponse,
													ServerErrorCode expectedStatus,
													List<String> composers,
													List<String> titlesExpected){

		StringBuilder selectedComposers = new StringBuilder("");
		int count = composers.size();

		for(String composer : composers){
			selectedComposers.append(composer);
			count--;
			if(count > 0) {
				selectedComposers.append(",");
			}
		}

		Object response = client.get(baseURL + "/concert/songs/composers?composers=" + selectedComposers, loginDtoResponse.getToken(), AppropriateSongsDtoResponse.class);
		if (response instanceof AppropriateSongsDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			AppropriateSongsDtoResponse appropriateSongsDtoResponse = (AppropriateSongsDtoResponse) response;
			List<String> sortedExpected = Ordering.natural().sortedCopy(titlesExpected);
			List<String> sortedActual = Ordering.natural().sortedCopy(appropriateSongsDtoResponse.getTitles());
			int counter = 0;
			assertEquals(sortedExpected.size(), sortedActual.size());
			for(String composerExpected : sortedExpected){
				assertEquals(composerExpected, sortedActual.get(counter));
				counter++;
			}
			return appropriateSongsDtoResponse;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

	protected AppropriateSongsDtoResponse testGetSongsByPerformer(LoginDtoResponse loginDtoResponse,
														   ServerErrorCode expectedStatus,
														   String performer,
														   List<String> titlesExpected){


		Object response = client.get(baseURL + "/concert/songs/performers/" + performer, loginDtoResponse.getToken(), AppropriateSongsDtoResponse.class);
		if (response instanceof AppropriateSongsDtoResponse) {
			assertEquals(ServerErrorCode.SUCCESS, expectedStatus);
			AppropriateSongsDtoResponse appropriateSongsDtoResponse = (AppropriateSongsDtoResponse) response;
			List<String> sortedExpected = Ordering.natural().sortedCopy(titlesExpected);
			List<String> sortedActual = Ordering.natural().sortedCopy(appropriateSongsDtoResponse.getTitles());
			int counter = 0;
			assertEquals(sortedExpected.size(), sortedActual.size());
			for(String composerExpected : sortedExpected){
				assertEquals(composerExpected, sortedActual.get(counter));
				counter++;
			}
			return appropriateSongsDtoResponse;
		} else {
			checkFailureResponse(response, expectedStatus);
			return null;
		}
	}

}
