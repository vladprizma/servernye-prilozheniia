package net.thumbtack.school.concert.dao;

import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.model.*;

import java.util.List;

public interface SongDao {

    void addSong(Song song) throws ServerException;
    void removeSong(Integer id);
    Song getSong(Integer id) throws ServerException;
    List<Song> getSongs();
    List<Song> getSongsFromPerformer(String performer);
    void truncateAllTables();

    void addSongAdder(SongAdder songAdder);
    void updateSongAdderSetUserToNull(Integer idSong);

    void addComposer(Composer composer);
    List<Composer> getComposersByName(String composer);

    void addWordsAuthor(WordsAuthor wordsAuthor);
    List<WordsAuthor> getWordsAuthorsByName(String wordsAuthor);

    void addRating(Rating rating);
    void updateRating(Rating rating);
    Rating getRatingByIdUser(Integer idSong, Integer idUser);
    void removeRatingByUser(Integer idUser, Integer idSong);

    void addComment(Comment comment);
    List<Comment> getCommentsByIdUser(Integer idUser);
    Comment getCommentByIdUser(Integer idSong, Integer idUser);
    void updateCommentSetUserToNull(Integer id);
    void removeComment(Integer id);

    void addJoined(Joined joined);
    void removeJoined(Integer idUser, Integer idComment);


}
