package net.thumbtack.school.concert.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Composer {
    private Integer idSong;
    private String composer;
}
