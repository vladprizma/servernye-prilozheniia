package net.thumbtack.school.concert.mapper;

import net.thumbtack.school.concert.model.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface UserMapper {

    @Insert("INSERT INTO `user` (lastName, firstName, login, password) VALUES (#{user.lastName}, #{user.firstName}, #{user.login}, #{user.password})")
    @Options(useGeneratedKeys = true, keyProperty = "user.id")
    void registerUser(@Param("user") User user);

    @Select("SELECT * FROM `user` WHERE login = #{login}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "songs", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getSongsByIdUser", fetchType = FetchType.LAZY))
    })
    User getUserByLogin(@Param("login") String login);

    @Select("SELECT * FROM `user` WHERE id = (SELECT `token`.idUser FROM `token` WHERE `token`.token = #{token})")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "songs", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getSongsByIdUser", fetchType = FetchType.LAZY))
    })
    User getUserByToken(@Param("token") String token);

    @Select("SELECT * FROM `user` WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "songs", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getSongsByIdUser", fetchType = FetchType.LAZY))
    })
    User getUserByIdUser(@Param("id") Integer id);

    @Delete("DELETE FROM `user` WHERE `user`.id = (SELECT `token`.idUser FROM `token` WHERE `token`.token = #{token})")
        // @Delete("DELETE FROM `token` WHERE token = #{token}") уберется каскадным удалением
    void deleteUser(@Param("token") String token);

    @Update("ALTER TABLE `user` AUTO_INCREMENT = 1")
    void prepareUsers();

    @Delete("DELETE FROM `user`")
    void deleteUsers();

    // token

    @Insert("INSERT INTO `token` (token, idUser) VALUES (#{token}, #{user.id})")
    void addActiveUser(@Param("token") String token, @Param("user") User user);

    @Select("SELECT token FROM `token` WHERE idUser = #{user.id}")
    String containsActiveUser(@Param("user") User user);

    @Delete("DELETE FROM `token` WHERE token = #{token}")
    void deleteActiveUser(@Param("token") String token);

}
