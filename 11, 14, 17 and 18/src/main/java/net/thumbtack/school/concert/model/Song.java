package net.thumbtack.school.concert.model;

import lombok.*;

import javax.annotation.Nullable;
import java.util.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Song {
    private Integer id;
    private String title;
    private String performer;
    private Integer duration;
    private AbstractUser songAdder;
    private List<String> composers;
    private List<String> wordsAuthors;
    private List<Rating> ratings = new ArrayList<>();
    private List<Comment> comments = new ArrayList<>();

    public void addRating(Rating rating){
        ratings.add(rating);
    }

    public void removeRating(Rating rating){
        ratings.remove(rating);
    }

    public void addComment(Comment comment){
        comments.add(comment);
    }

    public void removeComment(Comment comment){
        comments.remove(comment);
    }

}
