package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ChangeCommentDtoRequest {
    // private static final int MIN_ID = 1;
    @NotEmpty
    private String token;
    @Min(1)
    private int id;
    @NotEmpty
    private String comment;

    /*
    public static void validate(ChangeCommentDtoRequest dtoRequest) throws ServerException {
        if(dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getId() < MIN_ID ||
                dtoRequest.getComment() == null ||
                dtoRequest.getComment().equals("")){

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }
     */
}
