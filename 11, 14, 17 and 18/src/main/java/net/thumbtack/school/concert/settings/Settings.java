package net.thumbtack.school.concert.settings;

public class Settings {
    private static Settings settings;
    private boolean mode;
    private static int restHttpPort = 8080;
    // false - RAM
    // true - MyBatis

    public Settings(){
        this.mode = false;
    }

    public static Settings getSettings(){
        if(Settings.settings == null){
            settings = new Settings();
        }
        return settings;
    }

    public static int getRestHTTPPort() {
        return restHttpPort;
    }

    public void setModeToRAM(){
        mode = false;
    }

    public void setModeToMyBatis(){
        mode = true;
    }

    public boolean getMode(){
        return mode;
    }

}
