package net.thumbtack.school.concert.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class WordsAuthor {
    private Integer idSong;
    private String wordsAuthor;
}
