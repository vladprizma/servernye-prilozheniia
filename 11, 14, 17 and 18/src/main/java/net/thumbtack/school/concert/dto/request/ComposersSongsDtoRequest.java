package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ComposersSongsDtoRequest extends DtoRequest{
    @NotEmpty
    private String token;
    @NotEmpty
    @Size(min = 1)
    private List<@NotEmpty String> composers;

    /*
    public static void validate(ComposersSongsDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getComposers() == null ||
                dtoRequest.getComposers().size() == 0 ||
                dtoRequest.getComposers().contains(null) ||
                dtoRequest.getComposers().contains("") ) {

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }
*/
}
