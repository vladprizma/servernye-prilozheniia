package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class CancelJoinToCommentDtoRequest extends DtoRequest {
    // private static final int MIN_ID = 1;
    @NotEmpty
    private String token;
    @NotEmpty
    private String commentAuthorLogin;
    @Min(1)
    private int id;
/*
    public static void validate(CancelJoinToCommentDtoRequest dtoRequest) throws ServerException {
        if(dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getCommentAuthorLogin() == null ||
                dtoRequest.getCommentAuthorLogin().equals("") ||
                dtoRequest.getId() < MIN_ID){

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }
*/

}
