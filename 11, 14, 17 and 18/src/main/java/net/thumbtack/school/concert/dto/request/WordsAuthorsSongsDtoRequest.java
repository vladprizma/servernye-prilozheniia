package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class WordsAuthorsSongsDtoRequest extends DtoRequest {
    @NotEmpty
    private String token;
    @NotEmpty
    @Size(min = 1)
    private List<@NotEmpty String> wordsAuthors;

    /*
    public static void validate(WordsAuthorsSongsDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getWordsAuthors() == null ||
                dtoRequest.getWordsAuthors().size() == 0 ||
                dtoRequest.getWordsAuthors().contains(null) ||
                dtoRequest.getWordsAuthors().contains("") ) {

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }
    */
}
