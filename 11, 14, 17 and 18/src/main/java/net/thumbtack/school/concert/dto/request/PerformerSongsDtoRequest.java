package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class PerformerSongsDtoRequest extends DtoRequest {
    @NotEmpty
    private String token;
    @NotEmpty
    private String performer;

    /*
    public static void validate(PerformerSongsDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getPerformer() == null ||
                dtoRequest.getPerformer().equals("")) {

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }
*/
}
