package net.thumbtack.school.concert.resources;

import net.thumbtack.school.concert.dto.request.*;
import net.thumbtack.school.concert.service.SongService;
import net.thumbtack.school.concert.service.UserService;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api")
public class Resources {
    private SongService songService = new SongService();
    private UserService userService = new UserService();

    @POST
    @Path("/concert/songs")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addSong(@Valid AddSongDtoRequest request) {
        return songService.addSong(request);
    }

    @DELETE
    @Path("/concert/songs/{id}")
    @Produces("application/json")
    public Response deleteSong(@HeaderParam("token") String token,
                               @PathParam("id") int id){
        return songService.deleteSong(token, id);
    }

    @GET
    @Path("/concert/songs/composers")
    @Produces("application/json")
    public Response getSongsFromComposers(@HeaderParam("token") String token,
                                          @QueryParam("composers") List<String> composers){
        return songService.getSongsFromComposers(token, composers);
    }

    @GET
    @Path("/concert/songs/wordsauthors")
    @Produces("application/json")
    public Response getSongsFromWordsAuthors(@HeaderParam("token") String token,
                                             @QueryParam("wordsAuthors") List<String> wordsAuthors){
        return songService.getSongsFromWordsAuthors(token, wordsAuthors);
    }

    @GET
    @Path("/concert/songs/performers/{performer}")
    @Produces("application/json")
    public String getSongsFromPerformer(@HeaderParam("token") String token,
                                        @PathParam("performer") String performer){
        return songService.getSongsFromPerformer(token, performer);
    }

    @POST
    @Path("/concert/songs/comments/joins")
    @Consumes("application/json")
    @Produces("application/json")
    public Response joinToComment(@Valid JoinCommentDtoRequest request){
        return songService.joinToComment(request);
    }

    @DELETE
    @Path("/concert/songs/comments/joins/{commentAuthorLogin}/{id}")
    @Produces("application/json")
    public Response cancelJoinToComment(@HeaderParam("token") String token,
                                        @PathParam("commentAuthorLogin") String commentAuthorLogin,
                                        @PathParam("id") int id){
        return songService.cancelJoinToComment(token, commentAuthorLogin, id);
    }

    @POST
    @Path("/concert/songs/comments")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addComment(@Valid AddCommentDtoRequest request){
        return songService.addComment(request);
    }

    @PUT
    @Path("/concert/songs/comments")
    @Produces("application/json")
    public Response changeComment(@Valid ChangeCommentDtoRequest request){
        return songService.changeComment(request);
    }

    @POST
    @Path("/concert/songs/ratings")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addRating(@Valid RatingAddDtoRequest request){
        return songService.addRating(request);
    }

    @PUT
    @Path("/concert/songs/ratings")
    @Produces("application/json")
    public Response changeRating(@Valid RatingChangeDtoRequest request){
        return songService.changeRating(request);
    }

    @DELETE
    @Path("/concert/songs/ratings/{id}")
    @Produces("application/json")
    public Response deleteRating(@HeaderParam("token") String token,
                                 @PathParam("id") int id){
        return songService.deleteRating(token, id);
    }

    @POST
    @Path("/concert/users/register")
    @Consumes("application/json")
    @Produces("application/json")
    public Response registration(@Valid RegisterDtoRequest request) {
        return userService.registration(request);
    }

    @POST
    @Path("/concert/users/login")
    @Consumes("application/json")
    @Produces("application/json")
    public Response login(@Valid LoginDtoRequest request) {
        return userService.login(request);
    }

    @DELETE
    @Path("/concert/users/logout")
    @Produces("application/json")
    public String logout(@HeaderParam("token") String token) {
        return userService.logout(token);
    }

    @DELETE
    @Path("/concert/users")
    @Produces("application/json")
    public Response deleteUser(@HeaderParam("token") String token) {
        return userService.deleteUser(token);
    }

}
