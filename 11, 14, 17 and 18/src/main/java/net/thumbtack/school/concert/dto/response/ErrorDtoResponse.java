package net.thumbtack.school.concert.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;

@AllArgsConstructor
@Getter
public class ErrorDtoResponse {
    private String error;
}
