package net.thumbtack.school.concert.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class SongAdder {
    private Integer idSong;
    private Integer idUser;

}
