package net.thumbtack.school.concert.daoimpl;

import net.thumbtack.school.concert.mapper.SongMapper;
import net.thumbtack.school.concert.mapper.UserMapper;
import net.thumbtack.school.concert.utils.MyBatisUtils;
import org.apache.ibatis.session.SqlSession;

public class DaoImplBase {
    protected SqlSession getSession() {
        return MyBatisUtils.getSqlSessionFactory().openSession();
    }

    protected UserMapper getUserMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(UserMapper.class);
    }

    protected SongMapper getSongMapper(SqlSession sqlSession) {
        return sqlSession.getMapper(SongMapper.class);
    }
}
