package net.thumbtack.school.concert.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.logging.LoggingFeature;
import net.thumbtack.school.concert.dto.response.ErrorDtoResponse;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {

	private static final Gson GSON = new GsonBuilder().create();
	
	private static javax.ws.rs.client.Client createClient() {
		return ClientBuilder.newClient().register(JacksonFeature.class).
				register(new LoggingFeature(
						Logger.getLogger(Client.class.getName()), Level.INFO, null, null));
	}

	public Object get(String url, String token, Class<?> classResponse) {
		javax.ws.rs.client.Client client = createClient();
		WebTarget myResource = client.target(url);
		Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
		Response response = builder.header("token", token).get();
		String body = response.readEntity(String.class);
		int httpCode = response.getStatus();
		Object obj;
		if(httpCode == Response.Status.OK.getStatusCode()) 
			obj = GSON.fromJson(body, classResponse);
			else {
				obj = GSON.fromJson(body, ErrorDtoResponse.class);
			}
		client.close();
		return obj;
	}
	
	public Object post(String url, Object object, Class<?> classResponse) {
		javax.ws.rs.client.Client client = createClient();
		WebTarget myResource = client.target(url);
		Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
		Response response = builder.post(Entity.json(object));
		String body = response.readEntity(String.class);
		int httpCode = response.getStatus();
		Object obj;
		if(httpCode == Response.Status.OK.getStatusCode()) { 
			obj = GSON.fromJson(body, classResponse);
		}
		else {
			obj = GSON.fromJson(body, ErrorDtoResponse.class);
		}
		client.close();
		return obj;
	}

	public Object put(String url, Object object,	Class<?> classResponse) {
		javax.ws.rs.client.Client client = createClient();
		WebTarget myResource = client.target(url);
		Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
		Response response = builder.put(Entity.json(object));

		String body = response.readEntity(String.class);
		int httpCode = response.getStatus();
		Object obj;
		if(httpCode == Response.Status.OK.getStatusCode()) { 
			obj = GSON.fromJson(body, classResponse);
		}
		else {
			obj = GSON.fromJson(body, ErrorDtoResponse.class);
		}
		client.close();
		return obj;
	}

	public Object delete(String url, String token, Class<?> classResponse) {
		javax.ws.rs.client.Client client = createClient();
		WebTarget myResource = client.target(url);
		Invocation.Builder builder = myResource.request(MediaType.APPLICATION_JSON);
		Response response = builder.header("token", token).delete();
		String body = response.readEntity(String.class);
		int httpCode = response.getStatus();
		Object obj;
		if(httpCode == Response.Status.OK.getStatusCode()) 
			obj = GSON.fromJson(body, classResponse);
			else {
				obj = GSON.fromJson(body, ErrorDtoResponse.class);
			}
		client.close();
		return obj;
	}
}
