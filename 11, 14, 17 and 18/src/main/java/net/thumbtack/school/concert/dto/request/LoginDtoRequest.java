package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class LoginDtoRequest {
    @NotEmpty
    private String login;
    @NotEmpty
    private String password;

    /*
    public static void validate(LoginDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest.getLogin() == null ||
                dtoRequest.getLogin().equals("") ||
                dtoRequest.getPassword() == null ||
                dtoRequest.getPassword().equals("")) {
            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }
*/
}
