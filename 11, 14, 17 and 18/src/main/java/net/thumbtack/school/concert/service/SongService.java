package net.thumbtack.school.concert.service;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import net.thumbtack.school.concert.dao.SongDao;
import net.thumbtack.school.concert.dao.UserDao;
import net.thumbtack.school.concert.daoimpl.SongDaoImpl;
import net.thumbtack.school.concert.daoimpl.UserDaoImpl;
import net.thumbtack.school.concert.dto.request.*;
import net.thumbtack.school.concert.dto.response.AddSongDtoResponse;
import net.thumbtack.school.concert.dto.response.AppropriateSongsDtoResponse;
import net.thumbtack.school.concert.dto.response.EmptyJsonDtoResponse;
import net.thumbtack.school.concert.dto.response.ErrorDtoResponse;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.generic.GenericMethods;
import net.thumbtack.school.concert.utils.Mapper;
import net.thumbtack.school.concert.model.*;
import net.thumbtack.school.concert.utils.RestUtils;

import javax.validation.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SongService {
    private static Gson gson = new Gson();
    private static final int FIRST_ELEMENT = 0;
    private static final Integer MAX_RATING = 5;
    private static final int MIN_SONG_RATING_AMOUNT = 1;
    private static final int NO_JOINED_USERS = 0;
    private SongDao songDao = new SongDaoImpl();
    private UserDao userDao = new UserDaoImpl();

    private User getUserByLogin(String login) throws ServerException {
        User user = userDao.getUserByLogin(login);
        if(user == null){
            throw new ServerException(ServerErrorCode.USER_LOGIN_DOES_NOT_EXIST);
        }
        return user;
    }

    private User getUserByToken(String token) throws ServerException {
        User user = userDao.getUserByToken(token);
        if(user == null){
            throw new ServerException(ServerErrorCode.USER_WRONG_TOKEN);
        }
        return user;
    }

    private Song getSong(int id) throws ServerException {
        Song song = songDao.getSong(id);
        if(song == null){
            throw new ServerException(ServerErrorCode.SONG_DOES_NOT_EXIST);
        }
        return song;
    }

    private void songRated(User user, Song song, boolean isRated) throws ServerException {
        Rating rating = songDao.getRatingByIdUser(song.getId(), user.getId());
        if(rating != null && !isRated){
            throw new ServerException(ServerErrorCode.SONG_RATING_ALREADY_ADDED);
        }
        if(rating == null && isRated){
            throw new ServerException(ServerErrorCode.SONG_IS_NOT_RATED_BY_USER);
        }
    }

    private void userIsSongAdder(User user, Song song, boolean equalsUser) throws ServerException {
        if(user != null && song.getSongAdder() != null) {
            Integer userId = user.getId();
            Integer songAdderId = ((User) song.getSongAdder()).getId();
            boolean isSameUser = userId.equals(songAdderId);
            if (!isSameUser && equalsUser) {
                throw new ServerException(ServerErrorCode.SONG_WRONG_USER_DELETE_ATTEMPT);
            }
            if (isSameUser && !equalsUser) {
                throw new ServerException(ServerErrorCode.SONG_ADDER_CHANGE_RATING_ATTEMPT);
            }
        }
        if(user == null && song.getSongAdder() == null && !equalsUser) {
            throw new ServerException(ServerErrorCode.SONG_ADDER_CHANGE_RATING_ATTEMPT);
        }
        if(user == null && song.getSongAdder() != null && equalsUser) {
            throw new ServerException(ServerErrorCode.SONG_WRONG_USER_DELETE_ATTEMPT);
        }
        if(user != null && song.getSongAdder() == null && equalsUser) {
            throw new ServerException(ServerErrorCode.SONG_WRONG_USER_DELETE_ATTEMPT);
        }
    }

    private void containsCommentByUser(User user, Song song, boolean isCommented) throws ServerException {
        Comment comment = songDao.getCommentByIdUser(song.getId(), user.getId());
        if(comment == null && isCommented){
            throw new ServerException(ServerErrorCode.SONG_IS_NOT_COMMENTED_BY_USER);
        }
        if (comment != null && !isCommented) {
            throw new ServerException(ServerErrorCode.SONG_USER_ALREADY_IN_CURRENT_COMMENT_SECTION);
        }
    }

    private void containsJoinedUser(User commentAuthor, User user, Song song, boolean containsUser) throws ServerException {
        Comment comment = songDao.getCommentByIdUser(song.getId(), commentAuthor.getId());
        boolean isContainsUser = false;
        for(Joined joined : comment.getJoined()){
            if(joined.getIdUser().equals(user.getId())){
                isContainsUser = true;
                break;
            }
        }
        if(!isContainsUser && containsUser){
            throw new ServerException(ServerErrorCode.SONG_USER_NOT_JOINED_TO_COMMENT);
        }
        if(isContainsUser && !containsUser){
            throw new ServerException(ServerErrorCode.SONG_USER_ALREADY_JOINED_TO_COMMENT);
        }
    }

    public Response addSong(AddSongDtoRequest request) {
        try{
            Song song = Mapper.INSTANCE.dtoToSong(request);
            User user = getUserByToken(request.getToken());
            song.setSongAdder(user);

            songDao.addSong(song);

            AddSongDtoResponse response = new AddSongDtoResponse(song.getId());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public Response deleteSong(String token, int id) {
        try{
            DeleteSongDtoRequest request = new DeleteSongDtoRequest(token, id);
            validate(request);

            Song song = getSong(request.getId());
            User user = getUserByToken(request.getToken());
            userIsSongAdder(user, song, true);

            if(song.getRatings().size() == MIN_SONG_RATING_AMOUNT) {
                songDao.removeSong(song.getId());
            }
            else{
                songDao.removeRatingByUser(user.getId(), song.getId());
                songDao.updateSongAdderSetUserToNull(song.getId());
            }

            String response = gson.toJson(new EmptyJsonDtoResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public String getAllSongs(GetSongsDtoRequest request){
        List<Song> songs = new ArrayList<>(songDao.getSongs());
        List<String> titles = new ArrayList<>();
        for(Song song : songs){
            titles.add(song.getTitle());
        }

        AppropriateSongsDtoResponse response = new AppropriateSongsDtoResponse(titles);
        return gson.toJson(response);
    }

    public Response getSongsFromComposers(String token, List<String> composersList){
        try{
            ComposersSongsDtoRequest request = new ComposersSongsDtoRequest(token, composersList);
            validate(request);

            List<String> titles = new ArrayList<>();
            Multimap<String, Integer> songs = HashMultimap.create();
            List<Composer> composers;
            for(String composerName : request.getComposers()){
                composers = songDao.getComposersByName(composerName);
                if(composers != null) {
                    List<Integer> idSongList = new ArrayList<>();
                    for(Composer composer : composers){
                        idSongList.add(composer.getIdSong());
                    }
                    songs.putAll(composerName, idSongList);
                }
            }

            if(songs.size() > 0) {
                List<Integer> intersection = new ArrayList<>(songs.get(request.getComposers().get(FIRST_ELEMENT)));
                for (String composer : songs.keySet()) {
                    intersection.retainAll(songs.get(composer));
                }
                for(Integer idSong : intersection){
                    titles.add(songDao.getSong(idSong).getTitle());
                }
            }

            AppropriateSongsDtoResponse response = new AppropriateSongsDtoResponse(titles);
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public Response getSongsFromWordsAuthors(String token, List<String> wordsAuthorsList){
        try{
            WordsAuthorsSongsDtoRequest request = new WordsAuthorsSongsDtoRequest(token, wordsAuthorsList);
            validate(request);

            List<String> titles = new ArrayList<>();
            Multimap<String, Integer> songs = HashMultimap.create();
            // Integer, т.к. одинаковые Song генерируют разные хэши
            List<WordsAuthor> wordsAuthors;
            for(String wordsAuthorName : request.getWordsAuthors()){
                wordsAuthors = songDao.getWordsAuthorsByName(wordsAuthorName);
                if(wordsAuthors != null) {
                    List<Integer> idSongList = new ArrayList<>();
                    for(WordsAuthor wordsAuthor : wordsAuthors){
                        idSongList.add(wordsAuthor.getIdSong());
                    }
                    songs.putAll(wordsAuthorName, idSongList);
                }
            }
            List<String> actual = new ArrayList<>(songs.keySet());

            if(songs.size() > 0) {
                List<Integer> intersection = new ArrayList<>(songs.get(actual.get(FIRST_ELEMENT)));
                actual.remove(FIRST_ELEMENT);
                for (String wordsAuthor : actual) {
                    intersection.retainAll(songs.get(wordsAuthor));
                }
                for(Integer idSong : intersection){
                    titles.add(songDao.getSong(idSong).getTitle());
                }
            }

            AppropriateSongsDtoResponse response = new AppropriateSongsDtoResponse(titles);
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public String getSongsFromPerformer(String token, String performer){
        PerformerSongsDtoRequest request = new PerformerSongsDtoRequest(token, performer);
        validate(request);

        List<Song> performersSongs = new ArrayList<>(songDao.getSongsFromPerformer(request.getPerformer()));
        List<String> titles = new ArrayList<>();
        for(Song song : performersSongs){
            titles.add(song.getTitle());
        }

        AppropriateSongsDtoResponse response = new AppropriateSongsDtoResponse(titles);
        return gson.toJson(response);
    }

    public Response joinToComment(JoinCommentDtoRequest request){
        try{
            Song song = getSong(request.getId());
            User user = getUserByToken(request.getToken());
            User commentAuthor = getUserByLogin(request.getCommentAuthorLogin());
            containsCommentByUser(commentAuthor, song, true);
            containsJoinedUser(commentAuthor, user, song, false);
            Comment comment = songDao.getCommentByIdUser(song.getId(), commentAuthor.getId());

            songDao.addJoined(new Joined(user.getId(), comment.getId()));

            String response = gson.toJson(new EmptyJsonDtoResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public Response cancelJoinToComment(String token, String commentAuthorLogin, int id){
        try{
            CancelJoinToCommentDtoRequest request = new CancelJoinToCommentDtoRequest(token, commentAuthorLogin, id);
            validate(request);

            Song song = getSong(request.getId());
            User user = getUserByToken(request.getToken());
            User commentAuthor = getUserByLogin(request.getCommentAuthorLogin());
            containsCommentByUser(commentAuthor, song, true);
            containsJoinedUser(commentAuthor, user, song, true);
            Comment comment = songDao.getCommentByIdUser(song.getId(), commentAuthor.getId());

            songDao.removeJoined(user.getId(), comment.getId());

            String response = gson.toJson(new EmptyJsonDtoResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public Response addComment(AddCommentDtoRequest request){
        try{
            User user = getUserByToken(request.getToken());
            Song song = getSong(request.getId());

            containsCommentByUser(user, song, false);
            Comment comment = new Comment(user.getId(), song.getId(), request.getComment());

            songDao.addComment(comment);

            String response = gson.toJson(new EmptyJsonDtoResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public Response changeComment(ChangeCommentDtoRequest request){
        try{
            User user = getUserByToken(request.getToken());
            Song song = getSong(request.getId());

            containsCommentByUser(user, song, true);
            Comment newComment = new Comment(user.getId(), song.getId(), request.getComment());

            Comment comment = songDao.getCommentByIdUser(song.getId(), user.getId());

            if(comment.getJoined().size() == NO_JOINED_USERS) {
                songDao.removeComment(comment.getId());
                songDao.addComment(newComment);
            }
            else{
                songDao.updateCommentSetUserToNull(comment.getId());
                songDao.addComment(newComment);
            }

            String response = gson.toJson(new EmptyJsonDtoResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public Response addRating(RatingAddDtoRequest request){
        try{
            User user = getUserByToken(request.getToken());
            Song song = getSong(request.getId());
            userIsSongAdder(user, song, false);
            songRated(user, song, false);
            Rating rating = new Rating(user.getId(), song.getId(), request.getRating());

            songDao.addRating(rating);

            String response = gson.toJson(new EmptyJsonDtoResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public Response changeRating(RatingChangeDtoRequest request){
        try{
            User user = getUserByToken(request.getToken());
            Song song = getSong(request.getId());
            userIsSongAdder(user, song, false);
            songRated(user, song, true);
            Rating newRating = new Rating(user.getId(), song.getId(), request.getRating());

            songDao.updateRating(newRating);

            String response = gson.toJson(new EmptyJsonDtoResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public Response deleteRating(String token, int id){
        try{
            RatingDeleteDtoRequest request = new RatingDeleteDtoRequest();
            validate(request);

            User user = getUserByToken(request.getToken());
            Song song = getSong(request.getId());
            userIsSongAdder(user, song, false);
            songRated(user, song, true);
            songDao.removeRatingByUser(user.getId(), song.getId());

            String response = gson.toJson(new EmptyJsonDtoResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    private <T extends DtoRequest> void validate(T request) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> violations = validator.validate(request);
        if(violations.size() != 0) {
            throw new ConstraintViolationException(
                    new HashSet<ConstraintViolation<?>>(violations));
        }
    }

}
