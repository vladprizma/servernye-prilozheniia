package net.thumbtack.school.concert.mapper;

import net.thumbtack.school.concert.model.*;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.FetchType;

import java.util.List;

public interface SongMapper {

    @Insert("INSERT INTO `song` (title, performer, duration) VALUES (#{song.title}, #{song.performer}, #{song.duration})")
    @Options(useGeneratedKeys = true, keyProperty = "song.id")
    void addSong(@Param("song") Song song);

    @Delete("DELETE FROM `song` WHERE id = #{id}")
    void removeSong(@Param("id") Integer id);

    @Select("SELECT * FROM `song` WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "songAdder", column = "id", javaType = User.class,
                    one = @One(select = "net.thumbtack.school.concert.mapper.SongMapper.getSongAdder", fetchType = FetchType.LAZY)),
            @Result(property = "composers", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getComposers", fetchType = FetchType.LAZY)),
            @Result(property = "wordsAuthors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getWordsAuthor", fetchType = FetchType.LAZY)),
            @Result(property = "ratings", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getRatingsByIdSong", fetchType = FetchType.LAZY)),
            @Result(property = "comments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getCommentsByIdSong", fetchType = FetchType.LAZY))
    })
    Song getSong(@Param("id") Integer id);

    @Select("SELECT * FROM `song`")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "songAdder", column = "id", javaType = User.class,
                    one = @One(select = "net.thumbtack.school.concert.mapper.SongMapper.getSongAdder", fetchType = FetchType.LAZY)),
            @Result(property = "composers", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getComposers", fetchType = FetchType.LAZY)),
            @Result(property = "wordsAuthors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getWordsAuthor", fetchType = FetchType.LAZY)),
            @Result(property = "ratings", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getRatingsByIdSong", fetchType = FetchType.LAZY)),
            @Result(property = "comments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getCommentsByIdSong", fetchType = FetchType.LAZY))
    })
    List<Song> getSongs();

    @Select("SELECT * FROM `song` WHERE `song`.id in (SELECT `songAdder`.idSong FROM `songAdder` WHERE `songAdder`.idUser = #{idUser})")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "songAdder", column = "id", javaType = User.class,
                    one = @One(select = "net.thumbtack.school.concert.mapper.SongMapper.getSongAdder", fetchType = FetchType.LAZY)),
            @Result(property = "composers", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getComposers", fetchType = FetchType.LAZY)),
            @Result(property = "wordsAuthors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getWordsAuthor", fetchType = FetchType.LAZY)),
            @Result(property = "ratings", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getRatingsByIdSong", fetchType = FetchType.LAZY)),
            @Result(property = "comments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getCommentsByIdSong", fetchType = FetchType.LAZY))
    })
    List<Song> getSongsByIdUser(@Param("idUser") Integer idUser);


    @Select("SELECT * FROM `song` WHERE performer = #{performer}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "songAdder", column = "id", javaType = User.class,
                    one = @One(select = "net.thumbtack.school.concert.mapper.SongMapper.getSongAdder", fetchType = FetchType.LAZY)),
            @Result(property = "composers", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getComposers", fetchType = FetchType.LAZY)),
            @Result(property = "wordsAuthors", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getWordsAuthor", fetchType = FetchType.LAZY)),
            @Result(property = "ratings", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getRatingsByIdSong", fetchType = FetchType.LAZY)),
            @Result(property = "comments", column = "id", javaType = List.class,
                    many = @Many(select = "net.thumbtack.school.concert.mapper.SongMapper.getCommentsByIdSong", fetchType = FetchType.LAZY))
    })
    List<Song> getSongsFromPerformer(@Param("performer") String performer);

    @Update("ALTER TABLE `song` AUTO_INCREMENT = 1")
    void prepareSongs();

    @Delete("DELETE FROM `song`")
    void deleteSongs();

    // songAdder

    @Insert("INSERT INTO `songAdder` (idSong, idUser) VALUES (#{songAdder.idSong}, #{songAdder.idUser})")
    Integer addSongAdder(@Param("songAdder") SongAdder songAdder);

    @Select("SELECT idUser FROM `songAdder` WHERE idSong = #{idSong}")
    Integer getIdUserFromSongAdder(@Param("idSong") Integer idSong);

    @Select("SELECT * FROM `user` WHERE id = (SELECT idUser FROM `songAdder` WHERE idSong = #{idSong})")
    User getSongAdder(@Param("idSong") Integer idSong);

    @Select("UPDATE `songAdder` SET idUser = NULL WHERE idSong = #{idSong}")
    void updateSongAdderSetUserToNull(Integer idSong);

    @Select("SELECT idSong FROM `songAdder` WHERE idUser = #{idUser}")
    List<Integer> getIdSongsByIdUser(@Param("idUser") Integer idUser);

    // composer

    @Insert("INSERT INTO `composer` (idSong, composer) VALUES (#{composer.idSong}, #{composer.composer})")
    Integer addComposer(@Param("composer") Composer composer);

    @Select("SELECT * FROM `composer` WHERE idSong = #{idSong}")
    List<Composer> getComposers(@Param("idSong") Integer idSong);

    @Select("SELECT * FROM `composer` WHERE composer = #{composer}")
    List<Composer> getComposersByName(@Param("composer") String composer);

    // wordsAuthor

    @Insert("INSERT INTO `wordsAuthor` (idSong, wordsAuthor) VALUES (#{wordsAuthor.idSong}, #{wordsAuthor.wordsAuthor})")
    Integer addWordsAuthor(@Param("wordsAuthor") WordsAuthor wordsAuthor);

    @Select("SELECT * FROM `wordsAuthor` WHERE idSong = #{idSong}")
    List<WordsAuthor> getWordsAuthor(@Param("idSong") Integer idSong);

    @Select("SELECT * FROM `wordsAuthor` WHERE wordsAuthor = #{wordsAuthor}")
    List<WordsAuthor> getWordsAuthorsByName(@Param("wordsAuthor") String wordsAuthor);

    // rating

    @Insert("INSERT INTO `rating` (idUser, idSong, rating) VALUES (#{rating.idUser}, #{rating.idSong}, #{rating.rating})")
    Integer addRating(@Param("rating") Rating rating);

    @Select("UPDATE `rating` SET rating = #{rating.rating} WHERE idUser = #{rating.idUser} AND idSong = #{rating.idSong}")
    void updateRating(@Param("rating") Rating rating);

    @Select("SELECT * FROM `rating` WHERE idSong = #{idSong}")
    List<Rating> getRatingsByIdSong(@Param("idSong") Integer idSong);

    @Select("SELECT * FROM `rating` WHERE idSong = #{idSong} AND idUser = #{idUser}")
    Rating getRatingByIdUser(@Param("idSong") Integer idSong, @Param("idUser") Integer idUser);

    @Delete("DELETE FROM `rating` WHERE idUser = #{idUser} AND idSong = #{idSong}")
    void removeRatingByUser(@Param("idUser") Integer idUser, @Param("idSong") Integer idSong);


    // comment

    @Insert("INSERT INTO `comment` (idUser, idSong, comment) VALUES (#{comment.idUser}, #{comment.idSong}, #{comment.comment})")
    Integer addComment(@Param("comment") Comment comment);

    @Select("SELECT * FROM `comment` WHERE idSong = #{idSong}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "joined", column = "id",
                    one = @One(select = "net.thumbtack.school.concert.mapper.SongMapper.getJoinedByIdComment", fetchType = FetchType.LAZY))
    })
    List<Comment> getCommentsByIdSong(@Param("idSong") Integer idSong);

    @Select("SELECT * FROM `comment` WHERE idUser = #{idUser}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "joined", column = "id",
                    one = @One(select = "net.thumbtack.school.concert.mapper.SongMapper.getJoinedByIdComment", fetchType = FetchType.LAZY))
    })
    List<Comment> getCommentsByIdUser(@Param("idUser") Integer idUser);

    @Select("SELECT * FROM `comment` WHERE idSong = #{idSong} AND idUser = #{idUser}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "joined", column = "id",
                    one = @One(select = "net.thumbtack.school.concert.mapper.SongMapper.getJoinedByIdComment", fetchType = FetchType.LAZY))
    })
    Comment getCommentByIdUser(@Param("idSong") Integer idSong, @Param("idUser") Integer idUser);

    @Select("UPDATE `comment` SET idUser = NULL WHERE id = #{id}")
    void updateCommentSetUserToNull(Integer id);

    @Delete("DELETE FROM `comment` WHERE id = #{id}")
    void removeComment(@Param("id") Integer id);

    @Update("ALTER TABLE `comment` AUTO_INCREMENT = 1")
    void prepareComments();

    // joined

    @Insert("INSERT INTO `joined` (idUser, idComment) VALUES (#{joined.idUser}, #{joined.idComment})")
    Integer addJoined(@Param("joined") Joined joined);

    @Select("SELECT * FROM `joined` WHERE idComment = #{idComment}")
    List<Joined> getJoinedByIdComment(@Param("idComment") Integer idComment);

    @Delete("DELETE FROM `joined` WHERE idUser = #{idUser} AND idComment = #{idComment}")
    void removeJoined(@Param("idUser") Integer idUser, @Param("idComment") Integer idComment);

}
