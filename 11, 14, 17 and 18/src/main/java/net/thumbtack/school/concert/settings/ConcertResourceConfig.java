package net.thumbtack.school.concert.settings;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

public class ConcertResourceConfig extends ResourceConfig {

    public ConcertResourceConfig() {
        packages("net.thumbtack.school.concert.resources",
                "net.thumbtack.school.concert.rest.mappers");
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
    }

}
