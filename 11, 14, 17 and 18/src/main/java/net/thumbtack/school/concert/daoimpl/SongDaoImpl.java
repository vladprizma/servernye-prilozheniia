package net.thumbtack.school.concert.daoimpl;

import com.google.common.collect.Multimap;
import net.thumbtack.school.concert.dao.SongDao;
import net.thumbtack.school.concert.database.Database;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.model.*;
import net.thumbtack.school.concert.settings.Settings;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

public class SongDaoImpl extends DaoImplBase implements SongDao {

    private static final Integer MAX_RATING = 5;
    private static final int FIRST_ELEMENT = 0;
    private static final Logger LOGGER = LoggerFactory.getLogger(SongDaoImpl.class);

    @Override
    public void addSong(Song song) throws ServerException {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().addSong(song);
        }
        else {
            LOGGER.debug("DAO add song {}", song);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).addSong(song);
                    getSongMapper(sqlSession).addSongAdder(new SongAdder(song.getId(), ((User) song.getSongAdder()).getId()));
                    song.getRatings().add(new Rating(((User) song.getSongAdder()).getId(), song.getId(), MAX_RATING));
                    getSongMapper(sqlSession).addRating(song.getRatings().get(FIRST_ELEMENT));
                    for(String wordsAuthor : song.getWordsAuthors()) {
                        getSongMapper(sqlSession).addWordsAuthor(new WordsAuthor(song.getId(), wordsAuthor));
                    }
                    for(String composer : song.getComposers()) {
                        getSongMapper(sqlSession).addComposer(new Composer(song.getId(), composer));
                    }
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't add song {} {}", song, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public void removeSong(Integer id) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().removeSong(id);
        }
        else {
            LOGGER.debug("DAO remove song {}", id);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).removeSong(id);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't remove song {} {}", id, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public Song getSong(Integer id) {
        if(!Settings.getSettings().getMode()) {
            return Database.getDatabase().getSong(id);
        }
        else {
            LOGGER.debug("DAO get song {}", id);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getSongMapper(sqlSession).getSong(id);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't get song {} {}", id, ex);
                    sqlSession.rollback();
                    throw ex;
                }
            }
        }
    }

    @Override
    public List<Song> getSongs() {
        if(!Settings.getSettings().getMode()) {
            return Database.getDatabase().getSongs();
        }
        else {
            LOGGER.debug("DAO get songs");
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getSongMapper(sqlSession).getSongs();
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't get songs", ex);
                    sqlSession.rollback();
                    throw ex;
                }
            }
        }
    }

    @Override
    public void truncateAllTables() {
        if(Settings.getSettings().getMode()) {
            LOGGER.debug("DAO truncate all tables");
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).deleteSongs();
                    getSongMapper(sqlSession).prepareSongs();
                    getSongMapper(sqlSession).prepareComments();
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't truncate all tables", ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public List<Song> getSongsFromPerformer(String performer) {
        if(!Settings.getSettings().getMode()) {
            return Database.getDatabase().getSongsFromPerformer(performer);
        }
        else {
            LOGGER.debug("DAO get songs from performer {}", performer);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getSongMapper(sqlSession).getSongsFromPerformer(performer);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't get songs from performer {} {}", performer, ex);
                    sqlSession.rollback();
                    throw ex;
                }
            }
        }
    }

    @Override
    public void addSongAdder(SongAdder songAdder) {
        if(Settings.getSettings().getMode()) {
            LOGGER.debug("DAO add song adder {}", songAdder);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).addSongAdder(songAdder);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't add song adder {} {}", songAdder, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public void updateSongAdderSetUserToNull(Integer idSong) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().updateSongAdderSetUserToNull(idSong);
        }
        else {
            LOGGER.debug("DAO update song adder set user to null {}", idSong);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).updateSongAdderSetUserToNull(idSong);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't update song adder set user to null {} {}", idSong, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public void addComposer(Composer composer) {
        if(Settings.getSettings().getMode()) {
            LOGGER.debug("DAO add composer {}", composer);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).addComposer(composer);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't add composer {} {}", composer, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public List<Composer> getComposersByName(String composer) {
        if(!Settings.getSettings().getMode()) {
            return Database.getDatabase().getComposersByName(composer);
        }
        else {
            LOGGER.debug("DAO get composer by name {}", composer);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getSongMapper(sqlSession).getComposersByName(composer);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't get composer by name {} {}", composer, ex);
                    sqlSession.rollback();
                    throw ex;
                }
            }
        }
    }

    @Override
    public void addWordsAuthor(WordsAuthor wordsAuthor) {
        if(Settings.getSettings().getMode()) {
            LOGGER.debug("DAO add words author {}", wordsAuthor);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).addWordsAuthor(wordsAuthor);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't add words author {} {}", wordsAuthor, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public List<WordsAuthor> getWordsAuthorsByName(String wordsAuthor) {
        if(!Settings.getSettings().getMode()) {
            return Database.getDatabase().getWordsAuthorsByName(wordsAuthor);
        }
        else {
            LOGGER.debug("DAO get words author by name {}", wordsAuthor);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getSongMapper(sqlSession).getWordsAuthorsByName(wordsAuthor);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't get words author by name {} {}", wordsAuthor, ex);
                    sqlSession.rollback();
                    throw ex;
                }
            }
        }
    }

    @Override
    public void addRating(Rating rating) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().addRating(rating);
        }
        else {
            LOGGER.debug("DAO add rating {}", rating);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).addRating(rating);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't add rating {} {}", rating, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public void updateRating(Rating rating) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().updateRating(rating);
        }
        else {
            LOGGER.debug("DAO update rating {}", rating);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).updateRating(rating);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't update rating {} {}", rating, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public Rating getRatingByIdUser(Integer idSong, Integer idUser) {
        if(!Settings.getSettings().getMode()) {
            return Database.getDatabase().getRatingByIdUser(idSong, idUser);
        }
        else {
            LOGGER.debug("DAO get rating by id user {} {}", idSong, idUser);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getSongMapper(sqlSession).getRatingByIdUser(idSong, idUser);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't get rating by id user {} {} {}", idSong, idUser, ex);
                    sqlSession.rollback();
                    throw ex;
                }
            }
        }
    }

    @Override
    public void removeRatingByUser(Integer idUser, Integer idSong) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().removeRatingByUser(idUser, idSong);
        }
        else {
            LOGGER.debug("DAO remove rating by user {} {}", idUser, idSong);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).removeRatingByUser(idUser, idSong);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't remove rating by user {} {} {}", idUser, idSong, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public void addComment(Comment comment) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().addComment(comment);
        }
        else {
            LOGGER.debug("DAO add comment {}", comment);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).addComment(comment);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't add comment {} {}", comment, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public List<Comment> getCommentsByIdUser(Integer idUser) {
        if(!Settings.getSettings().getMode()) {
            return Database.getDatabase().getCommentsByIdUser(idUser);
        }
        else {
            LOGGER.debug("DAO get comments by id user {}", idUser);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getSongMapper(sqlSession).getCommentsByIdUser(idUser);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't get comments by id user {} {}", idUser, ex);
                    sqlSession.rollback();
                    throw ex;
                }
            }
        }
    }

    @Override
    public Comment getCommentByIdUser(Integer idSong, Integer idUser) {
        if(!Settings.getSettings().getMode()) {
            return Database.getDatabase().getCommentByIdUser(idSong, idUser);
        }
        else {
            LOGGER.debug("DAO get comment by id user {} {}", idSong, idUser);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getSongMapper(sqlSession).getCommentByIdUser(idSong, idUser);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't get comment by id user {} {} {}", idSong, idUser, ex);
                    sqlSession.rollback();
                    throw ex;
                }
            }
        }
    }

    @Override
    public void updateCommentSetUserToNull(Integer id) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().updateCommentSetUserToNull(id);
        }
        else {
            LOGGER.debug("DAO update comment set user to null {}", id);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).updateCommentSetUserToNull(id);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't update comment set user to null {} {}", id, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public void removeComment(Integer id) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().removeComment(id);
        }
        else {
            LOGGER.debug("DAO remove comment {}", id);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).removeComment(id);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't remove comment {} {}", id, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public void addJoined(Joined joined) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().addJoined(joined);
        }
        else {
            LOGGER.debug("DAO add joined {}", joined);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).addJoined(joined);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't add joined {} {}", joined, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public void removeJoined(Integer idUser, Integer idComment) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().removeJoined(idUser, idComment);
        }
        else {
            LOGGER.debug("DAO remove joined {} {}", idUser, idComment);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getSongMapper(sqlSession).removeJoined(idUser, idComment);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't remove joined {} {} {}", idUser, idComment, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    /*

    @Override
    public int addSong(Song song) throws ServerException {
        return Database.getDatabase().addSong(song);
    }

    @Override
    public void removeSong(int id) {
        Database.getDatabase().removeSong(id);
    }

    @Override
    public Song getSong(int id) {
        return Database.getDatabase().getSong(id);
    }

    // Set<Song>
    @Override
    public List<Song> getSongs() {
        return Database.getDatabase().getSongs();
    }

    // Multimap<String, Song>
    @Override
    public List<Song> getSongsFromComposers(List<String> composers) {
        return Database.getDatabase().getSongsFromComposers(composers);
    }

    // Multimap<String, Song>
    @Override
    public List<Song> getSongsFromWordsAuthors(List<String> wordsAuthors) {
        return Database.getDatabase().getSongsFromWordsAuthors(wordsAuthors);
    }

    @Override
    public List<Song> getSongsFromPerformer(String performer) {
        return Database.getDatabase().getSongsFromPerformer(performer);
    }
*/
}
