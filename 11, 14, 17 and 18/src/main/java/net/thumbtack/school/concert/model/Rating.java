package net.thumbtack.school.concert.model;


import lombok.*;

@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Rating {
    private Integer idUser;
    private Integer idSong;
    @NonNull
    private Integer rating;
    /*
    @NonNull
    private User user;
    @NonNull
    private Song song;
    @NonNull
    private int rating;
     */
}
