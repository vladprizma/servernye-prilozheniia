package net.thumbtack.school.concert.daoimpl;

import net.thumbtack.school.concert.dao.UserDao;
import net.thumbtack.school.concert.database.Database;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.model.User;
import net.thumbtack.school.concert.settings.Settings;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserDaoImpl extends DaoImplBase implements UserDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @Override
    public void addActiveUser(String token, User user) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().addActiveUser(token, user);
        }
        else {
            LOGGER.debug("DAO add active user {}", user);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getUserMapper(sqlSession).addActiveUser(token, user);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't add active user {} {}", user, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public void deleteActiveUser(String token) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().deleteActiveUser(token);
        }
        else {
            LOGGER.debug("DAO delete active user {}", token);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getUserMapper(sqlSession).deleteActiveUser(token);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't delete active user {} {}", token, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public void deleteUser(String token) {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().deleteUser(token);
        }
        else {
            LOGGER.debug("DAO delete user {}", token);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getUserMapper(sqlSession).deleteUser(token);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't delete user {} {}", token, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public String containsActiveUser(User user) {
        if(!Settings.getSettings().getMode()) {
            return Database.getDatabase().containsActiveUser(user);
        }
        else {
            LOGGER.debug("DAO contains active user {}", user);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getUserMapper(sqlSession).containsActiveUser(user);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't check if contains active user {} {}", user, ex);
                    sqlSession.rollback();
                    throw ex;
                }
            }
        }
    }


    @Override
    public User getUserByLogin(String login) throws ServerException {
        if(!Settings.getSettings().getMode()) {
            return Database.getDatabase().getUserByLogin(login);
        }
        else {
            LOGGER.debug("DAO get user by login {}", login);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getUserMapper(sqlSession).getUserByLogin(login);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't get user by login {} {}", login, ex);
                    sqlSession.rollback();
                    throw ex;
                }
            }
        }
    }

    @Override
    public User getUserByToken(String token) {
        if(!Settings.getSettings().getMode()) {
            return Database.getDatabase().getUserByToken(token);
        }
        else {
            LOGGER.debug("DAO get user by token {}", token);
            try (SqlSession sqlSession = getSession()) {
                try {
                    return getUserMapper(sqlSession).getUserByToken(token);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't get user by token {} {}", token, ex);
                    sqlSession.rollback();
                    throw ex;
                }
            }
        }
    }

    @Override
    public void registerUser(User user) throws ServerException {
        if(!Settings.getSettings().getMode()) {
            Database.getDatabase().registerUser(user);
        }
        else {
            LOGGER.debug("DAO register user {}", user);
            try (SqlSession sqlSession = getSession()) {
                try {
                    getUserMapper(sqlSession).registerUser(user);
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't register user {} {}", user, ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

    @Override
    public void truncateAllTables() {
        if(!Settings.getSettings().getMode()) {
            Database.truncateTables();
        }
        else{
            LOGGER.debug("DAO truncate all tables");
            try (SqlSession sqlSession = getSession()) {
                try {
                    getUserMapper(sqlSession).deleteUsers();
                    getUserMapper(sqlSession).prepareUsers();
                } catch (RuntimeException ex) {
                    LOGGER.info("Can't truncate all tables", ex);
                    sqlSession.rollback();
                    throw ex;
                }
                sqlSession.commit();
            }
        }
    }

/*
    // to work w Database class

    @Override
    public void addActiveUser(String token, User user) {
        Database.getDatabase().addActiveUser(token, user);
    }

    @Override
    public void deleteActiveUser(String token) {
        Database.getDatabase().deleteActiveUser(token);
    }

    @Override
    public void deleteUser(String token) {
        Database.getDatabase().deleteUser(token);
    }

    @Override
    public String containsActiveUser(User user) {
        return Database.getDatabase().containsActiveUser(user);
    }

    @Override
    public User getUserByLogin(String login) {
        return Database.getDatabase().getUserByLogin(login);
    }

    @Override
    public User getUserByToken(String token){
        return Database.getDatabase().getUserByToken(token);
    }

    @Override
    public void registerUser(String token, User user) throws ServerException {
        Database.getDatabase().registerUser(token, user);
    }
*/
}
