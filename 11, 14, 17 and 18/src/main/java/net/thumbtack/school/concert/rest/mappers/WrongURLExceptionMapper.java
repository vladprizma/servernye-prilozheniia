package net.thumbtack.school.concert.rest.mappers;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.utils.RestUtils;


@Provider
public class WrongURLExceptionMapper implements	ExceptionMapper<NotFoundException> {

    @Override
	public Response toResponse(NotFoundException exception) {
		return RestUtils.failureResponse(Status.NOT_FOUND, new ServerException(ServerErrorCode.WRONG_URL));
	}
}