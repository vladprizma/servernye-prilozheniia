package net.thumbtack.school.concert.exception;

import net.thumbtack.school.concert.errorcode.ServerErrorCode;

public class ServerException extends Exception{

    private static final long serialVersionUID = 6049904777923589329L;
    private ServerErrorCode serverErrorCode;

    public ServerException(ServerErrorCode serverErrorCode) {
        this.serverErrorCode = serverErrorCode;
    }

    public String getMessage() {
        return serverErrorCode.getErrorString();
    }
}
