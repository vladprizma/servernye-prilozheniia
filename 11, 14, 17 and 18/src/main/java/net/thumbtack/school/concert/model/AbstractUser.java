package net.thumbtack.school.concert.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
public abstract class AbstractUser {

    private List<Song> songs = new ArrayList<>();
    private List<Comment> comments = new ArrayList<>();

    public void addSong(Song song){
        this.songs.add(song);
    }

    public void addComment(Comment comment){
        this.comments.add(comment);
    }

    public void removeComment(Comment comment){
        this.comments.remove(comment);
    }

}
