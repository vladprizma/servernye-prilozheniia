package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class RatingAddDtoRequest {
    @NotEmpty
    private String token;
    @Min(1)
    private int id;
    @Min(1)
    @Max(5)
    private int rating;

    /*
    private static final int MIN_ID = 1;
    private static final int MAX_RATING = 5;
    private static final int MIN_RATING = 1;

    public static void validate(RatingAddDtoRequest dtoRequest) throws ServerException {
        if(dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getId() < MIN_ID ||
                dtoRequest.getRating() < MIN_RATING ||
                dtoRequest.getRating() > MAX_RATING){

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }
*/
}
