package net.thumbtack.school.concert.rest.mappers;

import net.thumbtack.school.concert.dto.response.ErrorDtoResponse;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.utils.RestUtils;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class TodoListExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
	
    @Override
    public Response toResponse(ConstraintViolationException exception) {
        /*
        StringBuilder message = new StringBuilder();
        for (ConstraintViolation<?> cv : exception.getConstraintViolations()) {
            message.append(cv.getPropertyPath()).append(" ").append(cv.getMessage()).append("\n");
        }
    	*/
		return RestUtils.failureResponse(Status.BAD_REQUEST,
				new ServerException(ServerErrorCode.VALIDATION_ERROR));

    }
}