package net.thumbtack.school.concert.model;

import lombok.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class User extends AbstractUser {
    private Integer id;
    @NonNull
    private String lastName;
    @NonNull
    private String firstName;
    @NonNull
    private String login;
    @NonNull
    private String password;

    /*
    private Set<Rating> ratedSongs = new HashSet<>();

    public void addRating(Rating rating){
        this.ratedSongs.add(rating);
    }

    public void removeRating(Rating rating){
        this.ratedSongs.remove(rating);
    }
    */
    public void removeSong(Song song){
        getSongs().remove(song);
    }

}
