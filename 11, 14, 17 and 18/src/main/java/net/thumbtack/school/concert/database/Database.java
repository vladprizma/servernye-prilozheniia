package net.thumbtack.school.concert.database;

import com.google.common.collect.*;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.model.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public final class Database {
    private static Database database;
    private static AtomicInteger idSongCounter;
    private static AtomicInteger idUserCounter;
    private static AtomicInteger idCommentCounter;
    private static final Integer MAX_RATING = 5;
    private static final int FIRST_ELEMENT = 0;
    private Map<String, User> users;
    private BiMap<String, User> onlineUsers;
    private BiMap<Integer, Song> idSong;
    private BiMap<Integer, Comment> idComments;
    private Multimap<String, Song> performerSongs;
    private Multimap<String, Composer> composerSongs;
    private Multimap<String, WordsAuthor> wordsAuthorsSongs;
    private Multimap<Integer, Comment> userComments;
    private Multimap<Integer, Rating> userRatings;


    private Database() {
        idSongCounter = new AtomicInteger(0);
        idUserCounter = new AtomicInteger(0);
        idCommentCounter = new AtomicInteger(0);
        this.users = new HashMap<>();
        this.onlineUsers =  Maps.synchronizedBiMap(HashBiMap.create());
        this.idSong = Maps.synchronizedBiMap(HashBiMap.create());
        this.idComments = Maps.synchronizedBiMap(HashBiMap.create());
        this.composerSongs = Multimaps.synchronizedMultimap(HashMultimap.create());
        this.wordsAuthorsSongs = Multimaps.synchronizedMultimap(HashMultimap.create());
        this.performerSongs = Multimaps.synchronizedMultimap(HashMultimap.create());
        this.userComments = Multimaps.synchronizedMultimap(HashMultimap.create());
        this.userRatings = Multimaps.synchronizedMultimap(HashMultimap.create());
    }

    public static Database getDatabase(){
        if(Database.database == null){
            database = new Database();
        }
        return database;
    }

    public static void truncateTables(){
        database = new Database();
    }

    //Возвращает токен по активному пользователю, если он уже в сети
    public String containsActiveUser(User user){
        return onlineUsers.inverse().get(user);
    }

    public void addActiveUser(String token, User user){
        this.onlineUsers.put(token, user);
    }

    public void deleteActiveUser(String token) {
        onlineUsers.remove(token);
    }

    public User getUserByLogin(String login) {
        return users.get(login);
    }

    public User getUserByToken(String token){
        return onlineUsers.get(token);
    }

    public void registerUser(User user) throws ServerException {
        user.setId(idUserCounter.incrementAndGet());
        String userLogin = user.getLogin();
        if(users.putIfAbsent(userLogin, user) != null){
            throw new ServerException(ServerErrorCode.USER_LOGIN_OCCUPIED);
        }
    }

    public void deleteUser(String token){
        this.users.remove(onlineUsers.remove(token).getLogin());
    }

    public int addSong(Song song) throws ServerException {
        song.setId(idSongCounter.incrementAndGet());
        song.getRatings().add(new Rating(((User) song.getSongAdder()).getId(), song.getId(), MAX_RATING));
        this.idSong.put(song.getId(), song);
        for(String composer : song.getComposers()){
            this.composerSongs.put(composer, new Composer(song.getId(), composer));
        }
        for(String wordsAuthor : song.getWordsAuthors()){
            this.wordsAuthorsSongs.put(wordsAuthor, new WordsAuthor(song.getId(), wordsAuthor));
        }
        this.performerSongs.put(song.getPerformer(), song);

        return song.getId();
    }

    public void removeSong(int id){
        this.idSong.remove(id);
    }

    public Song getSong(int id) {
        return idSong.get(id);
    }

    public List<Song> getSongs() {
        return new ArrayList<>(idSong.values());
    }

    public List<Song> getSongsFromPerformer(String performer) {
        return new ArrayList<>(performerSongs.get(performer));
    }

    public void updateSongAdderSetUserToNull(Integer id){
        idSong.get(id).setSongAdder(null);
    }

    public List<Composer> getComposersByName(String composer){
        return new ArrayList<>(composerSongs.get(composer));
    }

    public List<WordsAuthor> getWordsAuthorsByName(String wordsAuthor){
        return new ArrayList<>(wordsAuthorsSongs.get(wordsAuthor));
    }

    public void addRating(Rating rating){
        idSong.get(rating.getIdSong()).addRating(rating);
        userRatings.put(rating.getIdUser(), rating);
    }

    public void updateRating(Rating rating){
        for(Rating ratingInSearch : userRatings.get(rating.getIdUser())){
            if(ratingInSearch.getIdSong().equals(rating.getIdSong())){
                userRatings.remove(ratingInSearch.getIdUser(), ratingInSearch);
            }
        }
        userRatings.put(rating.getIdUser(), rating);
    }

    public Rating getRatingByIdUser(Integer idSong, Integer idUser){
        int counter = 0;
        for(Rating ratingInSearch : userRatings.get(idUser)){
            if(ratingInSearch.getIdSong().equals(idSong)){
                return (new ArrayList<>(userRatings.get(ratingInSearch.getIdUser()))).get(counter);
            }
            counter++;
        }
        return null;
    }

    public void removeRatingByUser(Integer idUser, Integer idOfSong){
        List<Rating> ratings = new ArrayList<>(userRatings.get(idUser));
        for(Rating ratingInSearch : ratings){
            if(ratingInSearch.getIdSong().equals(idOfSong)){
                idSong.get(ratingInSearch.getIdSong()).removeRating(ratingInSearch);
                userRatings.remove(idUser, ratingInSearch);
                break;
            }
        }
    }

    public void addComment(Comment comment){
        comment.setId(idCommentCounter.incrementAndGet());
        idSong.get(comment.getIdSong()).addComment(comment);
        idComments.put(comment.getId(), comment);
        userComments.put(comment.getIdUser(), comment);
    }

    public List<Comment> getCommentsByIdUser(Integer idUser){
        return new ArrayList<>(userComments.get(idUser));
    }

    public Comment getCommentByIdUser(Integer idSong, Integer idUser){
        for(Comment commentInSearch : userComments.get(idUser)){
            if(commentInSearch.getIdSong().equals(idSong)){
                return commentInSearch;
            }
        }
        return null;
    }

    public void updateCommentSetUserToNull(Integer id){
        idComments.get(id).setIdUser(null);
    }

    public void removeComment(Integer id){
        Comment comment = idComments.get(id);
        userComments.remove(comment.getIdUser(), comment);
        idSong.get(comment.getIdSong()).removeComment(comment);
        idComments.remove(id);
    }

    public void addJoined(Joined joined){
        idComments.get(joined.getIdComment()).addJoinedUser(joined);
    }

    public void removeJoined(Integer idUser, Integer idComment){
        idComments.get(idComment).getJoined().removeIf(joined -> joined.getIdUser().equals(idUser));
    }

}
