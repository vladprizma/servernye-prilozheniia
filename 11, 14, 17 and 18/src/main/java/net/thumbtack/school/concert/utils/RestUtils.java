package net.thumbtack.school.concert.utils;

import com.google.gson.Gson;
import net.thumbtack.school.concert.dto.response.ErrorDtoResponse;
import net.thumbtack.school.concert.exception.ServerException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class RestUtils {

	private static final Gson GSON = new Gson();

	public static Response failureResponse(Status status, ServerException ex) {
		return Response.status(status).entity(GSON.toJson(new ErrorDtoResponse(ex.getMessage())))
				.type(MediaType.APPLICATION_JSON).build();
	}

	public static Response failureResponse(ServerException ex) {
		return failureResponse(Status.BAD_REQUEST, ex);
	}

}
