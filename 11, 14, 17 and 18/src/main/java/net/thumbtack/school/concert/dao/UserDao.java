package net.thumbtack.school.concert.dao;

import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.model.User;

public interface UserDao {

    void addActiveUser(String token, User user);
    void deleteActiveUser(String token);
    void deleteUser(String token);
    public String containsActiveUser(User user);
    User getUserByLogin(String login) throws ServerException;
    User getUserByToken(String token);
    void registerUser(User user) throws ServerException;
    void truncateAllTables();

}
