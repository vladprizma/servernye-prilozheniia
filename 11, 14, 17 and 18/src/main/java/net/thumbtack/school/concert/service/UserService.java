package net.thumbtack.school.concert.service;

import com.google.gson.Gson;
import net.thumbtack.school.concert.dao.SongDao;
import net.thumbtack.school.concert.dao.UserDao;
import net.thumbtack.school.concert.daoimpl.SongDaoImpl;
import net.thumbtack.school.concert.daoimpl.UserDaoImpl;
import net.thumbtack.school.concert.dto.request.*;
import net.thumbtack.school.concert.dto.response.EmptyJsonDtoResponse;
import net.thumbtack.school.concert.dto.response.ErrorDtoResponse;
import net.thumbtack.school.concert.dto.response.LoginDtoResponse;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;
import net.thumbtack.school.concert.generic.GenericMethods;
import net.thumbtack.school.concert.utils.Mapper;
import net.thumbtack.school.concert.model.*;
import net.thumbtack.school.concert.utils.RestUtils;

import javax.validation.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

public class UserService {
    private static Gson gson = new Gson();
    private static final int MIN_SONG_RATING_AMOUNT = 1;
    private static final int NO_JOINED_USERS = 0;
    private UserDao userDao = new UserDaoImpl();
    private SongDao songDao = new SongDaoImpl();

    private User getUserByToken(String token) throws ServerException {
        User user = userDao.getUserByToken(token);
        if(user == null){
            throw new ServerException(ServerErrorCode.USER_WRONG_TOKEN);
        }
        return user;
    }

    private User getUserByLogin(String login) throws ServerException {
        User user = userDao.getUserByLogin(login);
        if(user == null){
            throw new ServerException(ServerErrorCode.USER_LOGIN_DOES_NOT_EXIST);
        }
        return user;
    }

    private User checkPassword(String password, User user) throws ServerException {
        if(!password.equals(user.getPassword())){
            throw new ServerException(ServerErrorCode.USER_PASSWORD_INCORRECT);
        }
        return user;
    }


    public Response registration(RegisterDtoRequest request) {
        try {
            User user = Mapper.INSTANCE.dtoToUser(request);
            String token = UUID.randomUUID().toString();
            userDao.registerUser(user);
            userDao.addActiveUser(token, user);

            LoginDtoResponse response = new LoginDtoResponse(token);
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public Response login(LoginDtoRequest request) {
        try {
            User user = checkPassword(request.getPassword(), getUserByLogin(request.getLogin()));

            String token = userDao.containsActiveUser(user);
            if(token == null){
                token = UUID.randomUUID().toString();
                userDao.addActiveUser(token, user);
            }

            LoginDtoResponse response = new LoginDtoResponse(token);
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public String logout(String token) {
        LogoutDtoRequest request = new LogoutDtoRequest(token);
        validate(request);

        userDao.deleteActiveUser(token);

        EmptyJsonDtoResponse dtoResponse = new EmptyJsonDtoResponse();
        return gson.toJson(dtoResponse);
    }

    public Response deleteUser(String token) {
        try {
            DeleteUserDtoRequest request = new DeleteUserDtoRequest(token);
            validate(request);

            User user = getUserByToken(token);
            Integer idUser = user.getId();

            for(Song song : user.getSongs()){
                if(song.getRatings().size() == MIN_SONG_RATING_AMOUNT){
                    songDao.removeSong(song.getId());
                    // всё остальное удалится каскадым удалением
                }
                else {
                    songDao.removeRatingByUser(user.getId(), song.getId());
                    songDao.updateSongAdderSetUserToNull(song.getId());
                }
            }

            List<Comment> comments = songDao.getCommentsByIdUser(idUser);

            for (Comment comment : comments){
                if(comment.getJoined().size() == NO_JOINED_USERS) {
                    songDao.removeComment(comment.getId());
                    // всё остальное удалится каскадым удалением
                }
                else{
                    songDao.updateCommentSetUserToNull(comment.getId());
                }
            }

            userDao.deleteUser(token);

            String response = gson.toJson(new EmptyJsonDtoResponse());
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        }
        catch(ServerException e){
            return RestUtils.failureResponse(e);
        }
    }

    public void truncateAllTables() {
        userDao.truncateAllTables();
        songDao.truncateAllTables();
    }

    private <T extends DtoRequest> void validate(T request) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> violations = validator.validate(request);
        if(violations.size() != 0) {
            throw new ConstraintViolationException(
                    new HashSet<ConstraintViolation<?>>(violations));
        }
    }

}
