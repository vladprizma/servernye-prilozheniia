package net.thumbtack.school.concert.model;

import lombok.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
public class Comment {
    private Integer id;
    private Integer idUser;
    private Integer idSong;
    private String comment;
    private List<Joined> joined = new ArrayList<>();

    public Comment(Integer idUser, Integer idSong, String comment) {
        this.idUser = idUser;
        this.idSong = idSong;
        this.comment = comment;
    }

    public void addJoinedUser(Joined joined){
        this.joined.add(joined);
    }

    public void removeJoinedUser(Joined joined) {
        this.joined.remove(joined);
    }
}
