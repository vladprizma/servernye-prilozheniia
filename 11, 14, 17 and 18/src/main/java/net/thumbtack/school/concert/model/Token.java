package net.thumbtack.school.concert.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Token {
    private String token;
    private Integer idUser;

}
