package net.thumbtack.school.concert.utils;


import net.thumbtack.school.concert.dto.request.AddSongDtoRequest;
import net.thumbtack.school.concert.dto.request.RegisterDtoRequest;
import net.thumbtack.school.concert.model.Song;
import net.thumbtack.school.concert.model.User;
import org.mapstruct.factory.Mappers;

@org.mapstruct.Mapper
public interface Mapper {

    Mapper INSTANCE = Mappers.getMapper(Mapper.class);

    Song dtoToSong(AddSongDtoRequest addSongDtoRequest);

    User dtoToUser(RegisterDtoRequest registerDtoRequest);

}
