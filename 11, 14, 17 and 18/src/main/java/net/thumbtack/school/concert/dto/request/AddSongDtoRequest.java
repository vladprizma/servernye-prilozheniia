package net.thumbtack.school.concert.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.thumbtack.school.concert.dao.SongDao;
import net.thumbtack.school.concert.dao.UserDao;
import net.thumbtack.school.concert.errorcode.ServerErrorCode;
import net.thumbtack.school.concert.exception.ServerException;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class AddSongDtoRequest {
    @NotEmpty
    private String token;
    @NotEmpty
    private String title;
    @NotEmpty
    @Size(min = 1)
    private List<@NotEmpty String> composers;
    @NotEmpty
    @Size(min = 1)
    private List<@NotEmpty String> wordsAuthors;
    @NotEmpty
    private String performer;
    @Min(1)
    private int duration;

    /*
    public static void validate(AddSongDtoRequest dtoRequest) throws ServerException {
        if (dtoRequest.getToken() == null ||
                dtoRequest.getToken().equals("") ||
                dtoRequest.getTitle() == null ||
                dtoRequest.getTitle().equals("") ||
                dtoRequest.getComposers() == null ||
                dtoRequest.getComposers().size() == 0 ||
                dtoRequest.getComposers().contains(null) ||
                dtoRequest.getComposers().contains("") ||
                dtoRequest.getWordsAuthors() == null ||
                dtoRequest.getWordsAuthors().size() == 0 ||
                dtoRequest.getWordsAuthors().contains(null) ||
                dtoRequest.getWordsAuthors().contains("") ||
                dtoRequest.getPerformer() == null ||
                dtoRequest.getPerformer().equals("") ||
                dtoRequest.getDuration() <= 0) {

            throw new ServerException(ServerErrorCode.REQUEST_WRONG_PARAMETERS);
        }
    }
    */
}
