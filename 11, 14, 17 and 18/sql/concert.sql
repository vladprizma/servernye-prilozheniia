drop database if exists concert;
create database concert;
use concert;

create table `user` (
	id INT(11) NOT NULL AUTO_INCREMENT,
	lastName VARCHAR(255) NOT NULL,
	firstName VARCHAR(255) NOT NULL,
	login VARCHAR(255) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
    
    PRIMARY KEY (id), 
    UNIQUE KEY (login),
    KEY (lastName),
    KEY (firstName)
 )ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

create table song (
	id INT(11) NOT NULL AUTO_INCREMENT,
	title VARCHAR(255) NOT NULL,
	performer VARCHAR(255) NOT NULL,
	duration INT(11) NOT NULL,
    
    PRIMARY KEY (id)
 )ENGINE=INNODB DEFAULT CHARSET=utf8mb4;
 
 create table token (
	token VARCHAR(255) NOT NULL,
	idUser INT(11) NOT NULL,
    
    foreign key (idUser) references `user` (id) on delete cascade
 )ENGINE=INNODB DEFAULT CHARSET=utf8mb4;
 
create table rating (
	idUser INT(11) NOT NULL,
	idSong INT(11) NOT NULL,
	rating INT(11) NOT NULL,
    
    foreign key (idUser) references `user` (id) on delete cascade,
	foreign key (idSong) references song (id) on delete cascade
 )ENGINE=INNODB DEFAULT CHARSET=utf8mb4;
 
create table `comment` (
   id INT(11) NOT NULL AUTO_INCREMENT,
   idUser INT(11) DEFAULT NULL,
   idSong INT(11) NOT NULL,
   `comment` VARCHAR(255) NOT NULL,
    
    PRIMARY KEY (id),
    foreign key (idUser) references `user` (id) ON DELETE SET NULL,
	foreign key (idSong) references song (id) on delete cascade
 )ENGINE=INNODB DEFAULT CHARSET=utf8mb4;
 
 create table composer (
	idSong INT(11) NOT NULL,
	composer VARCHAR(255) NOT NULL,
    
	foreign key (idSong) references song (id) on delete cascade
 )ENGINE=INNODB DEFAULT CHARSET=utf8mb4;
 
create table wordsAuthor (
	idSong INT(11) NOT NULL,
	wordsAuthor VARCHAR(255) NOT NULL,
    
	foreign key (idSong) references song (id) on delete cascade
)ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

create table joined (
	idUser INT(11) NOT NULL,
    idComment INT(11) NOT NULL,
    
	foreign key (idUser) references `user` (id) on delete cascade,
    foreign key (idComment) references `comment` (id) on delete cascade
)ENGINE=INNODB DEFAULT CHARSET=utf8mb4;

create table songAdder (
   idSong INT(11) NOT NULL,
   idUser INT(11) DEFAULT NULL,
    
	foreign key (idSong) references song (id) on delete cascade,
    foreign key (idUser) references `user` (id) ON DELETE SET NULL
 )ENGINE=INNODB DEFAULT CHARSET=utf8mb4;
 